const gulp = require('gulp');
const sass = require('gulp-sass');
const useref = require('gulp-useref');
const uglify = require('gulp-uglify');
const gulpIf = require('gulp-if');
const cssnano = require('gulp-cssnano');
const browsersync = require('browser-sync');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const htmlmin = require('gulp-htmlmin');

// browserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: './app/',
    },
    port: 3600
  });
  done();
}

function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// sass process function
function scss() {
  return gulp.src('./app/scss/**/*.scss')
  .pipe(sass({ outputStyle: 'expanded' }))
  .pipe(autoprefixer())
  .pipe(gulp.dest('./app/css'))
  .pipe(browsersync.stream());
}

//  watch and process images
function images() {
  return gulp.src('./app/images/**/*.+(png|jpg|gif|svg)')
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({
        plugins: [
          {
            removeViewBox: false,
            collapseGroups: true
          }
        ]
      })
    ]))
    .pipe(gulp.dest('./dist/images'));
}

// process fonts
function fonts() {
  return gulp.src('./app/fonts/*.*')
    .pipe(gulp.dest('./dist/fonts'));
}

// function clean
function clean() {
  return del(['./dist']);
}

// batch and minify
function batchMinify() {
  return gulp.src(['./app/*.html', './app/*/*.html'])
    .pipe(useref())
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.html', htmlmin({collapseWhitespace: true})))
    .pipe(gulp.dest('./dist'));
}

// serve files
function serveDev() {
  gulp.watch('./app/scss/**/*.scss', gulp.series(scss, browserSyncReload));
  gulp.watch('./app/js/**/*.js', browserSyncReload);
  gulp.watch('./app/**/*.html', browserSyncReload);
  gulp.watch('./app/images/**/*', gulp.series(images, browserSyncReload));
  gulp.watch('./app/fonts/**/*', gulp.series(fonts, browserSyncReload));
}

const build = gulp.series(clean, gulp.parallel(scss, images, fonts), batchMinify);
const watch = gulp.parallel(serveDev, browserSync);


exports.images = images;
exports.scss = scss;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = watch;

(function($){
    'use strict';

    var surveyData = {
        tqGreat: [],
        tqGood: [],
        tqFair: [],
        tqPoor: [],
        prepJobsYes: [],
        prepJobsNo: [],

    };

    var $loadprogress = $('#loadprogress');

    function processJobPreparedness(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;

        for(var item in obj){
            if(has.call(obj, item)){
                if(obj[item].areGraduatesPrepared == 'yes'){
                    surveyData.prepJobsYes.push(obj[item]);
                }
                if(obj[item].areGraduatesPrepared == 'no'){
                    surveyData.prepJobsNo.push(obj[item]);
                }
            }
        }

        var yesPerc = ((surveyData.prepJobsYes.length / objCount) * 100).toFixed(1);
        var noPerc = ((surveyData.prepJobsNo.length / objCount) * 100).toFixed(1);
        $('.yesJOB').html(yesPerc + '%');
        $('.noJOB').html(noPerc + '%');

        var graphData = {
            datasets:[{
                label: 'Graduate Preparedness',
                data: [surveyData.prepJobsYes.length, surveyData.prepJobsNo.length],
                backgroundColor: [
                    '#0077b3',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('jobPrep').getContext('2d');
        var jobPrepChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions       
        });
    }

    function processTalentQuality(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj) {
            if(has.call(obj, item)) {
                if (obj[item].currentTalentRankings == 'great') {
                    surveyData.tqGreat.push(obj[item]);
                }

                if (obj[item].currentTalentRankings == 'good') {
                    surveyData.tqGood.push(obj[item]);
                }

                if (obj[item].currentTalentRankings == 'fair' || obj[item].currentTalentRankings == 'fair`') {
                    surveyData.tqFair.push(obj[item]);
                }

                if (obj[item].currentTalentRankings == 'poor' || obj[item].currentTalentRankings == 'poor`') {
                    surveyData.tqPoor.push(obj[item]);
                }
            }
        }
        var greatPerc = ((surveyData.tqGreat.length / objCount) * 100).toFixed(1);
        var goodPerc = ((surveyData.tqGood.length / objCount) * 100).toFixed(1);
        var FairPerc = ((surveyData.tqFair.length / objCount) * 100).toFixed(1);
        var poorPerc = ((surveyData.tqPoor.length / objCount) * 100).toFixed(1);
        $('.great').html(greatPerc + '%');
        $('.good').html(goodPerc + '%');
        $('.fair').html(FairPerc + '%');
        $('.poor').html(poorPerc + '%');

        var graphData = {
            datasets: [{
                label: '',
                data: [surveyData.tqGreat.length, surveyData.tqGood.length, surveyData.tqFair.length, surveyData.tqPoor.length],
                backgroundColor: [
                    '#0077b3',
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Great',
                'Good',
                'Fair',
                'Poor'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('talentQualityChart').getContext('2d');
        var talentQualityChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processSurveyData(obj){
        var objCount = Object.keys(obj).length;
        $('.surveyCount').html(objCount);
        $('.totalResp').html(objCount);
        $loadprogress.addClass('d-none');
        processTalentQuality(obj);
        processJobPreparedness(obj);
    }

    function getCorporateSurveys(){
        $loadprogress.removeClass('d-none');
        service.getCorporateSurveys().then(function(snapshot){
            var c_surveys = snapshot.val();
            processSurveyData(c_surveys);
        }).catch(function(err){
            console.log(err);
        })
    }

    getCorporateSurveys();

    $('#sign-out').on('click', function(e) {
        e.preventDefault();
        service.signOut();
    });

}(jQuery))
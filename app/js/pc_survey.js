(function($) {
    'use strict';

    var surveyData = {
        age13_18: [],
        age19_25: [],
        age26_30: [],
        jhs: [],
        shs: [],
        university: [],
        postgraduate: [],
        chosenCareerYes: [],
        chosenCareerMaybe: [],
        chosencareerNo: [],
        careerChoiceYes: [],
        careerChoiceMaybe: [],
        careerChoiceNo: [],
        seekParentsAdviceYes: [],
        seekParentsAdviceMaybe:[],
        seekParentsAdviceNo:[],
        seekTeachersAdviceYes: [],
        seekTeachersAdviceMaybe:[],
        seekTeachersAdviceNo:[],
        isChoiceDifferentTeachersYes: [],
        isChoiceDifferentTeachersMaybe: [],
        isChoiceDifferentTeachersNo: [],
        isChoiceDifferentParentsYes: [],
        isChoiceDifferentParentsMaybe: [],
        isChoiceDifferentParentsNo: [],
        adequateInfoYes: [],
        adequateInfoNo: [],
        adequateInfoMaybe: [],
        consultIndustryProsYes: [],
        consultIndustryProsMaybe: [],
        consultIndustryProsNo: [],
        adviceFromProsYes: [],
        adviceFromProsMaybe: [],
        adviceFromProsNo: [],
        careerInterestYes: [],
        careerInterestMaybe: [],
        careerInterestNo: [],
        careerSalaryYes: [],
        careerSalaryMaybe: [],
        careerSalaryNo: [],
        careerPopularityYes: [],
        careerPopularityMaybe: [],
        careerPopularityNo: [],
        careerJSYes: [],
        careerJSMaybe: [],
        careerJSNo: [],
        careerMentorYes: [],
        careerMentorMaybe: [],
        careerMentorNo: [],
        careerGuidanceYes: [],
        careerGuidanceMaybe: [],
        careerGuidanceNo: []
    };
    var $loadprogress = $('#loadprogress');


    function processSurveyData(obj){
        var objCount = Object.keys(obj).length;
        $('.surveyCount').html(objCount);
        $('.totalResp').html(objCount);
        $loadprogress.addClass('d-none');
        processAgeData(obj);
        processEducationData(obj);
        processChosenCareers(obj);
        processOwnChoice(obj);
        processChoiceWithParents(obj);
        processChoiceWithTeachers(obj);
        processChoiceDiffParents(obj);
        processChoiceDiffTeachers(obj);
        processAdequateInfo(obj);
        processConsultIndustryPros(obj);
        processAdviceFromPros(obj);
        processCareerInterest(obj);
        processCareerSalary(obj);
        processCareerPopularity(obj);
        processJobSecurity(obj);
        processCareerMentor(obj);
        processCareerGuidance(obj);
    }

    function getPublicsurveys() {
        $loadprogress.removeClass('d-none');
        service.getPublicSurveys().then(function(snapshot){
            var p_surveys = snapshot.val();
            processSurveyData(p_surveys);
        }).catch(function(error) {
            console.log(error);
        });
    }

    function processCareerGuidance(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;

        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].careerChoiceByGuidance == 'agree' || obj[item].careerChoiceByGuidance == 'yes'){
                    surveyData.careerGuidanceYes.push(obj[item]);
                }
                if(obj[item].careerChoiceByGuidance == 'neutral' || obj[item].careerChoiceByGuidance == 'maybe'){
                    surveyData.careerGuidanceMaybe.push(obj[item]);
                }
                if(obj[item].careerChoiceByGuidance == 'disagree' || obj[item].careerChoiceByGuidance == 'no'){
                    surveyData.careerGuidanceNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.careerGuidanceYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.careerGuidanceMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.careerGuidanceNo.length / objCount) * 100).toFixed(1);
        $('.ccGuidanceYes').html(yesPercentage + '%');
        $('.ccGuidanceMaybe').html(maybePercentage + '%');
        $('.ccGuidanceNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.careerGuidanceYes.length, surveyData.careerGuidanceMaybe.length, surveyData.careerGuidanceNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccGuidanceChart').getContext('2d');
        var ccGuidanceChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processCareerMentor(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;

        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].careerChoiceByMentor == 'agree' || obj[item].careerChoiceByMentor == 'yes'){
                    surveyData.careerMentorYes.push(obj[item]);
                }
                if(obj[item].careerChoiceByMentor == 'neutral' || obj[item].careerChoiceByMentor == 'maybe'){
                    surveyData.careerMentorMaybe.push(obj[item]);
                }
                if(obj[item].careerChoiceByMentor == 'disagree' || obj[item].careerChoiceByMentor == 'no'){
                    surveyData.careerMentorNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.careerMentorYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.careerMentorMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.careerMentorNo.length / objCount) * 100).toFixed(1);
        $('.ccMentorYes').html(yesPercentage + '%');
        $('.ccMentorMaybe').html(maybePercentage + '%');
        $('.ccMentorNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.careerMentorYes.length, surveyData.careerMentorMaybe.length, surveyData.careerMentorNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccMentorChart').getContext('2d');
        var ccMentorChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processJobSecurity(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;

        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].careerChoiceBySecurity == 'agree' || obj[item].careerChoiceBySecurity == 'yes'){
                    surveyData.careerJSYes.push(obj[item]);
                }
                if(obj[item].careerChoiceBySecurity == 'neutral' || obj[item].careerChoiceBySecurity == 'maybe'){
                    surveyData.careerJSMaybe.push(obj[item]);
                }
                if(obj[item].careerChoiceBySecurity == 'disagree' || obj[item].careerChoiceBySecurity == 'no'){
                    surveyData.careerJSNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.careerJSYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.careerJSMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.careerJSNo.length / objCount) * 100).toFixed(1);
        $('.ccJSYes').html(yesPercentage + '%');
        $('.ccJSMaybe').html(maybePercentage + '%');
        $('.ccJSNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.careerJSYes.length, surveyData.careerJSMaybe.length, surveyData.careerJSNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccJSChart').getContext('2d');
        var ccJSChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processCareerPopularity(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;

        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].careerChoiceByPopularity == 'agree' || obj[item].careerChoiceByPopularity == 'yes'){
                    surveyData.careerPopularityYes.push(obj[item]);
                }
                if(obj[item].careerChoiceByPopularity == 'neutral' || obj[item].careerChoiceByPopularity == 'maybe'){
                    surveyData.careerPopularityMaybe.push(obj[item]);
                }
                if(obj[item].careerChoiceByPopularity == 'disagree' || obj[item].careerChoiceByPopularity == 'no'){
                    surveyData.careerPopularityNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.careerPopularityYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.careerPopularityMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.careerPopularityNo.length / objCount) * 100).toFixed(1);
        $('.ccPopularityYes').html(yesPercentage + '%');
        $('.ccPopularityMaybe').html(maybePercentage + '%');
        $('.ccPopularityNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.careerPopularityYes.length, surveyData.careerPopularityMaybe.length, surveyData.careerPopularityNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccPopularityChart').getContext('2d');
        var ccPopularityChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processCareerSalary(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;

        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].careerChoiceBySalary == 'agree' || obj[item].careerChoiceBySalary == 'yes'){
                    surveyData.careerSalaryYes.push(obj[item]);
                }
                if(obj[item].careerChoiceBySalary == 'neutral' || obj[item].careerChoiceBySalary == 'maybe'){
                    surveyData.careerSalaryMaybe.push(obj[item]);
                }
                if(obj[item].careerChoiceBySalary == 'disagree' || obj[item].careerChoiceBySalary == 'no'){
                    surveyData.careerSalaryNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.careerSalaryYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.careerSalaryMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.careerSalaryNo.length / objCount) * 100).toFixed(1);
        $('.ccSSYes').html(yesPercentage + '%');
        $('.ccSSMaybe').html(maybePercentage + '%');
        $('.ccSSNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.careerSalaryYes.length, surveyData.careerSalaryMaybe.length, surveyData.careerSalaryNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccSSChart').getContext('2d');
        var ccSSChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processCareerInterest(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].interestInCareerChoice == 'agree' || obj[item].interestInCareerChoice == 'yes' || obj[item].interestInCareerChoice == 'yes`'){
                    surveyData.careerInterestYes.push(obj[item]);
                }
                if(obj[item].interestInCareerChoice == 'neutral' || obj[item].interestInCareerChoice == 'maybe'){
                    surveyData.careerInterestMaybe.push(obj[item]);
                }
                if(obj[item].interestInCareerChoice == 'disagree' || obj[item].interestInCareerChoice == 'no'){
                    surveyData.careerInterestNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.careerInterestYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.careerInterestMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.careerInterestNo.length / objCount) * 100).toFixed(1);
        $('.ccInterestYes').html(yesPercentage + '%');
        $('.ccInterestMaybe').html(maybePercentage + '%');
        $('.ccInterestNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.careerInterestYes.length, surveyData.careerInterestMaybe.length, surveyData.careerInterestNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccInterestChart').getContext('2d');
        var ccInterestChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processAdviceFromPros(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].adviceFromPros == 'agree' || obj[item].adviceFromPros == 'yes'){
                    surveyData.adviceFromProsYes.push(obj[item]);
                }
                if(obj[item].adviceFromPros == 'neutral' || obj[item].adviceFromPros == 'maybe'){
                    surveyData.adviceFromProsMaybe.push(obj[item]);
                }
                if(obj[item].adviceFromPros == 'disagree' || obj[item].adviceFromPros == 'no'){
                    surveyData.adviceFromProsNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.adviceFromProsYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.adviceFromProsMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.adviceFromProsNo.length / objCount) * 100).toFixed(1);
        $('.ccAFPYes').html(yesPercentage + '%');
        $('.ccAFPMaybe').html(maybePercentage + '%');
        $('.ccAFPNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.adviceFromProsYes.length, surveyData.adviceFromProsMaybe.length, surveyData.adviceFromProsNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccAFPChart').getContext('2d');
        var ccAFPChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processConsultIndustryPros(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].consultIndustryPros == 'agree' || obj[item].consultIndustryPros == 'yes'){
                    surveyData.consultIndustryProsYes.push(obj[item]);
                }
                if(obj[item].consultIndustryPros == 'neutral' || obj[item].consultIndustryPros == 'maybe'){
                    surveyData.consultIndustryProsMaybe.push(obj[item]);
                }
                if(obj[item].consultIndustryPros == 'disagree' || obj[item].consultIndustryPros == 'no'){
                    surveyData.consultIndustryProsNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.consultIndustryProsYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.consultIndustryProsMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.consultIndustryProsNo.length / objCount) * 100).toFixed(1);
        $('.ccCIPYes').html(yesPercentage + '%');
        $('.ccCIPMaybe').html(maybePercentage + '%');
        $('.ccCIPNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.consultIndustryProsYes.length, surveyData.consultIndustryProsMaybe.length, surveyData.consultIndustryProsNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccCIPChart').getContext('2d');
        var ccCIPChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processAdequateInfo(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].adequateInfoAvailable == 'agree' || obj[item].adequateInfoAvailable == 'yes'){
                    surveyData.adequateInfoYes.push(obj[item]);
                }
                if(obj[item].adequateInfoAvailable == 'neutral' || obj[item].adequateInfoAvailable == 'maybe'){
                    surveyData.adequateInfoMaybe.push(obj[item]);
                }
                if(obj[item].adequateInfoAvailable == 'disagree' || obj[item].adequateInfoAvailable == 'no'){
                    surveyData.adequateInfoNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.adequateInfoYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.adequateInfoMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.adequateInfoNo.length / objCount) * 100).toFixed(1);
        $('.ccAIAYes').html(yesPercentage + '%');
        $('.ccAIAMaybe').html(maybePercentage + '%');
        $('.ccAIANo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.adequateInfoYes.length, surveyData.adequateInfoMaybe.length, surveyData.adequateInfoNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccAIAChart').getContext('2d');
        var ccAIAChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processChoiceDiffParents(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].isChoiceDifferentParents == 'agree' || obj[item].isChoiceDifferentParents == 'yes'){
                    surveyData.isChoiceDifferentParentsYes.push(obj[item]);
                }

                if(obj[item].isChoiceDifferentParents == 'neutral' || obj[item].isChoiceDifferentParents == 'maybe'){
                    surveyData.isChoiceDifferentParentsMaybe.push(obj[item]);
                }

                if(obj[item].isChoiceDifferentParents == 'disagree' || obj[item].isChoiceDifferentParents == 'no'){
                    surveyData.isChoiceDifferentParentsNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.isChoiceDifferentParentsYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.isChoiceDifferentParentsMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.isChoiceDifferentParentsNo.length / objCount) * 100).toFixed(1);
        $('.ccDPYes').html(yesPercentage + '%');
        $('.ccDPMaybe').html(maybePercentage + '%');
        $('.ccDPNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.isChoiceDifferentParentsYes.length, surveyData.isChoiceDifferentParentsMaybe.length, surveyData.isChoiceDifferentParentsNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccDPChart').getContext('2d');
        var ccDPChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processChoiceDiffTeachers(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].isChoiceDifferentTeachers == 'agree' || obj[item].isChoiceDifferentTeachers == 'yes'){
                    surveyData.isChoiceDifferentTeachersYes.push(obj[item]);
                }

                if(obj[item].isChoiceDifferentTeachers == 'neutral' || obj[item].isChoiceDifferentTeachers == 'maybe'){
                    surveyData.isChoiceDifferentTeachersMaybe.push(obj[item]);
                }

                if(obj[item].isChoiceDifferentTeachers == 'disagree' || obj[item].isChoiceDifferentTeachers == 'no'){
                    surveyData.isChoiceDifferentTeachersNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.isChoiceDifferentTeachersYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.isChoiceDifferentTeachersMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.isChoiceDifferentTeachersNo.length / objCount) * 100).toFixed(1);
        $('.ccDTYes').html(yesPercentage + '%');
        $('.ccDTMaybe').html(maybePercentage + '%');
        $('.ccDTNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.isChoiceDifferentTeachersYes.length, surveyData.isChoiceDifferentTeachersMaybe.length, surveyData.isChoiceDifferentTeachersNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccDTChart').getContext('2d');
        var ccDTChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processChoiceWithTeachers(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].seekTeachersAdvice == 'agree' || obj[item].seekTeachersAdvice == 'yes'){
                    surveyData.seekTeachersAdviceYes.push(obj[item]);
                }

                if(obj[item].seekTeachersAdvice == 'neutral' || obj[item].seekTeachersAdvice == 'maybe'){
                    surveyData.seekTeachersAdviceMaybe.push(obj[item]);
                }

                if(obj[item].seekTeachersAdvice == 'disagree' || obj[item].seekTeachersAdvice == 'no'){
                    surveyData.seekTeachersAdviceNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.seekTeachersAdviceYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.seekTeachersAdviceMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.seekTeachersAdviceNo.length / objCount) * 100).toFixed(1);
        $('.cctYes').html(yesPercentage + '%');
        $('.cctMaybe').html(maybePercentage + '%');
        $('.cctNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.seekTeachersAdviceYes.length, surveyData.seekTeachersAdviceMaybe.length, surveyData.seekTeachersAdviceNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccTeachersChart').getContext('2d');
        var cctChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processChoiceWithParents(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].seekParentsAdvice == 'agree' || obj[item].seekParentsAdvice == 'yes'){
                    surveyData.seekParentsAdviceYes.push(obj[item]);
                }

                if(obj[item].seekParentsAdvice == 'neutral' || obj[item].seekParentsAdvice == 'maybe'){
                    surveyData.seekParentsAdviceMaybe.push(obj[item]);
                }

                if(obj[item].seekParentsAdvice == 'disagree' || obj[item].seekParentsAdvice == 'no'){
                    surveyData.seekParentsAdviceNo.push(obj[item]);
                }
            }
        }

        var yesPercentage = ((surveyData.seekParentsAdviceYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.seekParentsAdviceMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.seekParentsAdviceNo.length / objCount) * 100).toFixed(1);
        $('.ccpYes').html(yesPercentage + '%');
        $('.ccpMaybe').html(maybePercentage + '%');
        $('.ccpNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.seekParentsAdviceYes.length, surveyData.seekParentsAdviceMaybe.length, surveyData.seekParentsAdviceNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('ccParentsChart').getContext('2d');
        var ccpChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processOwnChoice(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].wasCareerChoiceOwn == 'agree' || obj[item].wasCareerChoiceOwn == 'yes'){
                    surveyData.careerChoiceYes.push(obj[item]);
                }

                if(obj[item].wasCareerChoiceOwn == 'neutral' || obj[item].wasCareerChoiceOwn == 'maybe'){
                    surveyData.careerChoiceMaybe.push(obj[item]);
                }

                if(obj[item].wasCareerChoiceOwn == 'disagree' || obj[item].wasCareerChoiceOwn == 'no'){
                    surveyData.careerChoiceNo.push(obj[item]);
                }
            }
        }
        var yesPercentage = ((surveyData.careerChoiceYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.careerChoiceMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.careerChoiceNo.length / objCount) * 100).toFixed(1);
        $('.ccoYes').html(yesPercentage + '%');
        $('.ccoMaybe').html(maybePercentage + '%');
        $('.ccoNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.careerChoiceYes.length, surveyData.careerChoiceMaybe.length, surveyData.careerChoiceNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('chosenCareersOwnChart').getContext('2d');
        var ccChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processChosenCareers(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].isCareerChosen == 'agree' || obj[item].isCareerChosen == 'yes'){
                    surveyData.chosenCareerYes.push(obj[item]);
                }

                if(obj[item].isCareerChosen == 'maybe' || obj[item].isCareerChosen == 'neutral'){
                    surveyData.chosenCareerMaybe.push(obj[item]);
                }

                if(obj[item].isCareerChosen == 'no' || obj[item].isCareerChosen == 'disagree'){
                    surveyData.chosencareerNo.push(obj[item]);
                }
            }
        };
        var yesPercentage = ((surveyData.chosenCareerYes.length / objCount) * 100).toFixed(1);
        var maybePercentage = ((surveyData.chosenCareerMaybe.length / objCount) * 100).toFixed(1);
        var noPercentage = ((surveyData.chosencareerNo.length / objCount) * 100).toFixed(1);
        $('.ccYes').html(yesPercentage + '%');
        $('.ccMaybe').html(maybePercentage + '%');
        $('.ccNo').html(noPercentage + '%');

        var graphData = {
            datasets: [{
                label: 'Respondents who have chosen their careers',
                data: [surveyData.chosenCareerYes.length, surveyData.chosenCareerMaybe.length, surveyData.chosencareerNo.length],
                backgroundColor: [
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels: [
                'Yes',
                'Maybe',
                'No'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        };
        var ctx = document.getElementById('chosenCareersChart').getContext('2d');
        var ccChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    function processAgeData(obj){
        // console.log(obj);
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                
                if(obj[item].surveyTakerAge == 'thirteen-eighteen'){
                    surveyData.age13_18.push(obj[item]);
                }
                if(obj[item].surveyTakerAge == 'nineteen-twentyfive'){
                    surveyData.age19_25.push(obj[item]);
                }
                if(obj[item].surveyTakerAge == 'twentysix-thirty'){
                    surveyData.age26_30.push(obj[item]);
                }
            }
        }

        var age13Percentage = ((surveyData.age13_18.length / objCount) * 100).toFixed(1);
        var age19Percentage = ((surveyData.age19_25.length / objCount) * 100).toFixed(1);
        var age26Percentage = ((surveyData.age26_30.length / objCount) * 100).toFixed(1);
        $('.age13').html(age13Percentage + '%');
        $('.age19').html(age19Percentage + '%');
        $('.age25').html(age26Percentage + '%');
        
        var graphData = {
            datasets:[{
                label: 'Age Range of Survey Takers',
                data: [surveyData.age13_18.length, surveyData.age19_25.length, surveyData.age26_30.length],
                backgroundColor: [
                    '#f9796b',
                    '#ff9780',
                    '#0077b3'
                ]
            }],
            labels: [
                'Thirteen to Eighteen',
                'Nineteen to Twenty-five',
                'Twenty-six to Thirty'
            ]
        };
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        }

        var ctx = document.getElementById("age-range").getContext('2d');
        var ageChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });

    }

    function processEducationData(obj){
        var has = Object.prototype.hasOwnProperty;
        var objCount = Object.keys(obj).length;
        for (var item in obj){
            if(has.call(obj, item)){
                if(obj[item].surveyTakerEducation == 'jhs'){
                    surveyData.jhs.push(obj[item]);
                }

                if(obj[item].surveyTakerEduction == 'shs'){
                    surveyData.shs.push(obj[item]);
                }

                if(obj[item].surveyTakerEducation == 'university'){
                    surveyData.university.push(obj[item]);
                }

                if(obj[item].surveyTakerEducation == 'Postgraduate'){
                    surveyData.postgraduate.push(obj[item]);
                }
            }
        }
        var jhsPercentage = ((surveyData.jhs.length / objCount) * 100).toFixed(1);
        var shsPercentage = ((surveyData.shs.length / objCount) * 100).toFixed(1);
        var uniPercentage = ((surveyData.university.length / objCount) * 100).toFixed(1);
        var postGradPercentage = ((surveyData.postgraduate.length / objCount) * 100).toFixed(1);
        $('.jhsPerc').html(jhsPercentage + '%');
        $('.shsPerc').html(shsPercentage + '%');
        $('.uniPerc').html(uniPercentage + '%');
        $('.postgradPerc').html(postGradPercentage + '%');

        var graphData = {
            datasets:[{
                data: [surveyData.jhs.length, surveyData.shs.length, surveyData.university.length, surveyData.postgraduate.length],
                backgroundColor: [
                    '#0077b3',
                    '#67b8e0',
                    '#9cd8ef',
                    '#f9796b'
                ]
            }],
            labels:[
                'JHS',
                'SHS',
                'University',
                'Postgraduate'
            ]
        }
        var graphOptions = {
            hover: {
                mode: 'index'
            }
        }

        var ctx = document.getElementById('education-range').getContext('2d');
        var eduChart = new Chart(ctx, {
            type: 'doughnut',
            data: graphData,
            options: graphOptions
        });
    }

    getPublicsurveys();
    $('#sign-out').on('click', function(e) {
        e.preventDefault();
        service.signOut();
    });

}(jQuery))
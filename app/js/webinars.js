(function($) {
    'use strict';

    var webinarDataSet = null;
    var $errorWidget = $('#data-error');
    const errMsg = `There are no webinars available yet. Please click <a href="create_webinars.html">here</a> to create a webinar.`
    var $overviewModal = $('#webinarOverviewModal');
    var $webinarsCardMasonry= $('#webinar-masonry');
    var $progressBar = $('#loadprogress');

    // get webinars by date they were created
    function getWebinarsByDate(callback){
        $progressBar.removeClass('d-none');
        service.getWebinarsByDateCreated().then(function(snapshot){
            var dataset = snapshot.val();
            callback(dataset);
        }).catch(function(err){
            console.log(err);
        });
    }

    // individual card render function for webinars listings
    function webinarCardTemplate(id, obj){
        let html = `<div class="shadow-sm card" id=${id}>
        <img src='${obj.videoThumbnailURL}' class="card-img-top" alt='${obj.name}'>
        <div class="card-body">
          <h4 class="card-title webinar-card-title mt-3 mb-3">
            ${obj.name}
          </h4>
          <h6 class="card-subtitle mb-2 webinar-card-subtitle">
            Speaker(s): ${obj.speaker.name}
          </h6>
          <h6 class="card-subtitle mb-2 webinar-card-date">
            Date: ${unryddleApp.setRegularDate(obj.webinarDate)}
          </h6>
          <p class="card-text webinar-card-desc">
            ${obj.overview}
          </p>
          <button class="btn btn-start btn-block" type="button" id='wbn-${id}' name='wbn-${id}' data-id='${id}'>
            View More
          </button>
        </div>
      </div>`;

        return html;
    }

    function processWebinarArray(array){
      let listData = '';
      var reverseArr = array.reverse();
      for(var i = 0; i < reverseArr.length; i++){
        listData = listData.concat(webinarCardTemplate(reverseArr[i].id, reverseArr[i]));
      }      
      $webinarsCardMasonry.append($(listData));
      $progressBar.addClass('d-none');

    }

    function processWebinarsForRender(obj) {
        const has = Object.prototype.hasOwnProperty;
        let webinarArr = [];
        for(var webinar in obj){
            if(has.call(obj, webinar)){
                let activeWebinar = obj[webinar];
                let webinarID = webinar;
                activeWebinar.id = webinarID;
                webinarArr.push(activeWebinar);
            }
        }
        // console.log(webinarArr);
        processWebinarArray(webinarArr);
    }

    // load webinars data set and process for render
    getWebinarsByDate(function(data){
        if(data){
            webinarDataSet = data;
            processWebinarsForRender(webinarDataSet);
        } else {
            $errorWidget.html(errMsg);
            $errorWidget.removeClass('d-none');
            $progressBar.addClass('d-none');
        }
    });

    $(document).on('click', 'body .urWrapper #urSiteContent .dashWrapper .container-fluid .row.justify-content-between.mt-3 .card-columns .shadow-sm.card .card-body button', function(e) {
        e.preventDefault();
        $overviewModal.modal(unryddleApp.modalOptions);
    })
}(jQuery))
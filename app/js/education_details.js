    (function($) {
      'use strict';

      const educationDetailsPageData = {
        successMsg: 'Form data was successfully submitted',
      }

      const $addElectives = $('#addElectives');
      const $highSchoolForm = $('#highSchoolCourse');
      const $highSchoolCourseName = $('#high_sch_courseName');
      const $highSchoolCourseDesc = $('#high_sch_courseDesc');
      const $electivesName = $("input[name='electives[]']");
      const $electivesDesc = $("textarea[name='electives_desc[]']");
      const $hsSubmitBtn = $('#submit_highSchool');
      const $hsSuccess = $('#high_sch-success');
      const $hsError = $('#high_sch-error');
      const $hndCourseForm = $('#hndCourse');
      const $hndPrgName = $('#hnd_prg_name');
      const $hndPrgDesc = $('#hnd_prg_desc');
      const $hndSubjects = $("input[name='hndSubjects[]']");
      const $hndSubjectsDesc = $("textarea[name='hndSubjectsDesc[]']");
      const $hndSuccess = $('#hnd-success');
      const $hndError = $('#hnd-error');
      const $addSubjectsBtn = $('#addHndSubjects');
      const $submitHNDBtn = $('#submit_hnd');
      const $addMajorsBtn = $('#addUGMajors');
      const $ugCourseForm = $('#underGradCourse');
      const $ugPrgName = $('#ug_prg_name');
      const $ugPrgDesc = $('#ug_prg_desc');
      const $ugSuccess = $('#ug-success');
      const $ugError = $('#ug-error');
      const $ugCourseBtn = $('#submit_ug');
      const $ugMajors = $("input[name='ugMajors[]']");
      const $ugMajorsDesc = $("textarea[name='ugMajorsDesc[]']");
      const $addCertBtn = $('#addLCCourses');
      const $lcCourseForm = $('#licencesCertCourse');
      const $lcCourseName = $('#lc_name');
      const $lcCourseDesc = $('#lc_desc');
      const $lcSuccess = $('#lc-success');
      const $lcError = $('#lc-error');
      const $lcSubmitBtn = $('#submit_lc');
      const $gradCourseForm = $('#gradCourse');
      const $gradPrgName = $('#gd_prg_name');
      const $gradPrgDesc = $('#gd_prg_desc');
      const $addGradMajors = $('#addGDMajors');
      const $gradSuccess = $('#gd-success');
      const $gradError = $('#gd-error');
      const $submitGradBtn = $('#submit_gd');
      const $doctoralForm = $('#doctoralCourse');
      const $doctoralPrgName = $('#dc_prg_name');
      const $doctoralPrgDesc = $('#dc_prg_desc');
      const $doctoralSuccess = $('#dc-success');
      const $doctoralError = $('#dc-error');
      const $addDCMajorBtn = $('#addDCMajors');
      const $docSubmitBtn = $('#submit_dc');
      const $logout = $('#logout-action');

      function renderSuccessMsg(str, handler) {
        handler.text(str);
        handler.removeClass('d-none');
        setTimeout(function() {
          handler.addClass('d-none');
        }, 6000);
        $("input[type='text'], textarea, select").val('');
      }

      function renderErrorMsg(error, handler) {
        let str;
        if (error.code == "auth/user-not-found") {
          str = "User does not exist. Please ensure you have entered a valid email and password."
        } else if(error.Error == 'PERMISSION_DENIED') {
          str = "You do not have permission to upload content here."
        } else {
          str = "There was a problem uploading your data. Please try again in a few minutes"
        }
        handler.text(str);
        handler.removeClass('d-none');
        setTimeout(function() {
          handler.addClass('d-none');
        }, 6000);
        $("input[type='text'], textarea, select").val('');
      }

      function getAllElectives(obj1, obj2){
        let setElectives = [];
        for(let i=0; i<obj1.length; i++){
          for(let j=0; j<obj2.length; j++){
            if(i === j){
              if($(obj1[i]).val() !== '' && $(obj2[j]).val() !== ''){
                setElectives.push({electiveName: $(obj1[i]).val(), electiveDescription: $(obj2[j]).val()})
              }
            }
            // console.log( + " " + );
          }
        }
        return setElectives;
      }

      function getAllSubjects(obj1, obj2){
        let setSubjects = [];
        for(let i=0; i<obj1.length; i++){
          for(let j=0; j<obj2.length; j++){
            if(i === j){
              if($(obj1[i]).val() !== '' && $(obj2[j]).val() !== ''){
                setSubjects.push({subjectName: $(obj1[i]).val(), subjectDescription: $(obj2[j]).val()})
              }
            }
            // console.log( + " " + );
          }
        }
        return setSubjects;
      }

      function getAllMajors(obj1, obj2){
        let setMajors = [];
        for(let i=0; i<obj1.length; i++){
          for(let j=0; j<obj2.length; j++){
            if(i === j){
              if($(obj1[i]).val() !== '' && $(obj2[j]).val() !== ''){
                setMajors.push({majorName: $(obj1[i]).val(), majorDescription: $(obj2[j]).val()})
              }
            }
            // console.log( + " " + );
          }
        }
        return setMajors;
      }

      function getAllLCCourses(obj1, obj2){
        let setLCCourses = [];
        for(let i=0; i<obj1.length; i++){
          for(let j=0; j<obj2.length; j++){
            if(i === j){
              if($(obj1[i]).val() !== '' && $(obj2[j]).val() !== ''){
                setLCCourses.push({courseName: $(obj1[i]).val(), courseDescription: $(obj2[j]).val()})
              }
            }
          }
        }
        return setLCCourses;
      }

      function electiveFields(){
        let html = "<div class='col-6 col-xs-12 col-sm-12 col-md-6 hElective'>" +
        "<div class='form-group'>" +
        "<label>Elective</label>" +
        "<input type='text' name='electives[]' class='form-control'>" +
        "<small class='form-text text-muted'>Name of Elective Subject</small>" +
        "</div>" +
        "<div class='form-group'>" +
        "<label>Elective Description</label>" +
        "<textarea name='electives_desc[]' rows='5' class='form-control'></textarea>" +
        "<small class='form-text text-muted'>Full description of elective subject</small>" +
        "</div>" +
        "</div>";

        return html;
      }

      function subjectFields(){
        let html = "<div class='col-6 col-xs-12 col-sm-12 col-md-6 hndSubject'>" +
        "<div class='form-group'>" +
        "<label>Subject</label>" +
        "<input type='text' name='hndSubjects[]' class='form-control'>" +
        "<small class='form-text text-muted'>Name of programme Subject</small>" +
        "</div>" +
        "<div class='form-group'>" +
        "<label>Subject Description</label>" +
        "<textarea name='hndSubjectsDesc[]' rows='5' class='form-control'></textarea>" +
        "<small class='form-text text-muted'>Full description of programme subject</small>" +
        "</div>" +
        "</div>";

        return html;
      }

      function majorFields(){
        let html = "<div class='col-6 col-xs-12 col-sm-12 col-md-6 ugMajor'>" +
        "<div class='form-group'>" +
        "<label>Major</label>" +
        "<input type='text' name='ugMajors[]' class='form-control'>" +
        "<small class='form-text text-muted'>Name of programme major</small>" +
        "</div>" +
        "<div class='form-group'>" +
        "<label>Major Description</label>" +
        "<textarea name='ugMajorsDesc[]' rows='5' class='form-control'></textarea>" +
        "<small class='form-text text-muted'>Full description of programme major</small>" +
        "</div>" +
        "</div>";

        return html;
      }

      function lcFields(){
        let html = "<div class='col-6 col-xs-12 col-sm-12 col-md-6 lcCourse'>" +
        "<div class='form-group'>" +
        "<label>Course</label>" +
        "<input type='text' name='lcCourses[]' class='form-control'>" +
        "<small class='form-text text-muted'>Name of Certificate Course</small>" +
        "</div>" +
        "<div class='form-group'>" +
        "<label>Course Description</label>" +
        "<textarea name='lcCoursesDesc[]' rows='5' class='form-control'></textarea>" +
        "<small class='form-text text-muted'>Full description of certification course</small>" +
        "</div>" +
        "</div>";

        return html;
      }

      function gdFields(){
        let html = "<div class='col-6 col-xs-12 col-sm-12 col-md-6 gdMajor'>" +
        "<div class='form-group'>" +
        "<label>Major</label>" +
        "<input type='text' name='gdMajors[]' class='form-control'>" +
        "<small class='form-text text-muted'>Name of programme major</small>" +
        "</div>" +
        "<div class='form-group'>" +
        "<label>Major Description</label>" +
        "<textarea name='gdMajorsDesc[]' rows='5' class='form-control'></textarea>" +
        "<small class='form-text text-muted'>Full description of programme major</small>" +
        "</div>" +
        "</div>";

        return html;
      }

      function dcFields(){
        let html = "<div class='col-6 col-xs-12 col-sm-12 col-md-6 dcMajor'>" +
        "<div class='form-group'>" +
        "<label>Major</label>" +
        "<input type='text' name='dcMajors[]' class='form-control'>" +
        "<small class='form-text text-muted'>Name of programme major</small>" +
        "</div>" +
        "<div class='form-group'>" +
        "<label>Major Description</label>" +
        "<textarea name='dcMajorsDesc[]' rows='5' class='form-control'></textarea>" +
        "<small class='form-text text-muted'>Full description of programme major</small>" +
        "</div>" +
        "</div>";

        return html;
      }

      // sign out function
      function signOut() {
          service.logout().then(function () {
              unryddleApp.clearData();
              window.location.replace(service.baseURL);
          }).catch(function (error) {
              console.log(error);
          });
      }

      $(document).ready(function() {
        console.log('page ready...');

        // add form fields for high school electives
        $addElectives.on('click', function(e) {
          e.preventDefault();
          $('#highSchoolRow').append($(electiveFields()));
        });
        // validate highschool form
        $highSchoolForm.validate({
          rules: {
            high_sch_courseName: { required: true },
            high_sch_courseDesc: { required: true }
          }
        })
        // submit form data for high school section
        $highSchoolForm.on('submit', function(e) {
          e.preventDefault();
          let spinner = $hsSubmitBtn.find('i');
          spinner.removeClass('d-none');
          if($(this).valid()){
            let payload = {
              courseName: $highSchoolCourseName.val(),
              courseDesc: $highSchoolCourseDesc.val(),
              electives: getAllElectives($("input[name='electives[]']"), $("textarea[name='electives_desc[]']")),
              createdOn: firebase.database.ServerValue.TIMESTAMP
            }
            let hsKey = service.highSchoolCoursesRef.push().key;
            firebase.database().ref('high_school_courses/' + hsKey).set(payload, function(error){
              if(error){
                console.log(error);
                spinner.addClass('d-none');
                renderErrorMsg(error, $hsError);
              } else {
                spinner.addClass('d-none');
                renderSuccessMsg(educationDetailsPageData.successMsg, $hsSuccess);
              }
            });
          } else {
            spinner.addClass('d-none');
          }
        })

        // add subject fields for HND form
        $addSubjectsBtn.on('click', function(e) {
          e.preventDefault();
          $('#hndRow').append($(subjectFields()));
        });

        // validate HND form
        $hndCourseForm.validate({
          rules: {
            hnd_prg_name: { required: true },
            hnd_prg_desc: { required: true }
          }
        });
        // submit form data for HND
        $hndCourseForm.on('submit', function(e) {
          e.preventDefault();
          let spinner = $submitHNDBtn.find('i');
          spinner.removeClass('d-none');
          if($(this).valid()){
            let payload = {
              programmeName: $hndPrgName.val(),
              programmeDescription: $hndPrgDesc.val(),
              subjects: getAllSubjects($("input[name='hndSubjects[]']"), $("textarea[name='hndSubjectsDesc[]']")),
              createdOn: firebase.database.ServerValue.TIMESTAMP
            };
            // console.log(payload)
            let hndKey = service.hndCoursesRef.push().key;
            firebase.database().ref('hnd_courses/' + hndKey).set(payload, function(error){
              if(error){
                console.log(error);
                spinner.addClass('d-none');
                renderErrorMsg(error, $hndError);
              } else {
                spinner.addClass('d-none');
                renderSuccessMsg(educationDetailsPageData.successMsg, $hndSuccess);
              }
            });
          } else {
            spinner.addClass('d-none');
          }
        });

        // add major fields for UG form
        $addMajorsBtn.on('click', function(e) {
          e.preventDefault();
          $('#ugRow').append($(majorFields()));
        });

        // validate ug form data
        $ugCourseForm.validate({
          rules: {
            ug_prg_name: { required: true },
            ug_prg_desc: { required: true }
          }
        });

        // submit form data for UG
        $ugCourseForm.on('submit', function(e) {
          e.preventDefault();

          let spinner = $ugCourseBtn.find('i');
          spinner.removeClass('d-none');

          if($(this).valid()){
            let payload = {
              programmeName: $ugPrgName.val(),
              programmeDescription: $ugPrgDesc.val(),
              majors: getAllMajors($("input[name='ugMajors[]']"), $("textarea[name='ugMajorsDesc[]']")),
              createdOn: firebase.database.ServerValue.TIMESTAMP
            };
            // console.log(payload);
            let ugKey = service.underGradCoursesRef.push().key;
            firebase.database().ref("undergraduate_courses/" + ugKey).set(payload, function(error){
              if(error){
                console.log(error);
                spinner.addClass('d-none');
                renderErrorMsg(error, $ugError);
              } else {
                spinner.addClass('d-none');
                renderSuccessMsg(educationDetailsPageData.successMsg, $ugSuccess);
              }
            });
          } else {
            spinner.addClass('d-none')
          }
        });

        // add Certificate fields
        $addCertBtn.on('click', function(e) {
          e.preventDefault();

          $('#lcRow').append($(lcFields()));
        });

        // validate certificate form
        $lcCourseForm.validate({
          rules: {
            lc_name: { required: true },
            lc_desc: { required: true }
          }
        });

        // submit licences and certification data
        $lcCourseForm.on('submit', function(e) {
          e.preventDefault();

          let spinner = $lcSubmitBtn.find('i');
          spinner.removeClass('d-none');

          if($(this).valid()){
            let payload = {
              certificateName: $lcCourseName.val(),
              certficateDescription: $lcCourseDesc.val(),
              courses: getAllLCCourses($("input[name='lcCourses[]']"), $("textarea[name='lcCoursesDesc[]']")),
              createdOn: firebase.database.ServerValue.TIMESTAMP
            };
            // console.log(payload);
            let lcKey = service.certCoursesRef.push().key;
            firebase.database().ref("certification_courses/" + lcKey).set(payload, function(error) {
              if(error){
                console.log(error);
                spinner.addClass('d-none');
                renderErrorMsg(error, $lcError);
              } else {
                spinner.addClass('d-none');
                renderSuccessMsg(educationDetailsPageData.successMsg, $lcSuccess);
              }
            });
          } else {
            spinner.addClass('d-none');
          }
        });

        // add form fields for graduate courses
        $addGradMajors.on('click', function(e) {
          e.preventDefault();

          $('#gdRow').append($(gdFields()));
        })

        // validate grad course form
        $gradCourseForm.validate({
          rules: {
            gd_prg_name: { required: true },
            gd_prg_desc: { required: true }
          }
        });

        // submit grad form
        $gradCourseForm.on('submit', function(e) {
          e.preventDefault();
          let spinner = $submitGradBtn.find('i');
          spinner.removeClass('d-none');

          if($(this).valid()){
            let payload = {
              programmeName: $gradPrgName.val(),
              programmeDescription: $gradPrgDesc.val(),
              majors: getAllMajors($("input[name='gdMajors[]']"), $("textarea[name='gdMajorsDesc[]']")),
              createdOn: firebase.database.ServerValue.TIMESTAMP
            };
            // console.log(payload);
            let gdKey = service.gradDegreesRef.push().key;
            firebase.database().ref("graduate_degree_courses/" + gdKey).set(payload, function(error){
              if(error){
                console.log(error);
                spinner.addClass('d-none');
                renderErrorMsg(error, $gradError);
              } else {
                spinner.addClass('d-none');
                renderSuccessMsg(educationDetailsPageData.successMsg, $gradSuccess);
              }
            });
          } else {
            spinner.addClass('d-none');
          }
        });

        // add doctoral course fields
        $addDCMajorBtn.on('click', function(e) {
          e.preventDefault();

          $('#dcRow').append($(dcFields()));
        });

        // validate doctoral form
        $doctoralForm.validate({
          rules: {
            dc_prg_name: { required: true },
            dc_prg_desc: { required: true }
          }
        });

        // submit doctoral form
        $doctoralForm.on('submit', function(e) {
          e.preventDefault();
          let spinner = $docSubmitBtn.find('i');
          spinner.removeClass('d-none');

          if($(this).valid()){
            let payload = {
              programmeName: $doctoralPrgName.val(),
              programmeDescription: $doctoralPrgDesc.val(),
              majors: getAllMajors($("input[name='dcMajors[]']"), $("textarea[name='dcMajorsDesc[]']")),
              createdOn: firebase.database.ServerValue.TIMESTAMP
            }
            console.log(payload);
            let dcKey = service.doctoralCoursesRef.push().key;
            firebase.database().ref("doctoral_courses/" + dcKey).set(payload, function(error){
              if(error){
                console.log(error);
                spinner.addClass('d-none');
                renderErrorMsg(error, $doctoralError);
              } else {
                spinner.addClass('d-none');
                renderSuccessMsg(educationDetailsPageData.successMsg,$doctoralSuccess);
              }
            })
          } else {
            spinner.addClass('d-none');
          }
        });

        // logout action
        $logout.on('click', function (e) {
            e.preventDefault();
            console.log('button works');
            signOut();
        });

      });
    }(jQuery))

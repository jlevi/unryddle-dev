(function($) {
    'use strict';

    const userData = unryddleApp.loadData('VIEW_USER');
    const userID = unryddleApp.loadData('VIEW_USER_ID');

    function viewUserInfo(){
        console.log(userData.fullName);
        $('#deleteUser').attr('data-id', userID);
        $('.emailAddress').text(userData.email);
        if(userData.fullName === undefined){
            $('.accountName').text('Not Available');
        } else {
            $('.accountName').text(userData.fullName).css({'text-transform': 'capitalize' });
        }
        
        if(userData.role === undefined) {
            $('.accountRole').text('This user has not been assigned a role yet');
        } else {
            $('.accountRole').text(userData.role).css({'text-transform': 'capitalize'});
        }

        if(userData.phoneNumber === undefined) {
            $('.phoneNumber').text('N/A');
        } else {
            $('.phoneNumber').text(userData.phoneNumber);
        }

        if(userData.locationAddress === undefined) {
            $('.locAddress').text('N/A');
        } else {
            $('.locAddress').text(userData.locationAddress);
        }

        if(userData.bankData === undefined) {
            $('.bankInfo').text('N/A');
        } else {
            $('.bankInfo').text(userData.bankData);
        }

        if(userData.bioData === undefined) {
            $('.bioInfo').text('This user has not updated their bio information'); 
        } else {
            $('.bioInfo').text(userData.bioData);
        }
    }

    $('#edit-profile').on('click', function(e) {
        e.preventDefault();

        window.location.href = `edit_profile.html?userID=${userID}`;
    });

    viewUserInfo();
}(jQuery))
(function($) {
    'use strict';

    let quoteSuccessMsg = 'Your quote was successfully added.';

    const $logout = $('#logout-action');
    const quoteForm = document.getElementById('newQuoteForm');
    const authorName = document.getElementById('quote_author_name');
    const authorProfession = document.getElementById('quote_author_profession');
    const quoteStatememnt = document.getElementById('quote_statement');
    const quoteSuccess = document.getElementById('quote-success');
    const quoteError= document.getElementById('quote-error');
    const quoteSubmitBtn = document.getElementById('submit_quote');
    const loadprogress  = document.getElementById('loadprogress');
    const quoteWrapper = document.getElementById('quotesGroup');

    /**
     * component to edit quotes - add later to list template function
     * <a href='#' id='view-user' class='user-action' data-id=${id}>
            <i class='fa fa-eye'></i>
        </a>
     */

    function quotesListTemplate(id, obj){

        let html = `<div class='list-group-item list-group-item-action flex-column align-items-start' id=${id}>
            <div class='row w-100'>
            <div class='col-2 text-right item-label'>Author:</div>
            <div class='col-8 user_name'> ${obj.author}</div>
            <div class='col-2'>
            
            </div>
            </div>
            <div class='row w-100'>
            <div class='col-2 text-right item-label'>Profession:</div>
            <div class='col-10 user_email'>${obj.profession}</div>
            </div>
            <div class='row w-100'>
            <div class='col-2 text-right item-label'>Quote:</div>
            <div class='col-10 user_role'>${obj.quote}</div>
            </div>
            </div>`;
        
            return html;

    }

    function renderSuccessMsg(str, handler) {
        handler.text(str);
        handler.removeClass('d-none');
        $('input, textarea').val('');
    }
    
    function renderErrorMsg(error, handler) {
        let str;
        if (error.code == "auth/user-not-found") {
          str = "User does not exist. Please ensure you have entered a valid email and password."
        } else {
          str = "There was a problem uploading your data. Please try again in a few minutes"
        }
        handler.text(str);
        handler.removeClass('d-none');
        $('input, textarea').val('');
    }

    // process quotes
    function processQuotes(obj){
        console.log(obj);
        $(loadprogress).addClass('d-none');
        $(quoteWrapper).html('');
        for(let quote in obj){
            if(obj.hasOwnProperty(quote)){
                let singleQuote = obj[quote];
                let quoteID = quote;

                $(quoteWrapper).append($(quotesListTemplate(quoteID, singleQuote)));
            }
        }
    }
    
    // function get quotes
    function getQuotes() {
        $(loadprogress).removeClass('d-none');
        service.quotesRef.on('value', function(snapshot){
            let quotesObj = snapshot.val();
            processQuotes(quotesObj);
        });
    }

    function submitQuote(spinner, obj){
        // console.log(obj);
        let quoteKey = service.quotesRef.push().key;
        firebase.database().ref('app_quotes/' + quoteKey).set(obj, function(error) {
            if (error) {
                $(spinner).addClass('d-none');
                renderErrorMsg(error, $(quoteError));
              } else {
                $(spinner).addClass('d-none');
                renderSuccessMsg(quoteSuccessMsg, $(quoteSuccess));
              }
        })
    }

    function signOut() {
        service.logout().then(function () {
          unryddleApp.clearData();
          window.location.replace(service.baseURL);
        }).catch(function (error) {
          console.log(error);
        });
    }

    getQuotes();

    $(document).ready(function() {

        // logout function
        $logout.on('click', function(e) {
            e.preventDefault();
            signOut();
        });

        // validate quote form
        $(quoteForm).validate({
            rules: {
                quote_author_name: { required: true },
                quote_author_profession: { required: true },
                quote_statement: { required: true }
            }
        });

        // submit quotes form
        $(quoteForm).on('submit', function(e) {
            e.preventDefault();

            let $this = $(this);
            let loader = $(quoteSubmitBtn).find('i');
            let quotePayload = {};
            
            if($this.valid()){
                $(loader).removeClass('d-none');
                quotePayload.author = $(authorName).val();
                quotePayload.profession = $(authorProfession).val();
                quotePayload.quote = $(quoteStatememnt).val();
                quotePayload.createdOn = firebase.database.ServerValue.TIMESTAMP;

                submitQuote(loader, quotePayload);
            } 
        })

    });
}(jQuery));
(function($){
    'use strict';

    // validate questions forms
    $jhsQuestionForm.validate({
        rules: {
          jhs_question_name: {
            required: true
          },
          jhsQuestionOptions: {
            required: true
          }
        }
      });
  
      // validate shs form
      $shsQuestionForm.validate({
        rules: {
          shs_question_name: {
            required: true
          },
          shsQuestionOptions: {
            required: true
          }
        }
      });
  
      $uniQuestionForm.validate({
        rules: {
          uni_question_name: {
            required: true
          },
          uniQuestionOptions: {
            required: true
          }
        }
      });

      $jhsQuestionForm.on('submit', function (e) {
        e.preventDefault();
        let spinner = $(this).find('button i');
        let jhsPayload, personality;
        spinner.removeClass('d-none');
        if ($(this).valid()) {
  
          jhsPayload = {
            question: $jhsQuestionTitle.val(),
            education_level: "jhs",
            personality_type: getRadioVals($("input[name='jhsQuestionOptions']")),
            createdOn: firebase.database.ServerValue.TIMESTAMP
          }
  
          let jhsQuestionsKey = service.raisecQuestionsRef.push().key;
          let jhsUpdateData = {};
          jhsUpdateData['app_questions/' + jhsQuestionsKey] = jhsPayload;
          jhsUpdateData['raisec_questions/' + jhsQuestionsKey] = jhsPayload;
  
          firebase.database().ref().update(jhsUpdateData, function (error) {
            if (error) {
              spinner.addClass('d-none');
              // $('input[name=jhsQuestionOptions]').attr('checked', false);
              renderErrorMsg(error, $jhsQuestionError);
            } else {
              spinner.addClass('d-none');
              // $('input[name=jhsQuestionOptions]').attr('checked', false);
              renderSuccessMsg(raisecQuestionsData.questionSuccessMsg, $jhsQuestionSuccess);
            }
          });
        }
      });

      $shsQuestionForm.on('submit', function (e) {
        e.preventDefault();
        let spinner = $(this).find('button i');
        spinner.removeClass('d-none')
        if ($(this).valid()) {
          let shsPayload = {
            question: $shsQuestionTitle.val(),
            personality_type: getRadioVals($("input[name='shsQuestionOptions']")),
            education_level: "shs",
            createdOn: firebase.database.ServerValue.TIMESTAMP
          }
  
          let shsQuestionsKey = service.raisecQuestionsRef.push().key;
          let shsUpdateData = {};
          shsUpdateData['app_questions/' + shsQuestionsKey] = shsPayload;
          shsUpdateData['raisec_questions/' + shsQuestionsKey] = shsPayload;
  
          firebase.database().ref().update(shsUpdateData, function (error) {
            if (error) {
              spinner.addClass('d-none');
              // $('input[name=shsQuestionOptions]').prop('checked', false);
              renderErrorMsg(error, $shsQuestionError);
            } else {
              spinner.addClass('d-none');
              // $('input[name=shsQuestionOptions]').prop('checked', false);
              renderSuccessMsg(raisecQuestionsData.questionSuccessMsg, $shsQuestionSuccess);
            }
          })
        }
      });

      $uniQuestionForm.on('submit', function (e) {
        e.preventDefault();
        let spinner = $(this).find('button i');
        spinner.removeClass('d-none')
        if ($(this).valid()) {
          let uniPayload = {
            question: $uniQuestionTitle.val(),
            personality_type: getRadioVals($("input[name='uniQuestionOptions']")),
            education_level: "university",
            createdOn: firebase.database.ServerValue.TIMESTAMP
          }
  
          let uniQuestionsKey = service.raisecQuestionsRef.push().key;
          let uniUpdateData = {};
          uniUpdateData['app_questions/' + uniQuestionsKey] = uniPayload;
          uniUpdateData['raisec_questions/' + uniQuestionsKey] = uniPayload;
  
          firebase.database().ref().set(uniUpdateData, function (error) {
            if (error) {
              spinner.addClass('d-none');
              $('input[name=uniQuestionOptions]').prop('checked', false);
              renderErrorMsg(error, $uniQuestionError)
            } else {
              spinner.addClass('d-none');
              $('input[name=uniQuestionOptions]').prop('checked', false);
              renderSuccessMsg(raisecQuestionsData.questionSuccessMsg, $uniQuestionSuccess);
            }
          })
        }
      });
}(jQuery))
(function ($) {
  'use strict';

  var $email = $('#email'),
    $password = $('#password'),
    $loginEmail = $('#login_email'),
    $loginPassword = $('#login_password'),
    $loginForm = $('#loginUser'),
    $createUserForm = $('#createUser'),
    $facebookBtn = $('#sign-up-facebook'),
    $googleBtn = $('#sign-up-google'),
    $emailBtn = $('#sign-up-btn'),
    $loginBtn = $('#login-btn'),
    $loginSuccess = $('#login-success'),
    $loginError = $('#login-error'),
    $successMsg = $('#success-alert'),
    $errMsg = $('#error-alert'),
    $verifyEmail = $('#verify-email'),
    $forgotPasswordLink = $('#forgot_password'),
    $forgotPassModal = $('#forgotPasswordModal'),
    $pwResetForm = $('#passwordResetForm'),
    $pwResetEmail = $('#reset_email'),
    $pwResetBtn = $('#reset_password'),
    $pwResetSuccess = $('#resetPW-success'),
    $pwResetError = $('#resetPW-error');

  // user sign up/ create account function
  function signUp(payload) {
    service.createUser(payload).then(function (user) {
      $emailBtn.find('span').addClass('d-none');
      $successMsg.removeClass('d-none');
    }).catch(function (error) {
      $errMsg.text(error);
      $errMsg.removeClass('d-none');
    })
  }

  // process logged in user data
  function processUser(data) {
    console.log(data);
    let exists;
    let uID = data.uid;
    let rootRef = firebase.database().ref();
    let usersRef = rootRef.child('users');
    usersRef.child(uID).once('value', function (snapshot) {
      exists = (snapshot.val() !== null);
      if (exists) {
        console.log('user exists -> redirect to dashboard immediately');
        unryddleApp.storeData(unryddleApp.userData, snapshot.val());
        var successText = "Login successful. Redirecting..."
        $loginBtn.find('span').addClass('d-none');
        renderSuccessMsg(successText, $loginSuccess);
        window.location.replace('admin/welcome.html');
      } else {
        console.log("user don't exist -> create entry");
        if (unryddleApp.adminEmails.includes(data.email)) {
          usersRef.child(uID).set({
            email: data.email,
            profilePicture: data.photoURL,
            role: "admin",
            createdOn: firebase.database.ServerValue.TIMESTAMP
          }, function (error) {
            if (!error) {
              console.log('no error');
              var successText = "Login successful. Redirecting..."
              $loginBtn.find('span').addClass('d-none');
              renderSuccessMsg(successText, $loginSuccess);
              window.location.replace('admin/welcome.html');
            } else {
              console.log("Write failed" + error);
              renderErrorMsg(error, $loginError)
            }
          })

        } else {
          console.log('only admins can create new users');
          // renderErrorMsg(error, $loginError)
        }

      }
    });

  }

  // success msg
  function renderSuccessMsg(str, handler) {
    handler.text(str);
    handler.removeClass('d-none');
  }

  function renderErrorMsg(error, handler) {
    var str;
    if (error.code == "auth/user-not-found") {
      str = "User does not exist. Please ensure you have entered a valid email and password."
    } else {
      str = "There was a problem logging in. Please try again in a few minutes"
    }
    handler.text(str);
    handler.removeClass('d-none');
  }

  // user email verification
  // function verifyUserEmail(){
  //   service.verifyEmail().then(function() {
  //     $successMsg.html('');
  //     $successMsg.text('Please log in to your email account to complete verification');
  //   }).catch(function(error) {
  //     $errMsg.html('');
  //     $errMsg.text(error);
  //     $errMsg.removeClass('d-none');
  //   })
  // }

  //login function
  function loginUser(payload) {
    service.login(payload).then(function (user) {
      unryddleApp.storeData(unryddleApp.userAuthData, user);
      processUser(user);
    }).catch(function (error) {
      console.log(error)
      $loginBtn.find('span').addClass('d-none');
      renderErrorMsg(error, $loginError)
    })
  }

  $(document).ready(function () {

    // validate create form
    $createUserForm.validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 8
        }
      },
      messages: {
        email: {
          required: "you need to enter an email to proceed",
          email: "please make sure you have entered a valid email"
        },
        password: {
          required: "you need a password to proceed",
          minlength: "your password should have at least 8 characters"
        }
      }
    });

    // validate login form
    $loginForm.validate({
      rules: {
        login_email: {
          required: true,
          email: true
        },
        login_password: {
          required: true,
          minlength: 8
        }
      }
    });

    // validate reset form
    $pwResetForm.validate({
      rules: {
        reset_email: {
          required: true,
          email: true
        }
      }
    });

    // submit login data
    $loginForm.on('submit', function (e) {
      e.preventDefault();
      $loginBtn.find('span').removeClass('d-none');
      if ($(this).valid()) {
        var payload = {
          email: $loginEmail.val(),
          password: $loginPassword.val()
        };

        loginUser(payload);
      }
    });

    // submit reset form data for password change
    $pwResetForm.on('submit', function(e) {
      e.preventDefault();
      let spinner = $pwResetBtn.find('i');
      spinner.removeClass('d-none');
      if($(this).valid()) {
        let email = $pwResetEmail.val();
        service.sendResetEmail(email).then(function() {
          spinner.addClass('d-none');
          let msg = "Reset info was successfully sent to your email address:<strong>" + email + "</strong>. Check your email inbox for a link to reset your email.";
          $pwResetSuccess.html(msg);
          $pwResetSuccess.removeClass('d-none');
        }).catch(function(error) {
          // console.log(error)
          let code = error.code;
          if(code == "auth/user-not-found") {
            let msg = "No registered user with the email: <strong>" + email + "</strong> does not exist. Please try again, or contact the admin to create an account for you";
            $pwResetError.html(msg);
            $pwResetError.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }
    })

    // create user - email/Password
    $createUserForm.on('submit', function (e) {
      e.preventDefault();
      $emailBtn.find('span').removeClass('d-none');
      if ($(this).valid()) {
        var payload = {
          email: $email.val(),
          password: $password.val()
        }
        signUp(payload);
      } else {
        $emailBtn.find('span').addClass('d-none');
      }
    });

    $forgotPasswordLink.on('click', function(e) {
      e.preventDefault();

      $forgotPassModal.modal(unryddleApp.modalOptions);
    })

    // clear input field and alert boxes on modal hide
    $forgotPassModal.on('hide.bs.modal', function(e) {
      $pwResetForm.find('input, textarea').val('');

      if(!$pwResetSuccess.hasClass('d-none')){
        $pwResetSuccess.addClass('d-none');
      }

      if(!$pwResetError.hasClass('d-none')){
        $pwResetError.addClass('d-none');
      }
    });

    // clear alert messages if input changes on modal
    $pwResetEmail.on('change keyup keydown paste click', function(e) {
      if(!$pwResetSuccess.hasClass('d-none')){
        $pwResetSuccess.addClass('d-none');
      }

      if(!$pwResetError.hasClass('d-none')) {
        $pwResetError.addClass('d-none');
      }
    })
    // verify email account
    // $verifyEmail.on('click', function(e) {
    //   e.preventDefault;
    //   verifyUserEmail();
    // })
  });
}(jQuery))
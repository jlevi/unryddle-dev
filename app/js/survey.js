(function($) {
  'use strict';

  var surveyPageData = {

  }

  var $surveyForm = $('#studentSurvey'),
  $studentName = $('#studentName'),
  $studentYear = $('#studentYear'),
  $studentCourse = $('#studentCourse'),
  $studentPersonality = $('#studentPersonality'),
  $studentPersonalityTraits = $('#studentPersonalityTraits'),
  $studentEmail = $('#studentEmail'),
  $studentCareerList = $('#studentCareerList'),
  $studentHobbyList = $('#studentHobbyList'),
  $successMsg = $('#survey-success'),
  $errMsg = $('#survey-error'),
  $surveyListGroup = $('#surveyGroup'),
  $loadprogress = $('#loadprogress'),
  $viewUserForm = $('#viewUser'),
  $viewUserModal = $('#viewUserModalCenter'),
  $logout = $('#logout-action'),
  $surveyBtn = $('#submit_survey_btn');

  // success msg
  function renderSuccessMsg(str, handler) {
    handler.text(str);
    handler.removeClass('d-none');
    $('input, textarea').val('');
  }

  function signOut() {
    service.logout().then(function () {
      unryddleApp.clearData();
      window.location.replace(service.baseURL);
    }).catch(function (error) {
      console.log(error);
    });
  }


  function renderErrorMsg(error, handler) {
    var str;
    if (error.code == "auth/user-not-found") {
      str = "User does not exist. Please ensure you have entered a valid email and password."
    } else {
      str = "There was a problem uploading your data. Please try again in a few minutes"
    }
    handler.text(str);
    handler.removeClass('d-none');
    $('input, textarea').val('');
  }

  function renderSurveyList(id, data){

    var html = "<div class='list-group-item list-group-item-action flex-column align-items-start' id='" + id + "'" + ">" +
    "<div class='row w-100'>" +
    "<div class='col-2 text-right item-label'>" + "Student Name:" + "</div>" +
    "<div class='col-8 user_name'>" + data.studentName + "</div>" +
    "<div class='col-2'>" +
    "<a href='#' id='view-user' class='user-action' data-id='" + id + "'" + ">" +
    "<i class='fas fa-eye'></i>" +
    "</a>" +
    // "<a href='#' id='delete-user' class='user-action' data-id='" + id + "'" + ">" +
    // "<i class='fa fa-trash'></i>" +
    // "</a>" +
    "</div>" +
    "</div>" +
    "<div class='row w-100'>" +
    "<div class='col-2 text-right item-label'>" + "Year/Level:" + "</div>" +
    "<div class='col-10 user_email'>" + data.studentYear + "</div>" +
    "</div>" +
    "<div class='row w-100'>" +
    "<div class='col-2 text-right item-label'>" + "Course:" + "</div>" +
    "<div class='col-10 user_role'>" + data.studentCourse + "</div>" +
    "</div>" +
    "</div>";

    return html;
  }

  // process user list
  function processUserData(users){
    // console.log(users);
    $loadprogress.addClass('d-none');
    for(var user in users) {
      if(users.hasOwnProperty(user)) {
        var singleUser = users[user];
        var userID = user;

        $surveyListGroup.append($(renderSurveyList(userID, singleUser)));

      }
    }
  }


  // get all student surveys
  function getAllStudentSurveys() {
    $loadprogress.removeClass('d-none')
    service.getSurveys().then(function(snapshot){
      var userlist = snapshot.val();
      processUserData(userlist);
    }).catch(function(error){
      console.log(error);
    })
  }

  function populateUserViewForm(id, data){
    $loadprogress.addClass('d-none');
    $('.viewUserName').text(data.studentName);
    $studentName.val(data.studentName).attr('id', id);
    $studentEmail.val(data.studentEmail);
    $studentYear.val(data.studentYear);
    $studentCourse.val(data.studentCourse);
    $studentPersonality.val(data.studentPersonality);
    $studentHobbyList.val(data.hobby_list);
    $studentPersonalityTraits.val(data.personalityTraits);
    $studentCareerList.val(data.career_list);
  }

  // function to handle viewing individual student data
  function processAction(event) {
    $loadprogress.removeClass('d-none');
    var target = event.target;
    console.log('button clicked: ' + target);
    if(target.tagName === 'path' || target.tagName === 'svg'){
      var targetID = $(target).closest('a').attr('data-id');
      if($(target).closest('a').attr('id') === 'view-user') {
        service.studentSurveysRef.child(targetID).once('value', function(snapshot){
          var survey_data = snapshot.val();
          $viewUserForm.find('input, select').val('');
          populateUserViewForm(targetID, survey_data);
          // console.log(survey_data);
          $loadprogress.addClass('d-none');
        });
        $viewUserModal.modal(unryddleApp.modalOptions);
      }
    }
  }


  $(document).ready(function() {
    console.log('page ready');

    getAllStudentSurveys();

    $surveyForm.validate({
      rules: {
        studentName: { required: true },
        studentYear: { required: true },
        studentCourse: { required: true },
        studentPersonality: { required: true },
        studentCareerList: { required: true },
        studentHobbyList: { required: true },
        submit_survey_btn: { required: true },
        studentEmail: { required: true, email: true },
        studentPersonalityTraits: { required: true }
      }
    });

    // submit survey form
    $surveyForm.on('submit', function(e) {
      e.preventDefault();
      $(this).find('button i.fa-spin').removeClass('d-none');
      if($(this).valid()) {
        var payload = {
          studentName: $studentName.val(),
          studentYear: $studentYear.val(),
          studentCourse: $studentCourse.val(),
          studentPersonality: $studentPersonality.val(),
          career_list: ($studentCareerList.val()).split(','),
          hobby_list: ($studentHobbyList.val()).split(','),
          personalityTraits: ($studentPersonalityTraits.val()).split(','),
          studentEmail: $studentEmail.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        }

        var surveyKey = service.studentSurveysRef.push().key;
        var surveyData = {};
        surveyData['/student_surveys/' + surveyKey] = payload;

        firebase.database().ref().update(surveyData, function(error) {
          if(error){
            $surveyBtn.find('i').addClass('d-none');
            console.log(error);
            renderErrorMsg(err, $errMsg);
          } else {
            $surveyBtn.find('i').addClass('d-none');
            var successStr = "Your form has been successfully submitted. Thank you!";
            renderSuccessMsg(successStr, $successMsg);
          }
        })

        // console.table(payload);
      }
    })

    // click view action
    // user action click event(edit/delete)
    $(document).on('click', '.urWrapper #urSiteContent .dashWrapper .row .col-12 .shadow-sm.card .card-body .container-fluid .row .col-12 .list-group .list-group-item .row .col-2 a', processAction);

    // logout event
    $('#sign-out').on('click', function(e) {
      e.preventDefault();
      service.signOut();
    });
  });
}(jQuery))

(function ($) {
    'use strict';

    var welcomePageData = {
        userData: unryddleApp.loadData(unryddleApp.userData),
        verificationSuccess: "Verification was successfully sent."
    }

    var $authContent = $('#authContent');
    var $verifyEmailMsg = $('#verification-error');
    var $successMsg = $('#success-alert');
    var $errMsg = $('#error-alert');
    var $welcomeBlock = $('.welcomeTextMain');
    var $verificationBlock = $('.verTextMain');
    var $verificationButton = $('.verificationButton');
    var $currentUserEmail = $('#user-email');
    var $resendVerification = $('#resendVerification');

    // check that user has logged in before loading this page
    function checkUserStatus() {
        let rootRef = firebase.database().ref();
        let usersRef = rootRef.child('users');
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {

                $authContent.removeClass('d-none');

                if (welcomePageData.userData === null || welcomePageData === undefined) {
                    usersRef.child(user.uid).once('value', function (snapshot) {
                        unryddleApp.storeData(unryddleApp.userData, snapshot.val());
                        welcomePageData.userData = snapshot.val();
                    });
                }
            } else {
                window.location.replace(service.baseURL);
            }
        })
    }

    // user email verification
    function verifyUserEmail() {
        firebase.auth().onAuthStateChanged(function(user) {
            if(user) {
                user.sendEmailVerification().then(function() {
                    $verifyEmailMsg.addClass('d-none');
                    $verificationBlock.find('strong').text(user.email);
                    $verificationBlock.removeClass('d-none');
                    $verificationButton.removeClass('d-none');
                    $successMsg.text(welcomePageData.verificationSuccess);
                    $successMsg.removeClass('d-none');
                }).catch(function(err) {
                    $verifyEmailMsg.addClass('d-none');
                    $errMsg.text(err.message);
                    $errMsg.removeClass('d-none');
                })
            }
        });
    }

    // sign out function
    function signOut() {
        service.logout().then(function () {
            unryddleApp.clearData();
            window.location.replace(service.baseURL);
        }).catch(function (error) {
            console.log(error);
        });
    }

    function listRecentCareerProfiles(obj){   
        var has = Object.prototype.hasOwnProperty;
        var count = 0;
        var listData = '';
        for(var item in obj){
            if (has.call(obj, item)){
                count++;
                listData = listData.concat(
                    `<li id="${item}">
                    <div class="bg-light-info"><div class="count">${count}</div></div>
                    ${obj[item].name}. <span class="text-muted">${unryddleApp.convertEpoch(obj[item].createdOn)}</span>
                    </li>`
                );
            }
        }
        $('#loadprogress').addClass('d-none');
        $('#careerProfileFeed').html(listData);
    }

    function listRecentRIASECMarkers(obj){
        var has = Object.prototype.hasOwnProperty;
        var count = 0;
        // var personalityArr = [];
        var listData = '';
        for (var item in obj){
            if(has.call(obj, item)){
                count++;

                listData = listData.concat(
                    `<li id="${item}">
                    <div class="bg-light-info"><div class="count">${count}</div></div>
                    ${obj[item].question}. <span class="text-muted">${obj[item].personality_type}</span>
                    </li>`
                );
            }
        }
        // $('#riasecMarkersFeed').html(listData);
    }

    function retrieveTestsTakenCount() {
        $('#loadprogress').removeClass('d-none');
        service.testsTakenRef.on('value', function(snapshot){
            let testCount = 0;
            // console.log(snapshot.val());
            snapshot.forEach(function(){
                testCount++;
            });
            unryddleApp.storeData('TestCount', testCount);
            $('.testsTaken').text(testCount);  
        });
    }

    function retrieveStudentSurveyCount() {
        service.studentSurveysRef.on('value', function(snapshot) {
            let count = 0;
            snapshot.forEach(function() {
                count++;
            });
            unryddleApp.storeData('StudentSurveys', count);
            $('.studentSurveys').text(count);
        })
    }

    function retrievePublicSurveyCount() {
        service.publicSurveyRef.on('value', function(snapshot) {
            let count = 0;
            snapshot.forEach(function(){
                count++;
            });
            unryddleApp.storeData('PublicSurveys', count);
            $('.publicSurveys').text(count);
        });
    }

    function retrieveCorporateSurveyCount() {
        service.corporateSurveyRef.on('value', function(snapshot) {
            let count = 0;
            snapshot.forEach(function() {
                count++;
            });
            unryddleApp.storeData('CorpSurveys', count);
            $('.corporateSurveys').text(count);
        });
    }

    function retrieveRecentRIASECMarkers() {
        service.raisecQuestionsRef.limitToLast(10).on('value', function(snapshot) {
            var data = snapshot.val();
            $('#riasecMarkersFeed').html('');
            unryddleApp.storeData('DashboardMarkers', data);
            listRecentRIASECMarkers(data);
        })
    }

    function retrieveRecentCareerProfiles() {
        service.careerProfilesRef.limitToLast(10).on('value', function(snapshot){
            // console.log(snapshot.val());
            var data = snapshot.val();
            $('#careerProfileFeed').html('');
            // $('#loadprogress').addClass('d-none');
            unryddleApp.storeData('DashboardCProfiles', data);
            listRecentCareerProfiles(data);
        });
    }

    function loadSummaryData() {
        $('#loadprogress').removeClass('d-none');
        var testsTaken = unryddleApp.loadData('TestCount') ? unryddleApp.loadData('TestCount') : 0;
        var publicSurveyCount = unryddleApp.loadData('PublicSurveys') ? unryddleApp.loadData('PublicSurveys') : 0;
        var corpSurveyCount = unryddleApp.loadData('CorpSurveys') ? unryddleApp.loadData('CorpSurveys') : 0;
        var studentSurveyCount = unryddleApp.loadData('StudentSurveys') ? unryddleApp.loadData('StudentSurveys') : 0;
        var recentCareerProf = unryddleApp.loadData('DashboardCProfiles');
        var recentMarkers = unryddleApp.loadData('DashboardMarkers');

        $('.testsTaken').text(testsTaken);
        $('.studentSurveys').text(studentSurveyCount);
        $('.publicSurveys').text(publicSurveyCount);
        $('.corporateSurveys').text(corpSurveyCount);
        // $('#loadprogress').addClass('d-none');
        listRecentRIASECMarkers(recentMarkers);
        listRecentCareerProfiles(recentCareerProf);
    }

    function viewSelectedProfile(e) {
        $('#loadprogress').removeClass('d-none');
        var target = e.target;
        console.log(target);
        if(target.tagName === 'LI'){
            var id = $(target).attr('id');
            // get item details and load and store in single obj
            service.careerProfilesRef.child(id).once('value', function(snapshot){
                var obj = snapshot.val();
                unryddleApp.storeData("selectedCareerProfile", obj);
                unryddleApp.storeData("selectedCareerProfile_ID", id);
                $('#loadprogress').addClass('d-none');
                window.location.href = `edit_career_profiles.html?id=${id}`;
            });
        }
    }




    // checkUserStatus();
    retrieveTestsTakenCount();
    retrieveStudentSurveyCount();
    retrievePublicSurveyCount();
    retrieveCorporateSurveyCount();
    retrieveRecentCareerProfiles();
    retrieveRecentRIASECMarkers();
    loadSummaryData();
    $(document).ready(function () {
        firebase.auth().onAuthStateChanged(function(user) {
            if(user) {
                if(!user.emailVerified){
                    $('#verification-error').removeClass('d-none');
                }
            } else {
                console.log('redirect to login');
                window.location.replace(service.baseURL);
            }
        });
        
        // $currentUserEmail.text(welcomePageData.userAuth.email);

        // perform click action on survey cards
        $('.card-body').on('click', function(e){
            e.preventDefault();

            if($(this).parent('.card').attr('id') == 'psCard'){
                // console.log($(this));
                window.location.href = 'public_survey.html';
            }
            if($(this).parent('.card').attr('id') == 'csCard'){
                window.location.href = 'corporate_survey.html';
            }
            if($(this).parent('.card').attr('id') == 'ssCard'){
                window.location.href = 'survey_list.html';
            }
        });

        $(document).on('click', '#urSiteContent .dashWrapper .row .col-6 .card .card-body #careerProfileFeed li', viewSelectedProfile);

        /* off-canvas sidebar toggle */
        $('[data-toggle=offcanvas]').click(function () {
            $('.row-offcanvas').toggleClass('active');
            $('span.collapse').toggleClass('in');
        });

        $('[data-toggle=offcanvas-in]').click(function () {
            $('.row-offcanvas').addClass('active');
            $('span.collapse').addClass('in');
        });

        $('#toggle-nav').on('click', function(e) {
            e.preventDefault();
            $('#sidebar').show();
        });
        $('#toggle-nav-close').on('click', function(e) {
            e.preventDefault();
            $('#sidebar').hide();
        })

        // verify email account
        $('#verify-email').on('click', function (e) {
            e.preventDefault;
            verifyUserEmail();
        });

        $resendVerification.on('click', function (e) {
            e.preventDefault();
            verifyUserEmail();
        });

        // logout action
        $('#sign-out').on('click', function (e) {
            e.preventDefault();
            console.log('button works');
            signOut();
        });

        // edit profile action
        $("#edit-profile").on('click', function(e) {
            e.preventDefault();
            unryddleApp.storeData('SEL_USER_ID', currUser.uid);
            window.location.href = `edit_profile.html?userId=${currUser.uid}`;
        });


    });
}(jQuery))

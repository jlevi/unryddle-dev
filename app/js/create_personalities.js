(function($) {
    'use strict';

    function prepPayload() {
      var obj = {};
      obj.name = unryddleApp.removeSpace($('#personalityName').val());
      obj.alias = unryddleApp.removeSpace($('#personalityAlias').val());
      obj.description = unryddleApp.removeSpace($('#personalityDesc').val());
      obj.marker = unryddleApp.removeSpace($('#personalityMarker').val());
      obj.slug = unryddleApp.removeSpace($('#personalityName').val().toLowerCase());
      obj.createdOn = firebase.database.ServerValue.TIMESTAMP;

      if($('#personalityImageURL').val() !== ''){
        obj.imageURL = $('#personalityImageURL').val().trim();
      }

      if($('#personalityVideoURL').val !== '') {
        obj.videoURL = $('#personalityVideoURL').val().trim();
      }

      return obj;
    }

    // validate personality form
    $('#createPersonality').validate({
      rules: {
        personalityName: {
          required: true
        },
        personalityAlias: {
          required: true
        },
        personalityDesc: {
          required: true
        },
        personalityMarker: {
          required: true
        }
      }
    });

    // personality form submit
    $('#createPersonality').on('submit', function (e) {
        e.preventDefault();
        $(this).find('button i.fa-spin').removeClass('d-none');
        if ($(this).valid()) {
          let payload = prepPayload();
          let personalityKey = service.personalitiesRef.push().key;
          let personalityData = {};
          personalityData['/personalities/' + personalityKey] = payload;
  
          firebase.database().ref().update(personalityData, function (error) {
            if (error) {
              $personalityFormBtn.find('i').addClass('d-none');
              renderErrorMsg(error, $personalityError);
            } else {
              $personalityFormBtn.find('i').addClass('d-none');
              renderSuccessMsg(personalityPageData.successString, $personalitySuccess);
            }
          })
  
        }
    });

    // logout function
    $('#sign-out').on('click', function(e) {
        e.preventDefault();
        service.signOut();
    });
}(jQuery));
(function($) {
  "use strict";

  const careerProfilesData = {};

  const $loadprogress = $("#loadprogress");
  const $careerProfileError = $("#careerProfile-error");
  const $careerProfileSuccess = $("#careerProfile-success");
  const $realisticListGroup = $("#realisticGroup");
  const $investigativeListGroup = $("#investigativeGroup");
  const $artisticListGroup = $("#artisticGroup");
  const $socialListGroup = $("#socialGroup");
  const $enterprisingListGroup = $("#enterprisingGroup");
  const $conventionalListGroup = $("#conventionalGroup");
  const $dashAlert = $('#dashAlert');
  const $logout = $("#logout-action");

  function renderEmptyListMsg() {
    let html =
      "<h4 class='text-center mt-5 empty-header'>There are no career profiles under this section. Add a profile using the link above.</h4>";

    return html;
  }

  function listItemArr(arr){
    var listData = `<ul class='itemsList'>`;
    for (var i = 0; i < arr.length; i++){

        listData = listData.concat(`<li>${arr[i]}</li>`);
    }
    listData = listData.concat(`</ul>`);
    return listData;
  }


  function listEduArr(arr){
    console.log(arr);
    var listData = '';
    if(arr === undefined) {
      $dashAlert.find('p').text('Some career profiles have in this section have no Education information recorded. You will have to edit them to update their education info');
      $dashAlert.removeClass('d-none');
      listData = listData.concat(`<p class='unryddleErr'>No Education Data Recorded`);
      return listData;
    } else {
      listData = listData.concat(`<ul class='itemsList'>`);
      for (var i = 0; i < arr.length; i++) {
        if(arr[i] == 'juniorHigh'){
          listData = listData.concat(`<li>Junior High</li>`);
        }
        if(arr[i] == 'seniorHigh'){
          listData = listData.concat(`<li>Senior High</li>`);
        }
        if(arr[i] == 'higherNationalDiploma'){
          listData = listData.concat(`<li>Higher National Diploma</li>`);
        }
        if(arr[i] == 'undergradDegree'){
          listData = listData.concat(`<li>Undergraduate Degree</li>`);
        }
        if(arr[i] == 'gradDegree'){
          listData = listData.concat(`<li>Graduate Degree</li>`);
        }
        if(arr[i] == 'licOfCerts'){
          listData = listData.concat(`<li>Licenses of Certificates</li>`);
        }
        if(arr[i] == 'postGradDegree'){
          listData = listData.concat(`<li>Post Graduate Degree</li>`);
        }
      }
      listData = listData.concat(`</ul>`);
      // console.log(listData);
      return listData;
    }   
  }


  function careerTemplateList(obj, id, count, accordion){
    var html = `<div class='card'>
        <div class='card-header' id='item-${id}'>
        <h5 class='mb-0'>
        <a class='btn btn-link collapsed' data-toggle='collapse' data-target='#collapse-${id}' aria-controls='collapse-${id}'>
        <span class='badge badge-unryddle'>${count}</span>
        <span class='item-title'>${obj.name}</span>
        <span class='item-date'>${unryddleApp.convertEpoch(obj.createdOn)}</span>
        </a>
        </h5>
        </div>
        <div class='collapse' aria-labelledby='collapse-${id}' data-parent=${accordion} id='collapse-${id}'>
        <div class='card-body'>
        <h3 class='form-title mt-3'>Name</h3>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Name:</div>
        <div class='col-8 user_name'>${obj.name} </div>
        <div class='col-2'>
        <a href='#' id='edit-careerProfile' class='user-action' data-id=${id}>
        <i class='fas fa-edit'></i>
        </a>
        <a href='#' id='del-careerProfile' class='user-action' data-id=${id}>
        <i class='fas fa-trash'></i>
        </a>
        </div>
        </div>
        <h3 class='form-title mt-3'>Overview</h3>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Interest Code:</div>
        <div class='col-10 item_slug'>${obj.interestCode}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Career Cluster:</div>
        <div class='col-10 item_slug'>${obj.careerCluster}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Career Pathway:</div>
        <div class='col-10 item_slug'>${obj.careerPathway}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Industry:</div>
        <div class='col-10 item_slug'>${obj.careerIndustry}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Definition:</div>
        <div class='col-10 item_slug'>${obj.careerDefinition}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Sample Job Titles:</div>
        <div class='col-10 item_slug'>${obj.sampleJobTitles}</div>
        </div>
        <h3 class='form-title mt-3'>Quick Facts</h3>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Key Tasks:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.keyTasks)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Technology Skills:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.keySkills)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Tools Used:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.toolsUsed)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Wages/Salaries:</div>
        <div class='col-10 item_slug'></div>
        </div>
        <h3 class='form-title mt-3'>Job Description</h3>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Knowledge:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.careerKnowledge)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Skills:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.careerSkills)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Abilities:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.careerAbilities)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Work Activities:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.workActivities)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Detailed Work Activities:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.detailedWorkActivities)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Work Styles:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.workStyles)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Work Values:</div>
        <div class='col-10 item_slug'>${listItemArr(obj.workValues)}</div>
        </div>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Related Occupations:</div>
        <div class='col-10 item_slug'>${obj.relatedOccupations}</div>
        </div>
        <h3 class='form-title mt-3'>Education & Credentials</h3>
        <div class='row w-100 personality-list-margin'>
        <div class='col-2 text-right item-label'>Education Level:</div>
        <div class='col-10 item_slug'>${listEduArr(obj.careerEducation)}</div>
        </div>
        </div>
        </div>
        </div>`;
        return html;
  }



  function processRealisticResults(obj) {
    if(obj === undefined){
      unryddleApp.showErrorMsg(undefined, $dashAlert);
    } else {
      const has = Object.prototype.hasOwnProperty;
      let listData = '';
      let count = 0;
      let accordion = "#accordionR";
      for (var item in obj) {
        if (has.call(obj, item)) {
          let careerProfile = obj[item];
          let careerProfileID = item;
          count++;
          listData = listData.concat(careerTemplateList(careerProfile, careerProfileID, count, accordion));
        }
      }
      $loadprogress.addClass('d-none');
      $('#realisticGroup .accordionList').html(listData);
    }
    
  }

  function processInvestigativeResults(obj) {
    if(obj === undefined) {
      unryddleApp.showErrorMsg(undefined, $dashAlert);
    } else {
      const has = Object.prototype.hasOwnProperty;
      let listData = '';
      let count = 0;
      let accordion = "#accordionI";
      for (var item in obj) {
        if (has.call(obj, item)) {
          let careerProfile = obj[item];
          let careerProfileID = item;
          count++;
          listData = listData.concat(careerTemplateList(careerProfile, careerProfileID, count, accordion));
        }
      }
      $loadprogress.addClass('d-none');
      $('#investigativeGroup .accordionList').html(listData);
    }
    
  }

  function processArtisticResults(obj) {
    if(obj === undefined) {
      unryddleApp.showErrorMsg(undefined, $dashAlert);
    } else {
      const has = Object.prototype.hasOwnProperty;
      let listData = '';
      let count = 0;
      let accordion = "#accordionA";
      for (var item in obj) {
        if (has.call(obj, item)) {
          let careerProfile = obj[item];
          let careerProfileID = item;
          count++;
          listData = listData.concat(careerTemplateList(careerProfile, careerProfileID, count, accordion));
        }
      }
      $loadprogress.addClass('d-none');
      $('#artisticGroup .accordionList').html(listData);
    }
    
  }

  function processSocialResults(obj) {
    // console.log()
    if(obj === undefined) {
      unryddleApp.showErrorMsg(undefined, $dashAlert);
    } else {
      const has = Object.prototype.hasOwnProperty;
      let listData = '';
      let count = 0;
      let accordion = "#accordionS";
      for (var item in obj) {
        if (has.call(obj, item)) {
          let careerProfile = obj[item];
          let careerProfileID = item;
          count++;
          listData = listData.concat(careerTemplateList(careerProfile, careerProfileID, count, accordion));
        }
      }
      $loadprogress.addClass('d-none');
      $('#socialGroup .accordionList').html(listData);
    }
    
  }

  function processEnterprisingResults(obj) {
    if(obj === undefined) {
      unryddleApp.showErrorMsg(undefined, $dashAlert);
    } else {
      const has = Object.prototype.hasOwnProperty;
      let listData = '';
      let count = 0;
      let accordion = "#accordionE";
      for (var item in obj) {
        if (has.call(obj, item)) {
          let careerProfile = obj[item];
          let careerProfileID = item;
          count++;
          listData = listData.concat(careerTemplateList(careerProfile, careerProfileID, count, accordion));
        }
      }
      $loadprogress.addClass('d-none');
      $('#enterprisingGroup .accordionList').html(listData);
    }
    
  }

  function processConventionalResults(obj) {
    if(obj === undefined) {
      unryddleApp.showErrorMsg(undefined, $dashAlert);
    } else {
      const has = Object.prototype.hasOwnProperty;
      let listData = '';
      let count = 0;
      let accordion = "#accordionC";
      for (var item in obj) {
        if (has.call(obj, item)) {
          let careerProfile = obj[item];
          let careerProfileID = item;
          count++;
          listData = listData.concat(careerTemplateList(careerProfile, careerProfileID, count, accordion));
        }
      }
      $loadprogress.addClass('d-none');
      $('#conventionalGroup .accordionList').html(listData);
    }
    
  }


  function getRealisticCareers() {
    $loadprogress.removeClass('d-none');
    $('#realisticGroup .accordionList').html('');
    service.getRealisticCareers().then(function(snapshot) {
      var results = snapshot.val();
      processRealisticResults(results);
    }).catch(function(error) {
      unryddleApp.showErrorMsg(error, $dashAlert);
      $loadprogress.addClass('d-none');
    });
  }

  function getInvestigativeCareers() {
    $loadprogress.removeClass('d-none');
    $('#investigativeGroup .accordionList').html('');
    service.getInvestigativeCareers().then(function(snapshot) {
      var results = snapshot.val();
      processInvestigativeResults(results);
    }).catch(function(error) {
      unryddleApp.showErrorMsg(error, $dashAlert);
      $loadprogress.addClass('d-none');
    });
  }

  function getArtisticCareers() {
    $loadprogress.removeClass('d-none');
    $('#artisticGroup .accordionList').html('');
    service.getArtisticCareers().then(function(snapshot){
      var results = snapshot.val();
      processArtisticResults(results);
    }).catch(function(error) {
      unryddleApp.showErrorMsg(error, $dashAlert);
      $loadprogress.addClass('d-none');
    })
  }

  function getSocialCareers() {
    $loadprogress.removeClass('d-none');
    $('#socialGroup .accordionList').html('');
    service.getSocialCareers().then(function(snapshot){
      var results = snapshot.val();
      processSocialResults(results);
    }).catch(function(error){
      $loadprogress.addClass('d-none');
      unryddleApp.showErrorMsg(error, $dashAlert);
    });
  }

  function getEnterprisingCareers() {
    $loadprogress.removeClass('d-none');
    $('#enterprisingGroup .accordionList').html('');
    service.getEnterprisingCareers().then(function(snapshot){
      var results = snapshot.val();
      processEnterprisingResults(results);
    }).catch(function(error){
      $loadprogress.addClass('d-none');
      unryddleApp.showErrorMsg(error, $dashAlert);
    });
  }

  function getConventionalCareers() {
    $loadprogress.removeClass('d-none');
    $('#conventionalGroup .accordionList').html('');
    service.getConventionalCareers().then(function(snapshot){
      var results = snapshot.val();
      processConventionalResults(results);
    }).catch(function(error){
      $loadprogress.addClass('d-none');
      unryddleApp.showErrorMsg(error, $dashAlert);
    });
  }

  // sign out function
  function signOut() {
    service
      .logout()
      .then(function() {
        unryddleApp.clearData();
        window.location.replace(service.baseURL);
      })
      .catch(function(error) {
        unryddleApp.showErrorMsg(error, $dashAlert);
      });
  }

  // process edit action on button click
  function processEditAction(event) {
    let target = event.target;
    console.log(target);
    if (target.tagName === "svg" || target.tagName === 'path') {
      let itemID = $(target).closest("a").attr("data-id");
      service.careerProfilesRef.child(itemID).once("value", function(snapshot) {
        let obj = snapshot.val();
        unryddleApp.storeData("EDIT_CAREER_PROFILE", obj);
        unryddleApp.storeData("EDIT_CAREER_PROFiLE_ID", itemID);
        window.location.href = "edit_career_profiles.html?id=" + itemID;
      }).catch(function(err){
        unryddleApp.showErrorMsg(err, $dashAlert);
      });
    }
  }

  // process delete action on button click
  function processDeleteAction(event) {
    let target = event.target;
    console.log(target);
    if (target.tagName === 'svg' || target.tagName === 'path') {
      let itemID = $(target).closest("a").attr("data-id");
      console.log(itemID);
      $('#deleteCareer').attr('data-id', itemID);
      $('#deleteProfileCenter').modal(unryddleApp.modalOptions);
    }
  }

  function deleteCareerEntry(ev) {
    let target = ev.target;
    let targetID = $(target).attr('data-id');
    service.careerProfilesRef.child(targetID).remove().then(function(){
      $dashAlert.find('p').text('The career profile was successfully removed. Updating page...');
      $('#deleteProfileCenter').modal('hide');
      $dashAlert.removeClass('alert-danger').addClass('alert-success').removeClass('d-none');
    }).catch(function(err){
      $('#deleteProfileCenter').modal('hide');
      unryddleApp.showErrorMsg(err, $dashAlert);
    });

  }

  getRealisticCareers();

  $(document).ready(function() {
    // remove alert messages on input change
    $("input, select, textarea").on(
      "change keyup keydown paste click",
      function(e) {
        if (!$careerProfileError.hasClass("d-none")) {
          $careerProfileError.addClass("d-none");
        }

        if (!$careerProfileSuccess.hasClass("d-none")) {
          $careerProfileSuccess.addClass("d-none");
        }
      }
    );

    // logout function
    $logout.on("click", function(e) {
      e.preventDefault();

      signOut();
    });

    // personality tab click event
    $('a[data-toggle="tab"]').on("shown.bs.tab", function(e) {
      var tabTarget = $(e.target).attr("id");
      console.log(tabTarget)

      // realistic profile
      if (tabTarget == "realistic-tab") {
        getRealisticCareers();
      }

      // investigative profile
      if (tabTarget == "investigative-tab") {
        getInvestigativeCareers();
      }

      // artistic profile
      if (tabTarget == "artistic-tab") {
        getArtisticCareers();
      }

      // social profile
      if (tabTarget == "social-tab") {
        getSocialCareers();
      }

      // enterprising profile
      if (tabTarget == "enterprising-tab") {
        getEnterprisingCareers();
      }

      // conventional profile
      if (tabTarget == "conventional-tab") {
        getConventionalCareers();
      }
    });

    // edit event handler for careerprofiles
    $(document).on(
      "click", ".tab-pane.fade.show.active .list-group.mt-3 .accordionList .card .collapse.show .card-body .row.w-100.personality-list-margin .col-2 #edit-careerProfile", processEditAction);

    // delete event handler for careerprofiles
    $(document).on("click", ".tab-pane.fade.show.active .list-group.mt-3 .accordionList .card .collapse.show .card-body .row.w-100.personality-list-margin .col-2 #del-careerProfile", processDeleteAction);

    // perform actual delete process
    $(document).on("click", "div#deleteProfileCenter.modal.fade.show .modal-dialog .modal-content .modal-footer #deleteCareer", deleteCareerEntry);

    $('#deleteProfileCenter').on('hidden.bs.modal', function (e) {
      setTimeout(function(){
        window.location.reload();
      }, 3000);     
    });
  });
})(jQuery);

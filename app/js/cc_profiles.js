(function ($) {
    'use strict';

    var prData = {
        clusterList: [],
        pathwayNames: [],
        industryNames: [],
        valuesNames:[],
        skillsNames: [],
        r_payload: null,
        i_payload: null,
        a_payload: null,
        s_payload: null,
        e_payload: null,
        c_payload: null,

    }

    const $sub_Button = $('#submit_careerProfile');
    const $cp_Error = $('#careerProfile-error');
    const $cp_Success = $('#careerProfile-success');


    function processRealisticPayload() {
        var obj = {};
        obj.name = $('#careerProfile_r_name').val().trim();
        obj.personality = 'realistic';
        obj.sampleJobTitles = unryddleApp.spliceArrDataCommaSpace($('#careerProfile_r_sampleJobTitles').val().trim());
        obj.interestCode = $('#careerProfile_r_IC').val().trim();
        obj.careerCluster = unryddleApp.spliceArrDataComma($('#careerProfile_r_CC').val().trim());
        obj.careerPathway = unryddleApp.spliceArrDataComma($('#careerProfile_r_CPW').val().trim());
        obj.careerIndustry = unryddleApp.spliceArrDataComma($('#careerProfile_r_IN').val().trim());
        obj.careerDefinition = $('#careerProfile_r_DEFN').val().trim();
        obj.keyTasks = unryddleApp.spliceArrDataNewline($('#careerProfile_r_keyTasks').val().trim());
        obj.keySkills = unryddleApp.spliceArrDataNewline($('#careerProfile_r_keySkills').val().trim());
        obj.toolsUsed = unryddleApp.spliceArrDataNewline($('#careerProfile_r_tools').val().trim());
        obj.careerKnowledge = unryddleApp.spliceArrDataComma($('#careerProfile_r_knowledge').val().trim());
        obj.careerSkills = unryddleApp.spliceArrDataComma($('#careerProfile_r_skills').val().trim());
        obj.careerAbilities = unryddleApp.spliceArrDataNewline($('#careerProfile_r_abilities').val().trim());
        obj.workActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_r_workActvs').val().trim());
        obj.detailedWorkActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_r_detWorkActvs').val().trim());
        obj.workStyles = unryddleApp.spliceArrDataComma($('#careerProfile_r_workStyles').val().trim());
        obj.workValues = unryddleApp.spliceArrDataComma($('#careerProfile_r_workValues').val().trim());
        obj.relatedOccupations = unryddleApp.spliceArrDataNewline($('#careerProfile_r_relatedOccupations').val().trim());
        obj.careerEducation = unryddleApp.getCheckVals($("input[name='careerProfile_r_eduLevel']"));
        obj.careerSources = unryddleApp.spliceArrDataNewline($('#careerProfile_r_sources').val().trim());
        obj.createdOn = firebase.database.ServerValue.TIMESTAMP;
        // conditional values
        if($('#careerProfile_r_icon').val() != ''){
            obj.careerIcon = $('#careerProfile_r_icon').val().trim();
        }
        if($('#careerProfile_r_wages').val() != ''){
            obj.careerWages = $('#careerProfile_r_wages').val().trim();
        }
        if($('#careerProfile_r_internships').val() != ''){
            obj.careerInternships = unryddleApp.spliceArrDataNewline($('#careerProfile_r_internships').val().trim());
        }
        if($('#careerProfile_r_apprenticeships').val() != ''){
            obj.careerApprenticeships = unryddleApp.spliceArrDataNewline($('#careerProfile_r_apprenticeships').val().trim());
        }
        if($('#careerProfile_r_otjTraining').val() != ''){
            obj.careerOTJTraining = unryddleApp.spliceArrDataNewline($('#careerProfile_r_otjTraining').val().trim());
        }
        if($('#careerProfile_r_hobbies').val() != ''){
            obj.careerHobbies = unryddleApp.spliceArrDataNewline($('#careerProfile_r_hobbies').val().trim());
        }

        return obj;
    }

    function processInvestigativePayload() {
        var obj = {};
        obj.name = $('#careerProfile_i_name').val().trim();
        obj.personality = 'investigative';
        obj.sampleJobTitles = unryddleApp.spliceArrDataComma($('#careerProfile_i_sampleJobTitles').val().trim());
        obj.interestCode = $('#careerProfile_i_IC').val().trim();
        obj.careerCluster = unryddleApp.spliceArrDataComma($('#careerProfile_i_CC').val().trim());
        obj.careerPathway = unryddleApp.spliceArrDataComma($('#careerProfile_i_CPW').val().trim());
        obj.careerIndustry = unryddleApp.spliceArrDataComma($('#careerProfile_i_IN').val().trim());
        obj.careerDefinition = $('#careerProfile_i_DEFN').val().trim();
        obj.keyTasks = unryddleApp.spliceArrDataNewline($('#careerProfile_i_keyTasks').val().trim());
        obj.keySkills = unryddleApp.spliceArrDataNewline($('#careerProfile_i_keySkills').val().trim());
        obj.toolsUsed = unryddleApp.spliceArrDataNewline($('#careerProfile_i_tools').val().trim());
        obj.careerKnowledge = unryddleApp.spliceArrDataComma($('#careerProfile_i_knowledge').val().trim());
        obj.careerSkills = unryddleApp.spliceArrDataComma($('#careerProfile_i_skills').val().trim());
        obj.careerAbilities = unryddleApp.spliceArrDataNewline($('#careerProfile_i_abilities').val().trim());
        obj.workActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_i_workActvs').val().trim());
        obj.detailedWorkActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_i_detWorkActvs').val().trim());
        obj.workStyles = unryddleApp.spliceArrDataComma($('#careerProfile_i_workStyles').val().trim());
        obj.workValues = unryddleApp.spliceArrDataComma($('#careerProfile_i_workValues').val().trim());
        obj.relatedOccupations = unryddleApp.spliceArrDataNewline($('#careerProfile_i_relatedOccupations').val().trim());
        obj.careerEducation = unryddleApp.getCheckVals($("input[name='careerProfile_i_eduLevel']"));
        obj.careerSources = unryddleApp.spliceArrDataNewline($('#careerProfile_i_sources').val().trim());
        obj.createdOn = firebase.database.ServerValue.TIMESTAMP;
        // conditional values
        if($('#careerProfile_i_icon').val() != ''){
            obj.careerIcon = $('#careerProfile_i_icon').val().trim();
        }
        if($('#careerProfile_i_wages').val() != ''){
            obj.careerWages = $('#careerProfile_i_wages').val().trim();
        }
        if($('#careerProfile_i_internships').val() != ''){
            obj.careerInternships = unryddleApp.spliceArrDataNewline($('#careerProfile_i_internships').val().trim());
        }
        if($('#careerProfile_i_apprenticeships').val() != ''){
            obj.careerApprenticeships = unryddleApp.spliceArrDataNewline($('#careerProfile_i_apprenticeships').val().trim());
        }
        if($('#careerProfile_i_otjTraining').val() != ''){
            obj.careerOTJTraining = unryddleApp.spliceArrDataNewline($('#careerProfile_i_otjTraining').val().trim());
        }
        if($('#careerProfile_i_hobbies').val() != ''){
            obj.careerHobbies = unryddleApp.spliceArrDataNewline($('#careerProfile_i_hobbies').val().trim());
        }

        return obj;
    }

    function processArtisticPayload() {
        var obj = {};
        obj.name = $('#careerProfile_a_name').val().trim();
        obj.personality = 'artistic';
        obj.sampleJobTitles = unryddleApp.spliceArrDataComma($('#careerProfile_a_sampleJobTitles').val().trim());
        obj.interestCode = $('#careerProfile_a_IC').val().trim();
        obj.careerCluster = unryddleApp.spliceArrDataComma($('#careerProfile_a_CC').val().trim());
        obj.careerPathway = unryddleApp.spliceArrDataComma($('#careerProfile_a_CPW').val().trim());
        obj.careerIndustry = unryddleApp.spliceArrDataComma($('#careerProfile_a_IN').val().trim());
        obj.careerDefinition = $('#careerProfile_a_DEFN').val().trim();
        obj.keyTasks = unryddleApp.spliceArrDataNewline($('#careerProfile_a_keyTasks').val().trim());
        obj.keySkills = unryddleApp.spliceArrDataNewline($('#careerProfile_a_keySkills').val().trim());
        obj.toolsUsed = unryddleApp.spliceArrDataNewline($('#careerProfile_a_tools').val().trim());
        obj.careerKnowledge = unryddleApp.spliceArrDataComma($('#careerProfile_a_knowledge').val().trim());
        obj.careerSkills = unryddleApp.spliceArrDataComma($('#careerProfile_a_skills').val().trim());
        obj.careerAbilities = unryddleApp.spliceArrDataNewline($('#careerProfile_a_abilities').val().trim());
        obj.workActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_a_workActvs').val().trim());
        obj.detailedWorkActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_a_detWorkActvs').val().trim());
        obj.workStyles = unryddleApp.spliceArrDataComma($('#careerProfile_a_workStyles').val().trim());
        obj.workValues = unryddleApp.spliceArrDataComma($('#careerProfile_a_workValues').val().trim());
        obj.relatedOccupations = unryddleApp.spliceArrDataNewline($('#careerProfile_a_relatedOccupations').val().trim());
        obj.careerEducation = unryddleApp.getCheckVals($("input[name='careerProfile_a_eduLevel']"));
        obj.careerSources = unryddleApp.spliceArrDataNewline($('#careerProfile_a_sources').val().trim());
        obj.createdOn = firebase.database.ServerValue.TIMESTAMP;
        // conditional values
        if($('#careerProfile_a_icon').val() != ''){
            obj.careerIcon = $('#careerProfile_a_icon').val().trim();
        }
        if($('#careerProfile_a_wages').val() != ''){
            obj.careerWages = $('#careerProfile_a_wages').val().trim();
        }
        if($('#careerProfile_a_internships').val() != ''){
            obj.careerInternships = unryddleApp.spliceArrDataNewline($('#careerProfile_a_internships').val().trim());
        }
        if($('#careerProfile_a_apprenticeships').val() != ''){
            obj.careerApprenticeships = unryddleApp.spliceArrDataNewline($('#careerProfile_a_apprenticeships').val().trim());
        }
        if($('#careerProfile_a_otjTraining').val() != ''){
            obj.careerOTJTraining = unryddleApp.spliceArrDataNewline($('#careerProfile_a_otjTraining').val().trim());
        }
        if($('#careerProfile_a_hobbies').val() != ''){
            obj.careerHobbies = unryddleApp.spliceArrDataNewline($('#careerProfile_a_hobbies').val().trim());
        }

        return obj;
    }

    function processSocialPayload() {
        var obj = {};
        obj.name = $('#careerProfile_s_name').val().trim();
        obj.personality = 'social';
        obj.sampleJobTitles = unryddleApp.spliceArrDataComma($('#careerProfile_s_sampleJobTitles').val().trim());
        obj.interestCode = $('#careerProfile_s_IC').val().trim();
        obj.careerCluster = unryddleApp.spliceArrDataComma($('#careerProfile_s_CC').val().trim());
        obj.careerPathway = unryddleApp.spliceArrDataComma($('#careerProfile_s_CPW').val().trim());
        obj.careerIndustry = unryddleApp.spliceArrDataComma($('#careerProfile_s_IN').val().trim());
        obj.careerDefinition = $('#careerProfile_s_DEFN').val().trim();
        obj.keyTasks = unryddleApp.spliceArrDataNewline($('#careerProfile_s_keyTasks').val().trim());
        obj.keySkills = unryddleApp.spliceArrDataNewline($('#careerProfile_s_keySkills').val().trim());
        obj.toolsUsed = unryddleApp.spliceArrDataNewline($('#careerProfile_s_tools').val().trim());
        obj.careerKnowledge = unryddleApp.spliceArrDataComma($('#careerProfile_s_knowledge').val().trim());
        obj.careerSkills = unryddleApp.spliceArrDataComma($('#careerProfile_s_skills').val().trim());
        obj.careerAbilities = unryddleApp.spliceArrDataNewline($('#careerProfile_s_abilities').val().trim());
        obj.workActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_s_workActvs').val().trim());
        obj.detailedWorkActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_s_detWorkActvs').val().trim());
        obj.workStyles = unryddleApp.spliceArrDataComma($('#careerProfile_s_workStyles').val().trim());
        obj.workValues = unryddleApp.spliceArrDataComma($('#careerProfile_s_workValues').val().trim());
        obj.relatedOccupations = unryddleApp.spliceArrDataNewline($('#careerProfile_s_relatedOccupations').val().trim());
        obj.careerEducation = unryddleApp.getCheckVals($("input[name='careerProfile_s_eduLevel']"));
        obj.careerSources = unryddleApp.spliceArrDataNewline($('#careerProfile_s_sources').val().trim());
        obj.createdOn = firebase.database.ServerValue.TIMESTAMP;
        // conditional values
        if($('#careerProfile_s_icon').val() != ''){
            obj.careerIcon = $('#careerProfile_s_icon').val().trim();
        }
        if($('#careerProfile_s_wages').val() != ''){
            obj.careerWages = $('##careerProfile_s_wages').val().trim();
        }
        if($('#careerProfile_s_internships').val() != ''){
            obj.careerInternships = unryddleApp.spliceArrDataNewline($('#careerProfile_s_internships').val().trim());
        }
        if($('#careerProfile_s_apprenticeships').val() != ''){
            obj.careerApprenticeships = unryddleApp.spliceArrDataNewline($('#careerProfile_s_apprenticeships').val().trim());
        }
        if($('#careerProfile_s_otjTraining').val() != ''){
            obj.careerOTJTraining = unryddleApp.spliceArrDataNewline($('#careerProfile_s_otjTraining').val().trim());
        }
        if($('#careerProfile_s_hobbies').val() != ''){
            obj.careerHobbies = unryddleApp.spliceArrDataNewline($('#careerProfile_s_hobbies').val().trim());
        }

        return obj;
    }

    function processEnterprisingPayload() {
        var obj = {};
        obj.name = $('#careerProfile_e_name').val().trim();
        obj.personality = 'enterprising';
        obj.sampleJobTitles = unryddleApp.spliceArrDataComma($('#careerProfile_e_sampleJobTitles').val().trim());
        obj.interestCode = $('#careerProfile_e_IC').val().trim();
        obj.careerCluster = unryddleApp.spliceArrDataComma($('#careerProfile_e_CC').val().trim());
        obj.careerPathway = unryddleApp.spliceArrDataComma($('#careerProfile_e_CPW').val().trim());
        obj.careerIndustry = unryddleApp.spliceArrDataComma($('#careerProfile_e_IN').val().trim());
        obj.careerDefinition = $('#careerProfile_e_DEFN').val().trim();
        obj.keyTasks = unryddleApp.spliceArrDataNewline($('#careerProfile_e_keyTasks').val().trim());
        obj.keySkills = unryddleApp.spliceArrDataNewline($('#careerProfile_e_keySkills ').val().trim());
        obj.toolsUsed = unryddleApp.spliceArrDataNewline($('#careerProfile_e_tools').val().trim());
        obj.careerKnowledge = unryddleApp.spliceArrDataComma($('#careerProfile_e_knowledge').val().trim());
        obj.careerSkills = unryddleApp.spliceArrDataComma($('#careerProfile_e_skills').val().trim());
        obj.careerAbilities = unryddleApp.spliceArrDataNewline($('#careerProfile_e_abilities').val().trim());
        obj.workActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_e_workActvs').val().trim());
        obj.detailedWorkActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_e_detWorkActvs').val().trim());
        obj.workStyles = unryddleApp.spliceArrDataComma($('#careerProfile_e_workStyles').val().trim());
        obj.workValues = unryddleApp.spliceArrDataComma($('#careerProfile_e_workValues').val().trim());
        obj.relatedOccupations = unryddleApp.spliceArrDataNewline($('#careerProfile_e_relatedOccupations').val().trim());
        obj.careerEducation = unryddleApp.getCheckVals($("input[name='careerProfile_e_eduLevel']"));
        obj.careerSources = unryddleApp.spliceArrDataNewline($('#careerProfile_e_sources').val().trim());
        obj.createdOn = firebase.database.ServerValue.TIMESTAMP;
        // conditional values
        if($('#careerProfile_e_icon').val() != ''){
            obj.careerIcon = $('#careerProfile_e_icon').val().trim();
        }
        if($('#careerProfile_e_wages').val() != ''){
            obj.careerWages = $('#careerProfile_e_wages').val().trim();
        }
        if($('#careerProfile_e_internships').val() != ''){
            obj.careerInternships = unryddleApp.spliceArrDataNewline($('#careerProfile_e_internships').val().trim());
        }
        if($('#careerProfile_e_apprenticeships').val() != ''){
            obj.careerApprenticeships = unryddleApp.spliceArrDataNewline($('#careerProfile_e_apprenticeships').val().trim());
        }
        if($('#careerProfile_e_otjTraining').val() != ''){
            obj.careerOTJTraining = unryddleApp.spliceArrDataNewline($('#careerProfile_e_otjTraining').val().trim());
        }
        if($('#careerProfile_e_hobbies').val() != ''){
            obj.careerHobbies = unryddleApp.spliceArrDataNewline($('#careerProfile_e_hobbies').val().trim());
        }

        return obj;
    }

    function processConventionalPayload() {
        var obj = {};
        obj.name = $('#careerProfile_c_name').val().trim();
        obj.personality = 'conventional';
        obj.sampleJobTitles = unryddleApp.spliceArrDataComma($('#careerProfile_c_sampleJobTitles').val().trim());
        obj.interestCode = $('#careerProfile_c_IC').val().trim();
        obj.careerCluster = unryddleApp.spliceArrDataComma($('#careerProfile_c_CC').val().trim());
        obj.careerPathway = unryddleApp.spliceArrDataComma($('#careerProfile_c_CPW').val().trim());
        obj.careerIndustry = unryddleApp.spliceArrDataComma($('#careerProfile_c_IN').val().trim());
        obj.careerDefinition = $('#careerProfile_c_DEFN').val().trim();
        obj.keyTasks = unryddleApp.spliceArrDataNewline($('#careerProfile_c_keyTasks').val().trim());
        obj.keySkills = unryddleApp.spliceArrDataNewline($('#careerProfile_c_keySkills').val().trim());
        obj.toolsUsed = unryddleApp.spliceArrDataNewline($('#careerProfile_c_tools').val().trim());
        obj.careerKnowledge = unryddleApp.spliceArrDataComma($('#careerProfile_c_knowledge').val().trim());
        obj.careerSkills = unryddleApp.spliceArrDataComma($('#careerProfile_c_skills').val().trim());
        obj.careerAbilities = unryddleApp.spliceArrDataNewline($('#careerProfile_c_abilities').val().trim());
        obj.workActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_c_workActvs').val().trim());
        obj.detailedWorkActivities = unryddleApp.spliceArrDataNewline($('#careerProfile_c_detWorkActvs').val().trim());
        obj.workStyles = unryddleApp.spliceArrDataComma($('#careerProfile_c_workStyles').val().trim());
        obj.workValues = unryddleApp.spliceArrDataComma($('#careerProfile_c_workValues').val().trim());
        obj.relatedOccupations = unryddleApp.spliceArrDataNewline($('#careerProfile_c_relatedOccupations').val().trim());
        obj.careerEducation = unryddleApp.getCheckVals($("input[name='careerProfile_c_eduLevel']"));
        obj.careerSources = unryddleApp.spliceArrDataNewline($('#careerProfile_c_sources').val().trim());
        obj.createdOn = firebase.database.ServerValue.TIMESTAMP;
        // conditional values
        if($('#careerProfile_c_icon').val() != ''){
            obj.careerIcon = $('#careerProfile_c_icon').val().trim();
        }
        if($('#careerProfile_c_wages').val() != ''){
            obj.careerWages = $('#careerProfile_c_wages').val().trim();
        }
        if($('#careerProfile_c_internships').val() != ''){
            obj.careerInternships = unryddleApp.spliceArrDataNewline($('#careerProfile_c_internships').val().trim());
        }
        if($('#careerProfile_c_apprenticeships').val() != ''){
            obj.careerApprenticeships = unryddleApp.spliceArrDataNewline($('#careerProfile_c_apprenticeships').val().trim());
        }
        if($('#careerProfile_c_otjTraining').val() != ''){
            obj.careerOTJTraining = unryddleApp.spliceArrDataNewline($('#careerProfile_c_otjTraining').val().trim());
        }
        if($('#careerProfile_c_hobbies').val() != ''){
            obj.careerHobbies = unryddleApp.spliceArrDataNewline($('#careerProfile_c_hobbies').val().trim());
        }

        return obj;
    }

    $(document).ready(function() {
        console.log('page ready');
        

        // validate create career profile form
        $('#realisticCareerProfile').validate({
            rules: {
                "careerProfile_r_name": {
                    required: true
                },
                "careerProfile_r_personality": {
                    required: true
                },
                "careerProfile_r_sampleJobTitles": {
                    required: true
                },
                "careerProfile_r_IC": {
                    required: true
                },
                "careerProfile_r_CC": {
                    required: true
                },
                "careerProfile_r_CPW": {
                    required: true
                },
                "careerProfile_r_IN": {
                    required: true
                },
                "careerProfile_r_DEFN": {
                    required: true
                },
                "careerProfile_r_keyTasks": {
                    required: true
                },
                "careerProfile_r_keySkills": {
                    required: true
                },
                "careerProfile_r_tools": {
                    required: true
                },
                "careerProfile_r_knowledge": {
                    required: true
                },
                "careerProfile_r_skills": {
                    required: true
                },
                "careerProfile_r_abilities": {
                    required: true
                },
                "careerProfile_r_workActvs": {
                    required: true
                },
                "careerProfile_r_detWorkActvs": {
                    required: true
                },
                "careerProfile_r_workStyles": {
                    required: true
                },
                "careerProfile_r_workValues": {
                    required: true
                },
                "careerProfile_r_relatedOccupations": {
                    required: true
                },
                "careerProfile_r_edu": {
                    required: true
                },
                "careerProfile_r_sources": {
                    required: true
                }
            }
        });

        // validate create career profile form
        $('#investigativeCareerProfile').validate({
            rules: {
                "careerProfile_i_name": {
                    required: true
                },
                "careerProfile_i_personality": {
                    required: true
                },
                "careerProfile_i_sampleJobTitles": {
                    required: true
                },
                "careerProfile_i_IC": {
                    required: true
                },
                "careerProfile_i_CC": {
                    required: true
                },
                "careerProfile_i_CPW": {
                    required: true
                },
                "careerProfile_i_IN": {
                    required: true
                },
                "careerProfile_i_DEFN": {
                    required: true
                },
                "careerProfile_i_keyTasks": {
                    required: true
                },
                "careerProfile_i_keySkills": {
                    required: true
                },
                "careerProfile_i_tools": {
                    required: true
                },
                "careerProfile_i_knowledge": {
                    required: true
                },
                "careerProfile_i_skills": {
                    required: true
                },
                "careerProfile_i_abilities": {
                    required: true
                },
                "careerProfile_i_workActvs": {
                    required: true
                },
                "careerProfile_i_detWorkActvs": {
                    required: true
                },
                "careerProfile_i_workStyles": {
                    required: true
                },
                "careerProfile_i_workValues": {
                    required: true
                },
                "careerProfile_i_relatedOccupations": {
                    required: true
                },
                "careerProfile_i_edu": {
                    required: true
                },
                "careerProfile_i_sources": {
                    required: true
                }
            }
        });

        // validate create career profile form
        $('#artisticCareerProfile').validate({
            rules: {
                "careerProfile_a_name": {
                    required: true
                },
                "careerProfile_a_personality": {
                    required: true
                },
                "careerProfile_a_sampleJobTitles": {
                    required: true
                },
                "careerProfile_a_IC": {
                    required: true
                },
                "careerProfile_a_CC": {
                    required: true
                },
                "careerProfile_a_CPW": {
                    required: true
                },
                "careerProfile_a_IN": {
                    required: true
                },
                "careerProfile_a_DEFN": {
                    required: true
                },
                "careerProfile_a_keyTasks": {
                    required: true
                },
                "careerProfile_a_keySkills": {
                    required: true
                },
                "careerProfile_a_tools": {
                    required: true
                },
                "careerProfile_a_knowledge": {
                    required: true
                },
                "careerProfile_a_skills": {
                    required: true
                },
                "careerProfile_a_abilities": {
                    required: true
                },
                "careerProfile_a_workActvs": {
                    required: true
                },
                "careerProfile_a_detWorkActvs": {
                    required: true
                },
                "careerProfile_a_workStyles": {
                    required: true
                },
                "careerProfile_a_workValues": {
                    required: true
                },
                "careerProfile_a_relatedOccupations": {
                    required: true
                },
                "careerProfile_a_edu": {
                    required: true
                },
                "careerProfile_a_sources": {
                    required: true
                }
            }
        });

        // validate create career profile form
        $('#socialCareerProfile').validate({
            rules: {
                "careerProfile_s_name": {
                    required: true
                },
                "careerProfile_s_personality": {
                    required: true
                },
                "careerProfile_s_sampleJobTitles": {
                    required: true
                },
                "careerProfile_s_IC": {
                    required: true
                },
                "careerProfile_s_CC": {
                    required: true
                },
                "careerProfile_s_CPW": {
                    required: true
                },
                "careerProfile_s_IN": {
                    required: true
                },
                "careerProfile_s_DEFN": {
                    required: true
                },
                "careerProfile_s_keyTasks": {
                    required: true
                },
                "careerProfile_s_keySkills": {
                    required: true
                },
                "careerProfile_s_tools": {
                    required: true
                },
                "careerProfile_s_knowledge": {
                    required: true
                },
                "careerProfile_s_skills": {
                    required: true
                },
                "careerProfile_s_abilities": {
                    required: true
                },
                "careerProfile_s_workActvs": {
                    required: true
                },
                "careerProfile_s_detWorkActvs": {
                    required: true
                },
                "careerProfile_s_workStyles": {
                    required: true
                },
                "careerProfile_s_workValues": {
                    required: true
                },
                "careerProfile_s_relatedOccupations": {
                    required: true
                },
                "careerProfile_s_edu": {
                    required: true
                },
                "careerProfile_s_sources": {
                    required: true
                }
            }
        });

        // validate create career profile form
        $('#enterprisingCareerProfile').validate({
            rules: {
                "careerProfile_e_name": {
                    required: true
                },
                "careerProfile_e_personality": {
                    required: true
                },
                "careerProfile_e_sampleJobTitles": {
                    required: true
                },
                "careerProfile_e_IC": {
                    required: true
                },
                "careerProfile_e_CC": {
                    required: true
                },
                "careerProfile_e_CPW": {
                    required: true
                },
                "careerProfile_e_IN": {
                    required: true
                },
                "careerProfile_e_DEFN": {
                    required: true
                },
                "careerProfile_e_keyTasks": {
                    required: true
                },
                "careerProfile_e_keySkills": {
                    required: true
                },
                "careerProfile_e_tools": {
                    required: true
                },
                "careerProfile_e_knowledge": {
                    required: true
                },
                "careerProfile_e_skills": {
                    required: true
                },
                "careerProfile_e_abilities": {
                    required: true
                },
                "careerProfile_e_workActvs": {
                    required: true
                },
                "careerProfile_e_detWorkActvs": {
                    required: true
                },
                "careerProfile_e_workStyles": {
                    required: true
                },
                "careerProfile_e_workValues": {
                    required: true
                },
                "careerProfile_e_relatedOccupations": {
                    required: true
                },
                "careerProfile_e_edu": {
                    required: true
                },
                "careerProfile_e_sources": {
                    required: true
                }
            }
        });

        // validate create career profile form
        $('#conventionalCareerProfile').validate({
            rules: {
                "careerProfile_c_name": {
                    required: true
                },
                "careerProfile_c_personality": {
                    required: true
                },
                "careerProfile_c_sampleJobTitles": {
                    required: true
                },
                "careerProfile_c_IC": {
                    required: true
                },
                "careerProfile_c_CC": {
                    required: true
                },
                "careerProfile_c_CPW": {
                    required: true
                },
                "careerProfile_c_IN": {
                    required: true
                },
                "careerProfile_c_DEFN": {
                    required: true
                },
                "careerProfile_c_keyTasks": {
                    required: true
                },
                "careerProfile_c_keySkills": {
                    required: true
                },
                "careerProfile_c_tools": {
                    required: true
                },
                "careerProfile_c_knowledge": {
                    required: true
                },
                "careerProfile_c_skills": {
                    required: true
                },
                "careerProfile_c_abilities": {
                    required: true
                },
                "careerProfile_c_workActvs": {
                    required: true
                },
                "careerProfile_c_detWorkActvs": {
                    required: true
                },
                "careerProfile_c_workStyles": {
                    required: true
                },
                "careerProfile_c_workValues": {
                    required: true
                },
                "careerProfile_c_relatedOccupations": {
                    required: true
                },
                "careerProfile_c_edu": {
                    required: true
                },
                "careerProfile_c_sources": {
                    required: true
                }
            }
        });


        // perform realistic form submission
        $('#realisticCareerProfile').on('submit', function(e) {
            e.preventDefault();
            $('#submit_r_careerProfile').find('svg').removeClass('d-none');
            if($(this).valid()){
                prData.r_payload = processRealisticPayload();
                var careerProfileKey = service.careerProfilesRef.push().key;
                firebase.database().ref("career_profiles/" + careerProfileKey).set(prData.r_payload, function(err){
                    if (err) {
                        console.log(err);
                        $('#submit_r_careerProfile').find('svg').addClass("d-none");
                        var msg = "There was a problem creating this profile. Please try again.";
                        $('#careerProfile-r-error').text(msg);
                        $('#careerProfile-r-error').removeClass("d-none");
                        $('#realisticCareerProfile').find('input:text').val('');
                        $('#realisticCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                        
                      } else {
                        $('#submit_r_careerProfile').find('svg').addClass("d-none");
                        var msg = "Your data has been successfully entered.";
                        $('#careerProfile-r-success').text(msg);
                        $('#careerProfile-r-success').removeClass("d-none");
                        $('#realisticCareerProfile').find('input:text').val('');
                        $('#realisticCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      }
                })
            }
        });

        // perform investigative form submission
        $('#investigativeCareerProfile').on('submit', function(e) {
            e.preventDefault();
            $('#submit_i_careerProfile').find('svg').removeClass('d-none');

            if($(this).valid()){
                prData.i_payload = processInvestigativePayload();
                var careerProfileKey = service.careerProfilesRef.push().key;
                firebase.database().ref("career_profiles/" + careerProfileKey).set(prData.i_payload, function(err){
                    if (err) {
                        console.log(err);
                        $('#submit_i_careerProfile').find('svg').addClass("d-none");
                        var msg = "There was a problem creating this profile. Please try again.";
                        $('#careerProfile-i-error').text(msg);
                        $('#careerProfile-i-error').removeClass("d-none");
                        $('#investigativeCareerProfile').find('input:text').val('');
                        $('#investigativeCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      } else {
                        $('#submit_i_careerProfile').find('svg').addClass("d-none");
                        var msg = "Your data has been successfully entered.";
                        $('#careerProfile-i-success').text(msg);
                        $('#careerProfile-i-success').removeClass("d-none");
                        $('#investigativeCareerProfile').find('input:text').val('');
                        $('#investigativeCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      }
                })
            }
        });

        // perform artistic form submission
        $('#artisticCareerProfile').on('submit', function(e) {
            e.preventDefault();
            $('#submit_a_careerProfile').find('svg').removeClass('d-none');

            if($(this).valid()){
                prData.a_payload = processArtisticPayload();
                var careerProfileKey = service.careerProfilesRef.push().key;
                firebase.database().ref("career_profiles/" + careerProfileKey).set(prData.a_payload, function(err){
                    if (err) {
                        console.log(err);
                        $('#submit_a_careerProfile').find('svg').addClass("d-none");
                        var msg = "There was a problem creating this profile. Please try again.";
                        $('#careerProfile-a-error').text(msg);
                        $('#careerProfile-a-error').removeClass("d-none");
                        $('#artisticCareerProfile').find('input:text').val('');
                        $('#artisticCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                        
                      } else {
                        $('#submit_a_careerProfile').find('svg').addClass("d-none");
                        var msg = "Your data has been successfully entered.";
                        $('#careerProfile-a-success').text(msg);
                        $('#careerProfile-a-success').removeClass("d-none");
                        $('#artisticCareerProfile').find('input:text').val('');
                        $('#artisticCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      }
                });
            }
        });

        // perform social form submission
        $('#socialCareerProfile').on('submit', function(e) {
            e.preventDefault();
            $('#submit_s_careerProfile').find('svg').removeClass('d-none');

            if($(this).valid()){
                prData.s_payload = processSocialPayload();
                var careerProfileKey = service.careerProfilesRef.push().key;
                firebase.database().ref("career_profiles/" + careerProfileKey).set(prData.s_payload, function(err){
                    if (err) {
                        console.log(err);
                        $('#submit_s_careerProfile').find('svg').addClass("d-none");
                        var msg = "There was a problem creating this profile. Please try again.";
                        $('#careerProfile-s-error').text(msg);
                        $('#careerProfile-s-error').removeClass("d-none");
                        $('#socialCareerProfile').find('input:text').val('');
                        $('#socialCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      } else {
                        $('#submit_s_careerProfile').find('svg').addClass("d-none");
                        var msg = "Your data has been successfully entered.";
                        $('#careerProfile-s-success').text(msg);
                        $('#careerProfile-s-success').removeClass("d-none");
                        $('#socialCareerProfile').find('input:text').val('');
                        $('#socialCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      }
                });
            }
        });

        // perform enterprising form submission
        $('#enterprisingCareerProfile').on('submit', function(e) {
            e.preventDefault();
            $('#submit_e_careerProfile').find('svg').removeClass('d-none');

            if($(this).valid()){
                prData.e_payload = processEnterprisingPayload();
                var careerProfileKey = service.careerProfilesRef.push().key;
                firebase.database().ref("career_profiles/" + careerProfileKey).set(prData.e_payload, function(err){
                    if (err) {
                        console.log(err);
                        $('#submit_e_careerProfile').find('svg').addClass("d-none");
                        var msg = "There was a problem creating this profile. Please try again.";
                        $('#careerProfile-e-error').text(msg);
                        $('#careerProfile-e-error').removeClass("d-none");
                        $('#enterprisingCareerProfile').find('input:text').val('');
                        $('#enterprisingCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      } else {
                        $('#submit_e_careerProfile').find('svg').addClass("d-none");
                        var msg = "Your data has been successfully entered.";
                        $('#careerProfile-e-success').text(msg);
                        $('#careerProfile-e-success').removeClass("d-none");
                        $('#enterprisingCareerProfile').find('input:text').val('');
                        $('#enterprisingCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      }
                });
            }
        });

        // perform conventional form submission
        $('#conventionalCareerProfile').on('submit', function(e) {
            e.preventDefault();
            $('#submit_c_careerProfile').find('svg').removeClass('d-none');

            if($(this).valid()){
                prData.c_payload = processConventionalPayload();
                var careerProfileKey = service.careerProfilesRef.push().key;
                firebase.database().ref("career_profiles/" + careerProfileKey).set(prData.c_payload, function(err){
                    if (err) {
                        console.log(err);
                        $('#submit_c_careerProfile').find('svg').addClass("d-none");
                        var msg = "There was a problem creating this profile. Please try again.";
                        $('#careerProfile-c-error').text(msg);
                        $('#careerProfile-c-error').removeClass("d-none");
                        $('#conventionalCareerProfile').find('input:text').val('');
                        $('#conventionalCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      } else {
                        $('#submit_c_careerProfile').find('svg').addClass("d-none");
                        var msg = "Your data has been successfully entered.";
                        $('#careerProfile-c-success').text(msg);
                        $('#careerProfile-c-success').removeClass("d-none");
                        $('#conventionalCareerProfile').find('input:text').val('');
                        $('#conventionalCareerProfile').find('textarea').val('');
                        $('input:checkbox').removeAttr('checked');
                        $('input, textarea').val('');
                        location.reload();
                      }
                });
            }
        });

        // define auto suggest for career clusters
        var cluster = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: unryddleApp.spliceNames(unryddleApp.loadData('careerClusters'))
        });
        cluster.initialize();
        $('.tokenfield-typeahead-ccluster').tokenfield({
            typeahead: [
                null,
                {
                    source: cluster.ttAdapter()
                }
            ],
            showAutocompleteOnFocus: true
        });

        // define auto suggest for career pathway
        var cPathway = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: unryddleApp.spliceNames(unryddleApp.loadData('careerPathways'))
        });
        cPathway.initialize();
        $('.tokenfield-typeahead-pathway').tokenfield({
            typeahead: [
                null,
                {
                    source: cPathway.ttAdapter()
                }
            ],
            showAutocompleteOnFocus: true
        });

        // define auto suggest for industry field
        var cIndustry = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: unryddleApp.spliceNames(unryddleApp.loadData("careerIndustries"))
        });
        cIndustry.initialize();
        $('.tokenfield-typeahead-industry').tokenfield({
            typeahead:[
                null,
                {
                    source: cIndustry.ttAdapter()
                }
            ],
            showAutocompleteOnFocus: true
        });

        // define auto suggestions for skills
        var cSkills = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: unryddleApp.spliceNames(unryddleApp.loadData("careerSkills"))
        });
        cSkills.initialize();
        $('.tokenfield-typeahead-cskills').tokenfield({
            typeahead: [
                null,
                {
                    source: cSkills.ttAdapter()
                }
            ],
            showAutocompleteOnFocus: true
        });

        // define auto suggestions for career knowledge set
        var cKnowledge = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: unryddleApp.spliceNames(unryddleApp.loadData("careerKnowledge"))
        });
        cKnowledge.initialize();
        $('.tokenfield-typeahead-cknowledge').tokenfield({
            typeahead: [
                null,
                {
                    source: cKnowledge.ttAdapter()
                }
            ],
            showAutocompleteOnFocus: true
        });

        // define suto suggestions for work values
        var ccWorkValues = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: unryddleApp.spliceNames(unryddleApp.loadData("careerWorkValues"))
        });
        ccWorkValues.initialize();
        $('.tokenfield-typeahead-workValues').tokenfield({
            typeahead: [
                null,
                {
                    source: ccWorkValues.ttAdapter()
                }
            ],
            showAutocompleteOnFocus: true
        });

        // define auto suggestion for workStyles
        var ccWorkStyles = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: unryddleApp.spliceNames(unryddleApp.loadData("careerWorkStyles"))
        });
        ccWorkStyles.initialize();
        $('.tokenfield-typeahead-workStyles').tokenfield({
            typeahead: [
                null,
                {
                    source: ccWorkStyles.ttAdapter()
                }
            ],
            showAutocompleteOnFocus: true
        });
    });
})(jQuery);
(function($) {
    'use strict';

    const localUser = unryddleApp.loadData('USER_DATA') ? unryddleApp.loadData('USER_DATA') : unryddleApp.loadData('VIEW_USER');
    const localID = unryddleApp.loadData('SEL_USER_ID') ? unryddleApp.loadData('SEL_USER_ID') : unryddleApp.loadData('VIEW_USER_ID');
    const userAuth = firebase.auth().currentUser;
    // Create Quill Text Editor.
    var Delta = Quill.import('delta');
    var quill = new Quill('#account-bio', {
        theme: 'snow'
    });

    // render profile data
    function renderProfileData(obj){
        if (obj.fullname){
            $('.acctName').text(obj.fullname).css("text-transform", "capitalize");
            $('.accountName').text(obj.fullname).css("text-transform", "capitalize")
            $('#account-fullname').val(obj.fullname);
        } else {
            $('.acctName').text('Not set. Please update your profile information');
            $('.accountName').text('Not set');
            $('#account-fullname').val('');
        }
    }

    // setup coutry listing
    print_country('account-country');
    console.log(localUser);
    console.log(localID);
    console.log(userAuth);
    // validate accout update form
    $('#editProfileForm').validate({
        rules: {
            "account-fullname": {
                required: true
            },
            "account-email": {
                required: true,
                email: true
            },
            "account-phone": {
                digits: true
            },
            "account-facebook": {
                url: true
            },
            "account-twitter": {
                url: true
            },
            "account-youtube": {
                url: true
            }
        }
    });
    var change = new Delta();
    quill.on('text-change', function(delta){
        console.log(change = change.compose(delta));
    });

    function generatePayload() {
        var obj = {};
        obj.fullname = $("#account-fullname").val();
        obj.email = $('#account-email').val();
        // obj
    }

    $('#editProfileForm').on('submit', function(e) {
        e.preventDefault();

        if($(this).valid()) {
            var payload = generatePayload();
        } else {
            console.log('form not valid..please update');
        }
    });
    renderProfileData(localUser);
}(jQuery))
(function($) {
  "use strict";

  const EDITOBJ = unryddleApp.loadData("EDIT_CAREER_PROFILE");
  const OBJID = unryddleApp.loadData("EDIT_CAREER_PROFILE_ID");

  const $loadprogress = $("#loadprogress");
  const $careerProfileForm = $("#updateCareerProfile");
  const $careerProfileName = $("#careerProfile_name");
  const $careerProfileIcon = $("#careerProfile_icon");
  const $careerPersonality = $('#careerProfile_personality');
  const $careerProfileJobTitles = $("#careerProfile_sampleJobTitles");
  const $careerProfileInterestCode = $("#careerProfile_IC");
  const $careerProfileCluster = $("#careerProfile_CC");
  const $careerProfilePathway = $("#careerProfile_CPW");
  const $careerProfileIndustry = $("#careerProfile_IN");
  const $careerProfileDefinition = $("#careerProfile_DEFN");
  const $careerProfileKeyTasks = $("#careerProfile_keyTasks");
  const $careerProfileKeySkills = $("#careerProfile_keySkills");
  const $careerProfileEducation = $("input[name='careerProfile_eduLevel']");
  const $careerProfileWages = $("#careerProfile_wages");
  const $careerProfileWorkTasks = $("#careerProfile_workTasks");
  const $careerProfileWorkActivities = $("#careerProfile_workActvs");
  const $careerDetailedWorkActivities = $('#careerProfile_detWorkActvs');
  const $careerProfileWorkContext = $("#careerProfile_workContext");
  const $careerProfileWorkStyles = $("#careerProfile_workStyles");
  const $careerProfilieWorkValues = $("#careerProfile_workValues");
  const $careerProfileKnowledge = $("#careerProfile_knowledge");
  const $careerProfileSkills = $("#careerProfile_skills");
  const $careerProfileAbilities = $("#careerProfile_abilities");
  const $careerProfileTools = $("#careerProfile_tools");
  const $careerProfileInternships = $("#careerProfile_internships");
  const $careerProfileApprenticeship = $("#careerProfile_apprenticeships");
  const $careerProfileOTJTraining = $("#careerProfile_otjTraining");
  const $careerProfileHobbies = $("#careerProfile_hobbies");
  const $careerRelOcc = $("#careerProfile_relatedOccupations");
  const $careerProfileSources = $("#careerProfile_sources");
  const $careerProfileError = $("#careerProfile-error");
  const $careerProfileSuccess = $("#careerProfile-success");
  const $careerProfileSubmitBtn = $("#submit_careerProfile");
  const $logout = $("#sign-out");

  function updateProfileData() {
    var obj = {};
    obj.name = $careerProfileName.val().trim();
    obj.personality = $careerPersonality.val().trim();
    obj.sampleJobTitles = unryddleApp.spliceArrDataCommaSpace($careerProfileJobTitles.val().trim());
    obj.interestCode = $careerProfileInterestCode.val().trim();
    obj.careerCluster = unryddleApp.spliceArrDataComma($careerProfileCluster.val().trim());
    obj.careerPathway = unryddleApp.spliceArrDataComma($careerProfilePathway.val().trim())
    obj.careerIndustry = unryddleApp.spliceArrDataComma($careerProfileIndustry.val().trim());
    obj.careerDefinition = $careerProfileDefinition.val().trim();
    obj.keyTasks = unryddleApp.spliceArrDataNewline($careerProfileKeyTasks.val().trim());
    obj.keySkills = unryddleApp.spliceArrDataNewline($careerProfileKeySkills.val().trim());
    obj.toolsUsed = unryddleApp.spliceArrDataNewline($careerProfileTools.val().trim());
    obj.careerKnowledge = unryddleApp.spliceArrDataComma($careerProfileKnowledge.val().trim());
    obj.careerSkills = unryddleApp.spliceArrDataComma($careerProfileSkills.val().trim());
    obj.careerAbilities = unryddleApp.spliceArrDataNewline($careerProfileAbilities.val().trim());
    obj.workActivities = unryddleApp.spliceArrDataNewline($careerProfileWorkActivities.val().trim());
    obj.detailedWorkActivities = unryddleApp.spliceArrDataNewline($careerDetailedWorkActivities.val().trim());
    obj.workStyles = unryddleApp.spliceArrDataComma($careerProfileWorkStyles.val().trim());
    obj.workValues = unryddleApp.spliceArrDataComma($careerProfilieWorkValues.val().trim());
    obj.relatedOccupations = unryddleApp.spliceArrDataNewline($careerRelOcc.val().trim());
    obj.careerEducation = unryddleApp.getCheckVals($("input[name='careerProfile_eduLevel']"));
    obj.careerSources = unryddleApp.spliceArrDataNewline($careerProfileSources.val().trim());
    obj.updatedOn = firebase.database.ServerValue.TIMESTAMP;

    if($('#careerProfile_icon').val() != ''){
      obj.careerIcon = $('#careerProfile_icon').val().trim();
    }
    if($('#careerProfile_wages').val() != ''){
        obj.careerWages = $('#careerProfile_wages').val().trim();
    }
    if($('#careerProfile_internships').val() != ''){
        obj.careerInternships = spliceArrData($('#careerProfile_internships').val().trim());
    }
    if($('#careerProfile_apprenticeships').val() != ''){
        obj.careerApprenticeships = spliceArrData($('#careerProfile_apprenticeships').val().trim());
    }
    if($('#careerProfile_otjTraining').val() != ''){
        obj.careerOTJTraining = spliceArrData($('#careerProfile_otjTraining').val().trim());
    }
    if($('#careerProfile_hobbies').val() != ''){
        obj.careerHobbies = spliceArrData($('#careerProfile_hobbies').val().trim());
    }

    return obj;
  }

  function loadEditData(obj) {
    console.log(obj);
    console.log(obj.personality);
    // main section
    $('.careerName').text(obj.name).css({'text-transform': 'capitalize'});
    $careerProfileName.val(obj.name);
    $careerProfileIcon.val(obj.icon);
    $careerProfileJobTitles.val(obj.sampleJobTitles);
    $careerPersonality.val(obj.personality);
    // overview section
    $careerProfileInterestCode.val(obj.interestCode);
    $careerProfileCluster.val(obj.careerCluster);
    // $careerProfilePath.val(obj.overview.careerPath);
    $careerProfilePathway.val(obj.careerPathway);
    $careerProfileIndustry.val(obj.careerIndustry);
    $careerProfileDefinition.val(obj.careerDefinition);
    // quick facts section
    $careerProfileKeyTasks.val(unryddleApp.displayArrData(obj.keyTasks));
    $careerProfileKeySkills.val(unryddleApp.displayArrData(obj.keySkills));
    $careerProfileEducation.val(obj.careerEducation);
    if(obj.careerWages === undefined) {
      $careerProfileWages.val('');
    } else {
      $careerProfileWages.val(obj.careerWages);
    }
    // jobDescription section
    $careerProfileWorkTasks.val(obj.workTasks);
    $careerProfileWorkStyles.val(obj.workStyles);
    $careerProfileWorkContext.val(obj.workContext);
    $careerProfileWorkActivities.val(unryddleApp.displayArrData(obj.workActivities));
    $careerDetailedWorkActivities.val(unryddleApp.displayArrData(obj.detailedWorkActivities));
    $careerProfilieWorkValues.val(obj.workValues);
    // knowledge skills section
    $careerProfileKnowledge.val(obj.careerKnowledge);
    $careerProfileSkills.val(obj.careerSkills);
    $careerProfileAbilities.val(unryddleApp.displayArrData(obj.careerAbilities));
    // tools anad tech section
    $careerProfileTools.val(unryddleApp.displayArrData(obj.toolsUsed));

    // work experience section
    if(obj.careerInternships === undefined){
      $careerProfileInternships.val('');
    } else {
      $careerProfileInternships.val(unryddleApp.displayArrData(obj.careerInternships));
    }
    if (obj.careerApprenticeships === undefined) {
      $careerProfileApprenticeship.val('');
    } else {
      $careerProfileApprenticeship.val(unryddleApp.displayArrData(obj.careerApprenticeships));
    }
    if (obj.careerOTJTraining === undefined) {
      $careerProfileOTJTraining.val('');
    } else {
      $careerProfileOTJTraining.val(unryddleApp.displayArrData(obj.careerOTJTraining));
    }
    
    // others section
    if(obj.careerHobbies === undefined) {
      $careerProfileHobbies.val('');
    } else {
      $careerProfileHobbies.val(unryddleApp.displayArrData(obj.careerHobbies));
    }
    
    $careerRelOcc.val(unryddleApp.displayArrData(obj.relatedOccupations));
    // reference section
    $careerProfileSources.val(unryddleApp.displayArrData(obj.careerSources));

    // set button data attr
    $careerProfileSubmitBtn.attr('data-id', OBJID);

  }

  loadEditData(EDITOBJ);

  $(document).ready(function() {

    // remove alert messages on input change
    $("input, select, textarea").on(
      "change keyup keydown paste click",
      function(e) {
        if (!$careerProfileError.hasClass("d-none")) {
          $careerProfileError.addClass("d-none");
        }

        if (!$careerProfileSuccess.hasClass("d-none")) {
          $careerProfileSuccess.addClass("d-none");
        }
      }
    );

    // logout function
    $logout.on("click", function(e) {
      e.preventDefault();
    
      service.signOut();
    });


    // career profile form validation
    $careerProfileForm.validate({
      rules: {
        'careerProfile_name': { required: true },
        'careerProfile_personality': { required: true },
        'careerProfile_sampleJobTitles': { required: true },
        'careerProfile_IC': { required: true },
        'careerProfile_CC': { required: true },
        'careerProfile_CP': { required: true },
        'careerProfile_CPW': { required: true },
        'careerProfile_IN': { required: true },
        'careerProfile_DEFN': { required: true },
        'careerProfile_keyTasks': { required: true },
        'careerProfile_keySkills': { required: true },
        'careerProfile_eduLevel': { required: true },
        'careerProfile_workTasks': { required: true },
        'careerProfile_workActvs': { required: true },
        'careerProfile_workStyles': { required: true },
        'careerProfile_workValues': { required: true },
        'careerProfile_knowledge': { required: true },
        'careerProfile_skills': { required: true },
        'careerProfile_abilities': { required: true },
        'careerProfile_tools': { required: true },
        'careerProfile_tech': { required: true },
        'careerProfile_sources': { required: true }
      }
    });


    $careerProfileForm.on('submit', function(e) {
      e.preventDefault;

      let spinner = $careerProfileSubmitBtn.find('span.fas.fa-spinner');
      spinner.removeClass('d-none');

      var updatePayload = updateProfileData();

      service.careerProfilesRef.child(OBJID).update(updatePayload, function(error){
        if (error) {
          console.log(error);
          spinner.addClass("d-none");
          let msg ="There was a problem updating this profile. Please try again.";
          $careerProfileError.text(msg);
          $careerProfileError.removeClass("d-none");
        } else {
          spinner.addClass("d-none");
          let msg = "Your data has been successfully updated.";
          $careerProfileSuccess.text(msg);
          $careerProfileSuccess.removeClass("d-none");
        }
      });

    });
  })
})(jQuery);

(function ($) {
  'use strict';

  var userMgmtPageData = {
    userAuth: unryddleApp.loadData(unryddleApp.userAuthData),
    userData: unryddleApp.loadData(unryddleApp.userData),
    selectedUser: null,
  }

  var $authContent = $('#authContent'),
  $adminListGroup = $('#adminGroup'),
  $editorListGroup = $('#editorGroup'),
  $curatorListGroup = $('#curatorGroup'),
  $counselorListGroup = $('#counselorGroup'),
  $expertsListGroup = $('#expertsGroup'),
  $facilitatorListGroup = $('#facilitatorsGroup'),
  $financeListGroup = $('#financeGroup'),
  $editUserModal = $('#editUserModalCenter'),
  $deleteUserModal = $('#deleteUserModalCenter'),
  $edituserFullName = $('#edituser-fullname'),
  $edituserEmail = $('#edituser-email'),
  $edituserRole = $('#edituser-role'),
  $edituserPhone = $('#edituser-phone'),
  $edituserBio = $('#edituser-bio'),
  $loadingbar = $('#loadprogress'),
  $userUpdateBtn = $('#updUserBtn'),
  $userDeleteBtn = $('#delUserBtn'),
  $editUserForm = $('#editUser'),
  $updateSuccess = $('#updateSuccess'),
  $updateError = $('#updateError'),
  $newUserFullName = $('#newUser_fullname'),
  $newUserEmail = $('#newUser_email'),
  $newUserRole = $('#newUser_Role'),
  $newUserPhone = $('#newUser_phone'),
  $newUserBio = $('#newUser_bio'),
  $newUserPassword = $('#newUser_password'),
  $newUserForm = $('#createUserForm'),
  $newUserBtn = $('#add_newUser'),
  $addUserSuccess = $('#addUser-success'),
  $addUserError = $('#addUser-error'),
  $logout = $('#logout-action');

  function checkUserStatus() {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        $authContent.removeClass('d-none');
        displayAdmins();
      } else {
        window.location.replace(service.baseURL);
      }
    })
  }

  // sign out function
  function signOut() {
    service.logout().then(function () {
      unryddleApp.clearData();
      window.location.replace(service.baseURL);
    }).catch(function (error) {
      console.log(error);
    });
  }

  function emptyUserData() {
    var html = "<h4 class='text-center mt-5 empty-header'>There are no users within this category. To add a new user please click on the <em>Add New User</em> link below.</h4>";

    return html;
  }

  function renderUserListItem(id, data) {
    console.log()
    var userName;
    if (data.fullname == undefined) {
      userName = 'N/A - Please update user profile to set Name';
    } else {
      userName = data.fullname;
    }
    var html = "<div class='list-group-item list-group-item-action flex-column align-items-start' id='" + id + "'" + ">" +
    "<div class='row w-100'>" +
    "<div class='col-2 text-right item-label'>" + "Name:" + "</div>" +
    "<div class='col-8 user_name'>" + userName + "</div>" +
    "<div class='col-2'>" +
    "<a href='#' id='view-user' class='user-action' data-id='" + id + "'" + ">" +
    "<i class='fas fa-eye'></i>" +
    "</a>" +
    "</div>" +
    "</div>" +
    "<div class='row w-100'>" +
    "<div class='col-2 text-right item-label'>" + "Email:" + "</div>" +
    "<div class='col-10 user_email'>" + data.email + "</div>" +
    "</div>" +
    "<div class='row w-100'>" +
    "<div class='col-2 text-right item-label'>" + "Role:" + "</div>" +
    "<div class='col-10 user_role'>" + data.role + "</div>" +
    "</div>" +
    "</div>";

    return html;


  }

  function populateUserEditForm(id, data) {
    var fullname, phone, email, shortbio, role;
    $userUpdateBtn.attr('data-id', id);
    console.log(id);
    if (data.fullname == undefined) {
      $edituserFullName.val('').attr('data-id', id);
    } else {
      $edituserFullName.val(data.fullname).attr('data-id', id);
    }
    if (data.short_bio == undefined) {
      $edituserBio.val('');
    } else {
      $edituserBio.val(data.short_bio);
    }
    if (data.phone == undefined) {
      $edituserPhone.val('');
    } else {
      $edituserPhone.val(data.phone);
    }
    if (data.email == undefined) {
      $edituserEmail.val('');
      $('.editUserEmail').text('');
    } else {
      $edituserEmail.val(data.email);
      $('.editUserEmail').text(data.email);
    }
    if (data.role == undefined) {
      $edituserRole.val('');
    } else {
      $edituserRole.val(data.role);
    }
  }

  // render user list in tab view
  function processUserData(role, data) {
    // process if admin users
    if (role == "admin") {
      if (data == null) {
        $adminListGroup.append($(emptyUserData()));
        $loadingbar.addClass('d-none');
      } else {
        for (var user in data) {
          if (data.hasOwnProperty(user)) {
            var singleUser = data[user];
            var userID = user;
            $adminListGroup.append($(renderUserListItem(userID, singleUser)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if editor users
    if (role == "editor") {
      if (data == null) {
        $editorListGroup.append($(emptyUserData()));
        $loadingbar.addClass('d-none');
      } else {
        for (var user in data) {
          if (data.hasOwnProperty(user)) {
            var singleUser = data[user];
            var userID = user;
            $editorListGroup.append($(renderUserListItem(userID, singleUser)));
            $loadingbar.addClass('d-none');
          }
        }
      }

    }

    // process if curator users
    if (role == "curator") {
      if (data == null) {
        $curatorListGroup.append($(emptyUserData()));
        $loadingbar.addClass('d-none');
      } else {
        for (var user in data) {
          if (data.hasOwnProperty(user)) {
            var singleUser = data[user];
            var userID = user;
            $curatorListGroup.append($(renderUserListItem(userID, singleUser)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if counselor users
    if (role == "counselor") {
      if (data == null) {
        $counselorListGroup.append($(emptyUserData()));
        $loadingbar.addClass('d-none');
      } else {
        for (var user in data) {
          if (data.hasOwnProperty(user)) {
            var singleUser = data[user];
            var userID = user;
            $counselorListGroup.append($(renderUserListItem(userID, singleUser)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if expert users
    if (role == "expert") {
      if (data == null) {
        $expertsListGroup.append($(emptyUserData()));
        $loadingbar.addClass('d-none');
      } else {
        for (var user in data) {
          if (data.hasOwnProperty(user)) {
            var singleUser = data[user];
            var userID = user;
            $expertsListGroup.append($(renderUserListItem(userID, singleUser)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if facilitator users
    if (role == "facilitator") {
      if (data == null) {
        $facilitatorListGroup.append($(emptyUserData()));
        $loadingbar.addClass('d-none');
      } else {
        for (var user in data) {
          if (data.hasOwnProperty(user)) {
            var singleUser = data[user];
            var userID = user;
            $facilitatorListGroup.append($(renderUserListItem(userID, singleUser)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if finance users
    if (role == "finance") {
      if (data == null) {
        $financeListGroup.append($(emptyUserData()));
        $loadingbar.addClass('d-none');
      } else {
        for (var user in data) {
          if (data.hasOwnProperty(user)) {
            var singleUser = data[user];
            var userID = user;
            $financeListGroup.append($(renderUserListItem(userID, singleUser)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

  }

  // process user action on button click
  function processAction(event) {
    let target = event.target;
    if (target.tagName === 'svg' || target.tagName === 'path') {
      var targetID = $(target).closest('a').data('id');

      if ($(target).closest('a').attr('id') == 'view-user') {
        // console.log(targetID)
        // console.log('let us edit  user');
        service.usersRef.child(targetID).once('value', function (snapshot) {
          var user_data = snapshot.val();
          unryddleApp.storeData('VIEW_USER', user_data);
          unryddleApp.storeData('VIEW_USER_ID', targetID);
          window.location.href = `view_user.html?userID=${targetID}`;
        });
        // $editUserModal.modal(unryddleApp.modalOptions);
      }

    }
  }

  // get existing admin users
  function displayAdmins() {
    var role = unryddleApp.userRoles[0];
    $loadingbar.removeClass('d-none');
    $adminListGroup.html('');
    service.getAdminUsers().then(function (snapshot) {
      var userlist = snapshot.val();
      processUserData(role, userlist);
    }).catch(function (error) {
      console.log(error)
    })
  }

  // get existing editor users
  function displayEditors() {
    var role = unryddleApp.userRoles[1];
    $loadingbar.removeClass('d-none');
    $editorListGroup.html('');
    service.getEditorUsers().then(function (snapshot) {
      var userlist = snapshot.val();
      processUserData(role, userlist);
    }).catch(function (error) {
      console.log(error);
    })
  }

  // get existing curators
  function displayCurators() {
    var role = unryddleApp.userRoles[2];
    $loadingbar.removeClass('d-none');
    $curatorListGroup.html('');
    service.getCuratorUsers().then(function (snapshot) {
      var userlist = snapshot.val();
      processUserData(role, userlist);
    }).catch(function (error) {
      console.log(error);
    });
  }

  // get existing counselors
  function displayCounselors() {
    var role = unryddleApp.userRoles[3];
    $loadingbar.removeClass('d-none');
    $counselorListGroup.html('');
    service.getCounselorUsers().then(function (snapshot) {
      var userlist = snapshot.val();
      processUserData(role, userlist);
    }).catch(function (error) {
      console.log(error);
    });
  }

  // get existing experts
  function displayExperts() {
    var role = unryddleApp.userRoles[4];
    $loadingbar.removeClass('d-none');
    $expertsListGroup.html('');
    service.getExpertUsers().then(function (snapshot) {
      var userlist = snapshot.val();
      processUserData(role, userlist);
    }).catch(function (error) {
      console.log(error)
    });
  }

  // get existing facilitators
  function displayFacilitators() {
    var role = unryddleApp.userRoles[5];
    $loadingbar.removeClass('d-none');
    $facilitatorListGroup.html('');
    service.getFacilitatorUsers().then(function (snapshot) {
      var userlist = snapshot.val();
      processUserData(role, userlist);
    }).catch(function (error) {
      console.log(error);
    })
  }

    // get existing facilitators
  function displayFinance() {
    var role = unryddleApp.userRoles[6];
    $loadingbar.removeClass('d-none');
    $financeListGroup.html('');
    service.getFinanceUsers().then(function (snapshot) {
      var userlist = snapshot.val();
      processUserData(role, userlist);
    }).catch(function (error) {
      console.log(error);
    })
  }

  // delete user function
  function deleteSelectedUser(event){
    let target = event.target;

    if(target.tagName === 'BUTTON'){
      let userID = $(target).attr('data-id');
      console.log(userID);
    }
  }


  $(document).ready(function () {
    checkUserStatus();

    // logout action
    $logout.on('click', function (e) {
      e.preventDefault();

      console.log('button works');
      signOut();

    });


    // user action click event(edit/delete)
    $(document).on('click', '.list-group .list-group-item .row .col-2 a', processAction);

    // close dynamic modal on click
    // reset modal form on close
    $editUserModal.on('hidden.bs.modal', function (e) {
      // console.log(e.target);
      // console.log('modal hidden, now reset form');
      $editUserForm.find('input, select').val('');
      if (!$updateError.hasClass('d-none')) {
        $updateError.addClass('d-none');
      }

      if (!$updateSuccess.hasClass('d-none')) {
        $updateSuccess.addClass('d-none');
      }
      displayAdmins();
      displayCounselors();
      displayCurators();
      displayEditors();
      displayExperts();
      displayFacilitators();
      displayFinance();
    })
    // $(document).on('click', '#editUserModalCenter .modal-dialog .modal-content .modal-header .close span', function(e) {
    //     console.log(e.target);
    //     $editUserModal.modal('hide');
    // });

    $('#user-admin-tab').tab('show');

    // validate add user form
    $newUserForm.validate({
      rules: {
        newUser_fullname: { required: true },
        newUser_email: { required: true, email: true },
        newUser_Role: { required: true },
        newUser_phone: { required: true, digits: true, maxlength: 10 },
        newUser_password: { required: true, minlength: 8 },
        newUser_confirm_password: {
          required: true,
          equalTo: "#newUser_password"
        }
      },
      messages:{
        newUser_confirm_password: {
          equalTo: "Passwords entered are not the same"
        }
      }
    });

    $newUserForm.on('submit', function(e) {
      e.preventDefault();
      var spinner = $(this).find('button#add_newUser span');
      spinner.removeClass('d-none');
      if($(this).valid()){
        console.log('form valid');
        var userPayload = {
          fullname: $newUserFullName.val(),
          role: $newUserRole.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        }

        var createUserPayload = {
          email: $newUserEmail.val(),
          password: $newUserPassword.val()
        }
        service.createUser(createUserPayload).then(function(user){
          userPayload.email = user.email;
          userPayload.verified = user.emailVerified;
          userPayload.displayPhoto = user.photoURL;
          service.usersRef.child(user.uid).set(userPayload, function(error){
            if(error){
              console.log(error);
              spinner.addClass('d-none');
              let message  = error.message;
              $addUserError.text(message);
              $addUserError.removeClass('d-none');
            } else {
              spinner.addClass('d-none');
              let message = "User account with email: <strong>"+ userPayload.email + "</strong> has been created.";
              $addUserSuccess.html(message);
              $addUserSuccess.removeClass('d-none');
            }
          })
        }).catch(function(error) {
          spinner.addClass('d-none');
          let message = "We cannot create the account at this time. Please try again in a few minutes";
          $addUserError.text(message);
          $addUserError.removeClass('d-none');
        });
      } else {
        spinner.addClass('d-none');
      }
    });

    $('input, textarea, select').on('change keyup keydown paste click', function(e) {
      if(!$addUserSuccess.hasClass('d-none')) {
        $addUserSuccess.addClass('d-none');
      }

      if(!$addUserError.hasClass('d-none')) {
        $addUserError.addClass('d-none');
      }
    })


    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var tabTarget = $(e.target).attr('id');

      // display admins
      if (tabTarget == 'user-admin-tab') {
        // console.log('let us display admin users');
        displayAdmins();
      }

      // display editors
      if (tabTarget == 'user-editor-tab') {
        // console.log('let us display editor users');
        displayEditors();
      }

      // display curators
      if (tabTarget == 'user-curator-tab') {
        // console.log('let us display curators');
        displayCurators();
      }

      // display counselors
      if (tabTarget == 'user-counselor-tab') {
        // console.log('let us display counselors');
        displayCounselors();
      }

      // display experts
      if (tabTarget == 'user-expert-tab') {
        // console.log('let us display experts');
        displayExperts();
      }

      // display facilitators
      if (tabTarget == 'user-facilitator-tab') {
        // console.log('let us display facilitators');
        displayFacilitators();
      }

      // display finance
      if (tabTarget == 'user-finance-tab') {
        // console.log('let us display facilitators');
        displayFinance();
      }


    })

    // process update form
    $editUserForm.on('submit', function (e) {
      e.preventDefault();
      $(this).find('#updUserBtn span.fa.fa-spinner').removeClass('d-none');
      var userRef = service.usersRef.child($userUpdateBtn.attr('data-id'));
      var payload = {
        fullname: $edituserFullName.val(),
        email: $edituserEmail.val(),
        role: $edituserRole.find(':selected').val(),
        phone: $edituserPhone.val(),
        short_bio: $edituserBio.val(),
        updatedOn: firebase.database.ServerValue.TIMESTAMP
      };
      userRef.update(payload, function (error) {
        if (error) {
          console.log(error);
          $updateError.text('There was a problem updating. Please check your internet connection and try again.');
          $updateError.removeClass('d-none');
          $userUpdateBtn.find('span').addClass('d-none');
        } else {
          $updateSuccess.text('User profile has been successfully updated');
          $updateSuccess.removeClass('d-none');
          $userUpdateBtn.find('span').addClass('d-none');
        }
      })

    });

    // delete user action
    $(document).on('click', '#deleteUserModalCenter .modal-dialog.modal-dialog-centered .modal-content .modal-footer #delUserBtn', deleteSelectedUser);


  });
}(jQuery))

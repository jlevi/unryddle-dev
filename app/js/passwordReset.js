    (function($){
        'use strict';

        var passwordResetData = {

        };

        var $resetForm = $('#resetPW'),
            $newPassword = $('#new_password'),
            $resetButton = $('#reset-btn'),
            $resetSuccess = $('#reset-success'),
            $resetError = $('#reset-error');

        

        $(document).ready(function() {
            
            $resetForm.validate({
                rules: {
                    new_password:{
                        required: true,
                        minlength: 7
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password"
                    }
                }
            });

            $resetForm.on('submit', function(e){
                e.preventDefault();

                if($(this).valid()){
                    var spinner = $resetButton.find('span.fa.fa-spinner');
                    let payload = {
                        newPassword: $newPassword.val(),
                        code: unryddleApp.getAllUrlParams().oobcode
                    }
                    let homeLink = "http://" + window.location.host;
                    spinner.removeClass('d-none');
                    service.sendResetPW(payload.code, payload.newPassword).then(function(){
                        let successmsg = "You have successfully reset your password. <a href='" + homeLink + "'" + "><strong>Click here</strong></a> to login with your new password.";
                        $resetSuccess.html(successmsg);
                        $resetSuccess.removeClass('d-none');
                        spinner.addClass('d-none');
                    }).catch(function(error){
                        let errcode = error.code;
                        if(errcode == 'auth/expired-action-code'){
                            let msg = "Reset code has expired. <a href='" + homeLink + "'" + "><strong>Click here</strong></a> to resend a reset request.";
                            $resetError.html(msg);
                            $resetError.removeClass('d-none');
                            spinner.addClass('d-none');
                        }else if(errcode == 'auth/invalid-action-code'){
                            let msg = "This reset code is invalid. <a href='" + homeLink + "'" + "><strong>Click here</strong></a> to try again.";
                            $resetError.html(msg);
                            $resetError.removeClass('d-none');
                            spinner.addClass('d-none');
                        }else if(errcode == 'auth/user-disabled'){
                            let msg = "This user account has been disabled. Kindly request admin to re-enable it.";
                            $resetError.text(msg);
                            $resetError.removeClass('d-none');
                            spinner.addClass('d-none');
                        }else if(errcode == 'auth/user-not-found'){
                            let msg = "This user account has been deleted. Kindly request for a new account from the admin";
                            $resetError.text(msg);
                            $resetError.removeClass('d-none');
                            spinner.addClass('d-none');
                        }else if(errcode == 'auth/weak-password'){
                            let msg = "The password entered is weak. Use a stronger password mixing letters, characters and numbers";
                            $resetError.text(msg);
                            $resetError.removeClass('d-none');
                            spinner.addClass('d-none');
                        } else {
                            let msg = "A problem occured whilst processing your request. Please check your internet connection and try again";
                            $resetError.text(msg);
                            $resetError.removeClass('d-none');
                            spinner.addClass('d-none');
                        }
                    });

                }
            })
        });
    }(jQuery))
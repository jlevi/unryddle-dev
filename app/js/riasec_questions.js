(function ($) {
  'use strict';

  const raisecQuestionsData = {
    questionSuccessMsg: "Your question was successfully added",
    questionUpdateSuccess: "Question data successfully updated",
    questionPersonality: null,
    updateSuccessMsg: "Question data was successfully updated",
    selectedTab: null,

  }

  const $jhsQuestionForm = $('#jhsQuestionsForm')
  const $jhsQuestionBtn = $('#submit_jhs_question');
  const $jhsQuestionTitle = $('#jhs_question_name');
  const $jhsQuestionSuccess = $('#jhs_question-success');
  const $jhsQuestionError = $('#jhs_question-error');
  const $shsQuestionForm = $('#shsQuestionsForm');
  const $shsQuestionTitle = $('#shs_question_name');
  const $uniQuestionForm = $('#uniQuestionsForm');
  const $uniQuestionTitle = $('#uni_question_name');
  const $shsQuestionSuccess = $('#shs_question-success');
  const $shsQuestionError = $('#shs_question-error');
  const $uniQuestionSuccess = $('#uni_question-success');
  const $uniQuestionError = $('#uni_question-error');
  const $editQuestionModal = $('#editRAISECQUESTION');
  const $deleteQuestionModal = $('#deleteQuestion');
  const $editQuestionName = $('#edit_question_name');
  const $editEducationLevel = $('#edit_education_level');
  const $editPersonalityLevel = $('#edit_personality_level');
  const $editQuestionBtn = $('#update_question_btn');
  const $editQuestionForm = $('#editQuestion');
  const $updateSuccess = $('#updateSuccess');
  const $updateError = $('#updateError');
  const $deleteButton = $('#del_question_btn');
  const $deleteQuestionName = $('.del-q_name');
  const $loadingbar = $('#loadprogress');

  // sign out functio
  function signOut() {
    service.logout().then(function () {
      unryddleApp.clearData();
      window.location.replace(service.baseURL);
    }).catch(function (error) {
      console.log(error);
    });
  }


  function renderSuccessMsg(str, handler) {
    handler.text(str);
    handler.removeClass('d-none');
    setTimeout(function() {
      handler.addClass('d-none');
    }, 6000);
    $("input[type='text'], textarea, select").val('');
  }

  function renderErrorMsg(error, handler) {
    let str;
    if (error.code == "auth/user-not-found") {
      str = "User does not exist. Please ensure you have entered a valid email and password."
    } else {
      str = "There was a problem uploading your data. Please try again in a few minutes"
    }
    handler.text(str);
    handler.removeClass('d-none');
    setTimeout(function() {
      handler.addClass('d-none');
    }, 6000);
    $("input[type='text'], textarea, select").val('');
  }

  function emptyQuestionData() {
    let html = "<h4 class='text-center mt-5 empty-header'>No questions have been set for this section. Add a new question using the link above.</h4>";

    return html;
  }

  function processUNIData(data){
    console.log(data);
    if (data == null) {
      $('.empty-header').text('');
      $('#universityP .feedlist').append($(emptyQuestionData()));
      $('#loadprogress').addClass('d-none');
    } else {
      var listData = '';
      var count = 0;
      $('#universityP .feedlist').html('');
      for (let question in data) {
        if (data.hasOwnProperty(question)) {
          $('#loadprogress').addClass('d-none');
          let singleQuestion = data[question];
          let questionID = question;
          count++;
          listData = listData.concat(
            `<li id=${questionID}>
            <div class="bg-light-info"><div class="count">${count}</div></div>
            <span class='questioncontent'>${singleQuestion.question}</span>
            <span class='text-right float-right'>
            <a href='#' id='edit-question' data-id=${questionID}>
            <i class="fas fa-edit"></i>
            </a>
            <a href='#' id='del-question' data-id=${questionID}>
            <i class="fas fa-trash"></i>
            </a>
            </span>
            </li>`
          );
        }
      }
      $('#universityP .feedlist').html(listData);
    }
  }

  function processSHSData(data){
    if (data == null) {
      $('.empty-header').text('');
      $('#shsP .feedlist').append($(emptyQuestionData()));
      $("#loadprogress").addClass('d-none');
    } else {
      var listData = '';
      var count = 0;
      $('#shsP .feedlist').html('');
      for (let question in data) {
        if (data.hasOwnProperty(question)) {
          $("#loadprogress").addClass('d-none');
          let singleQuestion = data[question];
          let questionID = question;
          count++
          listData = listData.concat(
            `<li id=${questionID}>
            <div class="bg-light-info"><div class="count">${count}</div></div>
            <span class='questioncontent'>${singleQuestion.question}</span>
            <span class='text-right float-right'>
            <a href='#' id='edit-question' data-id=${questionID}>
            <i class="fas fa-edit"></i>
            </a>
            <a href='#' id='del-question' data-id=${questionID}>
            <i class="fas fa-trash"></i>
            </a>
            </span>
            </li>`
          );
        }
      }
      $('#shsP .feedlist').html(listData);
    }
  }

  function processJHSData(data) {
    // console.log(data);
      if(data == null) {
        $('.empty-header').text('');
        $('#jhsP .feedlist').append($(emptyQuestionData()));
        $("#loadprogress").addClass('d-none');
      } else {
        var listData = '';
        var count = 0;
        $('#jhsP .feedlist').html('');
        for(let question in data) {
          if(data.hasOwnProperty(question)){
            $("#loadprogress").addClass('d-none');
            let singleQuestion = data[question];
            let questionID = question;
            count++;
            listData = listData.concat(
              `<li id=${questionID}>
              <div class="bg-light-info"><div class="count">${count}</div></div>
              <span class='questioncontent'>${singleQuestion.question}</span>
              <span class='text-right float-right'>
              <a href='#' id='edit-question' data-id=${questionID}>
              <i class="fas fa-edit"></i>
              </a>
              <a href='#' id='del-question' data-id=${questionID}>
              <i class="fas fa-trash"></i>
              </a>
              </span>
              </li>`
            );
          }
        }
        $('#jhsP .feedlist').html(listData);
    }
  }

  function processRealisticMarkers(obj) {
    if(obj == null) {
      $('.empty-header').text('');
      $('#realisticP .feedlist').append($(emptyQuestionData()));
      $("#loadprogress").addClass('d-none');
    } else {
      var listData = '';
      var count = 0;
      $("#realisticP .feedlist").html('');
      for(let item in obj) {
        if(obj.hasOwnProperty(item)) {
          $('#loadprogress').addClass('d-none');
          let marker = obj[item];
          let markerID = item;
          count++;
          listData = listData.concat(
            `<li id=${markerID}>
              <div class="bg-light-info"><div class="count">${count}</div></div>
              <span class='questioncontent'>${marker.question}</span>
              <span class='text-right float-right'>
              <a href='#' id='edit-question' data-id=${markerID}>
              <i class="fas fa-edit"></i>
              </a>
              <a href='#' id='del-question' data-id=${markerID}>
              <i class="fas fa-trash"></i>
              </a>
              </span>
              </li>`
          )
        }
      }
      $('#realisticP .feedlist').html(listData);
    }
  }

  function processInvestigativeMarkers(obj) {
    if(obj == null) {
      $('.empty-header').text('');
      $('#investigativeP .feedlist').append($(emptyQuestionData()));
      $("#loadprogress").addClass('d-none');
    } else {
      var listData = '';
      var count = 0;
      $("#investigativeP .feedlist").html('');
      for(let item in obj) {
        if(obj.hasOwnProperty(item)) {
          $('#loadprogress').addClass('d-none');
          let marker = obj[item];
          let markerID = item;
          count++;
          listData = listData.concat(
            `<li id=${markerID}>
              <div class="bg-light-info"><div class="count">${count}</div></div>
              <span class='questioncontent'>${marker.question}</span>
              <span class='text-right float-right'>
              <a href='#' id='edit-question' data-id=${markerID}>
              <i class="fas fa-edit"></i>
              </a>
              <a href='#' id='del-question' data-id=${markerID}>
              <i class="fas fa-trash"></i>
              </a>
              </span>
              </li>`
          )
        }
      }
      $('#investigativeP .feedlist').html(listData);
    }
  }

  function processArtisticMarkers(obj) {
    if(obj == null) {
      $('.empty-header').text('');
      $('#artisticP .feedlist').append($(emptyQuestionData()));
      $("#loadprogress").addClass('d-none');
    } else {
      var listData = '';
      var count = 0;
      $("#artisticP .feedlist").html('');
      for(let item in obj) {
        if(obj.hasOwnProperty(item)) {
          $('#loadprogress').addClass('d-none');
          let marker = obj[item];
          let markerID = item;
          count++;
          listData = listData.concat(
            `<li id=${markerID}>
              <div class="bg-light-info"><div class="count">${count}</div></div>
              <span class='questioncontent'>${marker.question}</span>
              <span class='text-right float-right'>
              <a href='#' id='edit-question' data-id=${markerID}>
              <i class="fas fa-edit"></i>
              </a>
              <a href='#' id='del-question' data-id=${markerID}>
              <i class="fas fa-trash"></i>
              </a>
              </span>
              </li>`
          )
        }
      }
      $('#artisticP .feedlist').html(listData);
    }
  }

  function processSocialMarkers(obj) {
    if(obj == null) {
      $('.empty-header').text('');
      $('#socialP .feedlist').append($(emptyQuestionData()));
      $("#loadprogress").addClass('d-none');
    } else {
      var listData = '';
      var count = 0;
      $("#socialP .feedlist").html('');
      for(let item in obj) {
        if(obj.hasOwnProperty(item)) {
          $('#loadprogress').addClass('d-none');
          let marker = obj[item];
          let markerID = item;
          count++;
          listData = listData.concat(
            `<li id=${markerID}>
              <div class="bg-light-info"><div class="count">${count}</div></div>
              <span class='questioncontent'>${marker.question}</span>
              <span class='text-right float-right'>
              <a href='#' id='edit-question' data-id=${markerID}>
              <i class="fas fa-edit"></i>
              </a>
              <a href='#' id='del-question' data-id=${markerID}>
              <i class="fas fa-trash"></i>
              </a>
              </span>
              </li>`
          )
        }
      }
      $('#socialP .feedlist').html(listData);
    }
  }

  function processEnterprisingMarkers(obj) {
    if(obj == null) {
      $('.empty-header').text('');
      $('#enterprisingP .feedlist').append($(emptyQuestionData()));
      $("#loadprogress").addClass('d-none');
    } else {
      var listData = '';
      var count = 0;
      $("#enterprisingP .feedlist").html('');
      for(let item in obj) {
        if(obj.hasOwnProperty(item)) {
          $('#loadprogress').addClass('d-none');
          let marker = obj[item];
          let markerID = item;
          count++;
          listData = listData.concat(
            `<li id=${markerID}>
              <div class="bg-light-info"><div class="count">${count}</div></div>
              <span class='questioncontent'>${marker.question}</span>
              <span class='text-right float-right'>
              <a href='#' id='edit-question' data-id=${markerID}>
              <i class="fas fa-edit"></i>
              </a>
              <a href='#' id='del-question' data-id=${markerID}>
              <i class="fas fa-trash"></i>
              </a>
              </span>
              </li>`
          )
        }
      }
      $('#enterprisingP .feedlist').html(listData);
    }
  }

  function processConventionalMarkers(obj) {
    if(obj == null) {
      $('.empty-header').text('');
      $('#conventionalP .feedlist').append($(emptyQuestionData()));
      $("#loadprogress").addClass('d-none');
    } else {
      var listData = '';
      var count = 0;
      $("#conventionalP .feedlist").html('');
      for(let item in obj) {
        if(obj.hasOwnProperty(item)) {
          $('#loadprogress').addClass('d-none');
          let marker = obj[item];
          let markerID = item;
          count++;
          listData = listData.concat(
            `<li id=${markerID}>
              <div class="bg-light-info"><div class="count">${count}</div></div>
              <span class='questioncontent'>${marker.question}</span>
              <span class='text-right float-right'>
              <a href='#' id='edit-question' data-id=${markerID}>
              <i class="fas fa-edit"></i>
              </a>
              <a href='#' id='del-question' data-id=${markerID}>
              <i class="fas fa-trash"></i>
              </a>
              </span>
              </li>`
          )
        }
      }
      $('#conventionalP .feedlist').html(listData);
    }
  }

  function getJHSQuestions() {
    $('#loadprogress').removeClass('d-none');
    service.getAllJHSQuestions().then(function (snapshot) {
      let data = snapshot.val();
      processJHSData(data);
    }).catch(function (error) {
      $('#loadprogress').addClass('d-none');
      console.log(error);
    })
  }

  function getSHSQuestions() {
    $('#loadprogress').removeClass('d-none');
    service.getAllSHSQuestions().then(function (snapshot) {
      let data = snapshot.val();
      processSHSData(data);
    }).catch(function (error) {
      $('#loadprogress').addClass('d-none');
      console.log(error);
    });
  }

  function getUNIQuestions() {
    $('#loadprogress').removeClass('d-none');
    service.getAllUNIQuestions().then(function (snapshot) {
      let data = snapshot.val();
      processUNIData(data);
    }).catch(function (error) {
      $('#loadprogress').addClass('d-none');
      console.log(error);
    })
  }

  function getRealisticQuestions() {
    $('#loadprogress').removeClass('d-none');
    service.getAllRealisticMarkers().then(function(snapshot) {
      let data = snapshot.val();
      processRealisticMarkers(data);
    }).catch(function(error) {
      $('#loadprogress').addClass('d-none');
      console.log(error);
    });
  }

  function getInvestigativeQuestions() {
    $('#loadprogress').removeClass('d-none');
    service.getAllinvestigativeMarkers().then(function(snapshot) {
      let data = snapshot.val();
      processInvestigativeMarkers(data);
    }).catch(function(error) {
      $('#loadprogress').addClass('d-none');
      console.log(error);
    });
  }

  function getArtisticQuestions() {
    $('#loadprogress').removeClass('d-none');
    service.getAllArtisticMarkers().then(function(snapshot) {
      let data = snapshot.val();
      processArtisticMarkers(data);
    }).catch(function(error) {
      $('#loadprogress').addClass('d-none');
      console.log(error);
    });
  }

  function getSocialQuestions() {
    $('#loadprogress').removeClass('d-none');
    service.getAllSocialMarkers().then(function(snapshot) {
      let data = snapshot.val();
      processSocialMarkers(data);
    }).catch(function(error) {
      $('#loadprogress').addClass('d-none');
      console.log(error);
    });
  }

  function getEnterprisingQuestions() {
    $('#loadprogress').removeClass('d-none');
    service.getAllEnterprisingMarkers().then(function(snapshot) {
      let data = snapshot.val();
      processEnterprisingMarkers(data);
    }).catch(function(error) {
      $('#loadprogress').addClass('d-none');
      console.log(error);
    });
  }

  function getConventionalQuestions() {
    $('#loadprogress').removeClass('d-none');
    service.getAllConventionalMarkers().then(function(snapshot) {
      let data = snapshot.val();
      processConventionalMarkers(data);
    }).catch(function(error) {
      $('#loadprogress').addClass('d-none');
      console.log(error);
    });
  }

  function getRadioVals(obj) {
    let personality;
    obj.each(function() {
      let $this = $(this);
      if (($this).prop('checked')) {
        personality = $(this).val();
      }
    });

    return personality;
  }

  function renderDeleteContent(id, obj){
    $deleteButton.attr('data-id', id);
    $deleteQuestionName.text(obj.question);
  }

  function editQuestionAction(event) {
    let target = event.target;
    if (target.tagName === 'I') {
      let targetID = $(target).closest('a').data('id');

      if ($(target).closest('a').attr('id') == 'edit-question') {
        service.raisecQuestionsRef.child(targetID).once('value', function(snapshot) {
          let data = snapshot.val();
          $editQuestionForm.find('input, select').val('');
          populateQuestionEditForm(targetID, data);
        });
        $editQuestionModal.modal(unryddleApp.modalOptions);
      }

      if($(target).closest('a').attr('id') == 'del-question') {
        service.raisecQuestionsRef.child(targetID).once('value', function(snapshot) {
          let data = snapshot.val();
          renderDeleteContent(targetID, data);
        })
        $deleteQuestionModal.modal(unryddleApp.modalOptions);
      }
    }
  }

  function populateQuestionEditForm(id, obj) {
    $editQuestionBtn.attr('data-id', id);

    $editQuestionName.val(obj.question);
    $editEducationLevel.val(obj.education_level);
    $editPersonalityLevel.val(obj.personality_type);
  }



  $(document).ready(function () {
    console.log('page ready.....');

    getJHSQuestions();
    getRealisticQuestions();


    // store active tab data
    $("a[data-toggle='tab']").on('show.bs.tab', function(e) {
      console.log($(e.target).attr('href'));
    })

    // render respective calls when tab is shown
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      let tabTarget = $(e.target).attr('id');

      if (tabTarget == 'jhs-tab') {
        raisecQuestionsData.selectedTab = 'jhs';
        console.log('jhs tab');
        getJHSQuestions();
      }

      if (tabTarget == 'shs-tab') {
        raisecQuestionsData.selectedTab = 'shs';
        console.log('shs tab');
        getSHSQuestions();
      }

      if (tabTarget == 'uni-tab') {
        raisecQuestionsData.selectedTab = 'university';
        console.log('uni tab');
        getUNIQuestions();
      }

      if(tabTarget == 'r-tab') {
        console.log('realistic tab');
        getRealisticQuestions();
      }

      if(tabTarget == 'i-tab') {
        console.log('investigative tab');
        getInvestigativeQuestions();
      }

      if(tabTarget == 'a-tab') {
        console.log('artistic tab');
        getArtisticQuestions();
      }

      if(tabTarget == 's-tab') {
        console.log('social tab');
        getSocialQuestions();
      }

      if(tabTarget == 'e-tab') {
        console.log('enterprising tab');
        getEnterprisingQuestions();
      }

      if(tabTarget == 'c-tab') {
        console.log('conventional tab');
        getConventionalQuestions();
      }
    });

    // modify questions buttons action
    $(document).on('click', '.list-group .list-group-item .float-right a i', editQuestionAction);

    // logout action
    $('#sign-out').on('click', function (e) {
        e.preventDefault();
        // console.log('button works');
        signOut();

    });

    // submit update question form
    $editQuestionForm.on('submit', function(e) {
      e.preventDefault();

      $editQuestionBtn.find('span').removeClass('d-none');
      let questionID = $editQuestionBtn.attr('data-id');
      let payload = {
        question: $editQuestionName.val(),
        education_level: $editEducationLevel.val(),
        personality_type: $editPersonalityLevel.val()
      };
      let questionUpdateData = {};
      questionUpdateData['raisec_questions/' + questionID] = payload;
      questionUpdateData['app_questions/' + questionID] = payload;
      firebase.database().ref().update(questionUpdateData, function(error) {
        if (error) {
          console.log(error);
          $editQuestionBtn.find('span').addClass('d-none');
          renderErrorMsg(error, $updateError);
        } else {
          $editQuestionBtn.find('span').addClass('d-none');
          getJHSQuestions();
          renderSuccessMsg(raisecQuestionsData.updateSuccessMsg, $updateSuccess);
        }
      });
    });

    // delete question button action
    $deleteButton.on('click', function(e) {
      e.preventDefault();

      let targetID = $(this).attr('data-id');
      let deleteData = {};
      deleteData['raisec_questions/' + targetID] = null;
      deleteData['app_questions/' + targetID] = null;
      firebase.database().ref().update(deleteData, function(error){
        if(error){
          console.log("There was an error: " + error.message);
          renderErrorMsg(error, $deleteError);
        } else {
          console.log('data was deleted...');
          getJHSQuestions();
          getSHSQuestions();
          getUNIQuestions();
          $deleteQuestionModal.modal('hide');
        }
      });
      // service.raisecQuestionsRef.child(targetID).remove().then(function() {
        
      // }).catch(function(error) {
        
      // })
    })

  })
}(jQuery))

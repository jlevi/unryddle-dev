// main service file for unryddleApp
// contains core API calls and service functions

var service = {
  // firebase root references
  baseURL: "http://" + window.location.hostname + ":" + window.location.port,
  prodURL: "",
  rootDB: firebase.database(),
  rootRef: firebase.database().ref(),
  rootAuth: firebase.auth(),
  rootStorage: firebase.storage(),
  rootStorageRef: firebase.storage().ref(),
  usersRef: firebase.database().ref("users"),
  personalitiesRef: firebase.database().ref("personalities"),
  studentSurveysRef: firebase.database().ref("student_surveys"),
  publicSurveyRef: firebase.database().ref("public_survey"),
  corporateSurveyRef: firebase.database().ref("corporate_survey"),
  raisecQuestionsRef: firebase.database().ref("raisec_questions"),
  appQuestionsRef: firebase.database().ref("app_questions"),
  careerPathRef: firebase.database().ref("career_path"),
  careerClusterRef: firebase.database().ref("career_clusters"),
  careerPathwayRef: firebase.database().ref("career_pathways"),
  careerIndustryRef: firebase.database().ref("career_industries"),
  workContextRef: firebase.database().ref("work_contexts"),
  workStylesRef: firebase.database().ref("work_styles"),
  workValuesRef: firebase.database().ref("work_values"),
  careerKnowledgeRef: firebase.database().ref("career_knowledge"),
  careerSkillsRef: firebase.database().ref("career_skills"),
  careerTechRef: firebase.database().ref("career_technologies"),
  highSchoolCoursesRef: firebase.database().ref("high_school_courses"),
  hndCoursesRef: firebase.database().ref("hnd_courses"),
  underGradCoursesRef: firebase.database().ref("undergraduate_courses"),
  certCoursesRef: firebase.database().ref("certification_courses"),
  gradDegreesRef: firebase.database().ref("graduate_degree_courses"),
  doctoralCoursesRef: firebase.database().ref("doctoral_courses"),
  careerProfilesRef: firebase.database().ref("career_profiles"),
  quotesRef: firebase.database().ref('app_quotes'),
  testsTakenRef: firebase.database().ref('tests_taken'),
  webinarsRef: firebase.database().ref('webinars'),
  appLessonsRef: firebase.database().ref('app_lessons'),
  instructorsRef: firebase.database().ref('instructors'),
  filesStorageRef: firebase.storage().ref('files'),     // file storage ref
  imagesStorageRef: firebase.storage().ref('images'),  // image storage ref
  videoStorageRef: firebase.storage().ref('videos'),  // video storage ref

  // login function
  login: function(payload) {
    var xhr = service.rootAuth.signInWithEmailAndPassword(
      payload.email,
      payload.password
    );

    return xhr;
  },

  // logout function
  logout: function() {
    var xhr = service.rootAuth.signOut();

    return xhr;
  },

  signOut: function() {
    service.logout().then(function () {
      unryddleApp.clearData();
      window.location.replace(service.baseURL);
    }).catch(function (error) {
      console.log(error);
    });
  },

  // user management functions
  // 1. create user
  createUser: function(payload) {
    var xhr = service.rootAuth.createUserWithEmailAndPassword(
      payload.email,
      payload.password
    );

    return xhr;
  },

  // 2. add user
  addUser: function() {},

  // 3. verify email account
  verifyEmail: function() {
    let user = service.rootAuth.currentUser;
    console.log(user);
    var xhr = user.sendEmailVerification();

    return xhr;
  },

  valueChange: function(dataSnapshot) {
    return dataSnapshot.val();
    // return res;
  },

  // send reset user email
  sendResetEmail: function(email) {
    var xhr = service.rootAuth.sendPasswordResetEmail(email);
    return xhr;
  },

  // send new reset password
  sendResetPW: function(code, newPassword) {
    var xhr = service.rootAuth.confirmPasswordReset(code, newPassword);
    return xhr;
  },

  // get admin users
  getAdminUsers: function() {
    var userRef = firebase.database().ref("users");
    var xhr = userRef
      .orderByChild("role")
      .equalTo("admin")
      .once("value");

    return xhr;
    // console.log(service.rootDB);
  },

  // get Editor users
  getEditorUsers: function() {
    var userRef = firebase.database().ref("users");
    var xhr = userRef
      .orderByChild("role")
      .equalTo("editor")
      .once("value");

    return xhr;
  },

  // get curators
  getCuratorUsers: function() {
    var userRef = firebase.database().ref("users");
    var xhr = userRef
      .orderByChild("role")
      .equalTo("curator")
      .once("value");

    return xhr;
  },

  // get counselors
  getCounselorUsers: function() {
    var userRef = firebase.database().ref("users");
    var xhr = userRef
      .orderByChild("role")
      .equalTo("counselor")
      .once("value");

    return xhr;
  },

  // get experts
  getExpertUsers: function() {
    var userRef = firebase.database().ref("users");
    var xhr = userRef
      .orderByChild("role")
      .equalTo("expert")
      .once("value");

    return xhr;
  },

  // get professionals
  getFacilitatorUsers: function() {
    var userRef = firebase.database().ref("users");
    var xhr = userRef
      .orderByChild("role")
      .equalTo("professional")
      .once("value");

    return xhr;
  },

  // get finance users
  getFinanceUsers: function() {
    var userRef = firebase.database().ref('users');
    var xhr = userRef.orderByChild("role").equalTo('finance').once('value');

    return xhr;
  },

  // get personalities
  getPersonalities: function() {
    var xhr = service.personalitiesRef.once("value");

    return xhr;
  },

  // get all student surveys
  getSurveys: function() {
    var xhr = service.studentSurveysRef.once("value");

    return xhr;
  },

  // get all public surveys
  getPublicSurveys: function() {
    var xhr = service.publicSurveyRef.once("value");
    return xhr;
  },

  getCorporateSurveys: function() {
    var xhr = service.corporateSurveyRef.once("value");
    return xhr;
  },

  // get personality profiles
  // realistic
  getRealisticPersonalityProfile: function() {
    var xhr = service.personalitiesRef
      .orderByChild("slug")
      .equalTo("realistic")
      .once("value");

    return xhr;
  },

  // investigative
  getInvestigativePersonalityProfile: function() {
    var xhr = service.personalitiesRef
      .orderByChild("slug")
      .equalTo("investigative")
      .once("value");

    return xhr;
  },

  // artistic
  getArtisticPersonalityProfile: function() {
    var xhr = service.personalitiesRef
      .orderByChild("slug")
      .equalTo("artistic")
      .once("value");

    return xhr;
  },

  // social
  getSocialPersonalityProfile: function() {
    var xhr = service.personalitiesRef
      .orderByChild("slug")
      .equalTo("social")
      .once("value");

    return xhr;
  },

  // enterprising
  getEnterprisingPersonalityProfile: function() {
    var xhr = service.personalitiesRef
      .orderByChild("slug")
      .equalTo("enterprising")
      .once("value");

    return xhr;
  },

  // conventional
  getConventionalPersonalityProfile: function() {
    var xhr = service.personalitiesRef
      .orderByChild("slug")
      .equalTo("conventional")
      .once("value");

    return xhr;
  },

  // all personalities
  getAllPersonalityProfiles: function() {
    var xhr = service.personalitiesRef.once("value");

    return xhr;
  },

  // get jhs questions
  getAllJHSQuestions: function() {
    var xhr = service.raisecQuestionsRef
      .orderByChild("education_level")
      .equalTo("jhs")
      .once("value");

    return xhr;
  },

  // test on
  getJHSQuestions: function() {
    service.raisecQuestionsRef
      .orderByChild("education_level")
      .equalTo("jhs")
      .on("value", service.valueChange);
  },

  // get shs questions
  getAllSHSQuestions: function() {
    var xhr = service.raisecQuestionsRef
      .orderByChild("education_level")
      .equalTo("shs")
      .once("value");

    return xhr;
  },

  // get uni questions
  getAllUNIQuestions: function() {
    var xhr = service.raisecQuestionsRef
      .orderByChild("education_level")
      .equalTo("university")
      .once("value");

    return xhr;
  },

  // get realistic questions
  getAllRealisticMarkers: function() {
    var xhr = service.raisecQuestionsRef.orderByChild("personality_type").equalTo("realistic").once('value');

    return xhr;
  },

  // get investigative markers
  getAllinvestigativeMarkers: function() {
    var xhr = service.raisecQuestionsRef.orderByChild("personality_type").equalTo("investigative").once('value');

    return xhr;
  },

  // get artistic markers
  getAllArtisticMarkers: function() {
    var xhr = service.raisecQuestionsRef.orderByChild("personality_type").equalTo("artistic").once("value");
    return xhr;
  },

  // get social markers
  getAllSocialMarkers: function() {
    var xhr = service.raisecQuestionsRef.orderByChild("personality_type").equalTo("social").once("value");
    return xhr;
  },

  // get enterprising markers
  getAllEnterprisingMarkers: function() {
    var xhr = service.raisecQuestionsRef.orderByChild("personality_type").equalTo("enterprising").once("value");
    return xhr;
  },

  // get conventional markers
  getAllConventionalMarkers: function() {
    var xhr = service.raisecQuestionsRef.orderByChild("personality_type").equalTo("conventional").once("value");
    return xhr;
  },

  // get all career paths
  getCareerPaths: function() {
    var xhr = service.careerPathRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all career clusters
  getCareerClusters: function() {
    var xhr = service.careerClusterRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all industries
  getCareerIndustries: function() {
    var xhr = service.careerIndustryRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all career pathways
  getCareerPathways: function() {
    var xhr = service.careerPathwayRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all work contexts
  getWorkContexts: function() {
    var xhr = service.workContextRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all work values
  getWorkValues: function() {
    var xhr = service.workValuesRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all work styles
  getWorkStyles: function() {
    var xhr = service.workStylesRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all career knowledge data
  getKnowledge: function() {
    var xhr = service.careerKnowledgeRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all career skills
  getSkills: function() {
    var xhr = service.careerSkillsRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all career technologies
  getTechnologies: function() {
    var xhr = service.careerTechRef.orderByChild("name").once("value");
    return xhr;
  },

  // get all careerProfiles
  getCareerProfiles: function() {
    let xhr = service.careerProfilesRef.orderByChild("createdOn").once("value");
    return xhr;
  },

  // get realistic profiles
  getRealisticCareers: function() {
    var xhr = service.careerProfilesRef.orderByChild('personality').equalTo('realistic').once('value');

    return xhr;
  },

  // get investigative profiles
  getInvestigativeCareers: function() {
    var xhr = service.careerProfilesRef.orderByChild('personality').equalTo('investigative').once('value');
    return xhr;
  },

  // get artistic careers
  getArtisticCareers: function() {
    var xhr = service.careerProfilesRef.orderByChild('personality').equalTo('artistic').once('value');
    return xhr;
  },

  // get social careers
  getSocialCareers: function() {
    var xhr = service.careerProfilesRef.orderByChild('personality').equalTo('social').once('value');
    return xhr;
  },

  // get enterprising careers
  getEnterprisingCareers: function() {
    var xhr = service.careerProfilesRef.orderByChild('personality').equalTo('enterprising').once('value');
    return xhr;
  },

  // get conventional careers
  getConventionalCareers: function() {
    var xhr = service.careerProfilesRef.orderByChild('personality').equalTo('conventional').once('value');
    return xhr;
  },

  // get webinars by date
  getWebinarsByDateCreated: function() {
    let xhr = service.webinarsRef.orderByChild("createdOn").once("value");
    return xhr;
  },

  getWebinarsByAirDate: function() {
    let xhr = service.webinarsRef.orderByChild("").once("value");
    return xhr;
  }
};
// console.log(window.location.port);

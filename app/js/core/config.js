// main appConfig
var unryddleApp = {
  urConfig: {
    apiKey: "AIzaSyCLygvKPs75SM5DVkcGfob7DML5kBy-kKI",
    authDomain: "unryddle-dev.firebaseapp.com",
    databaseURL: "https://unryddle-dev.firebaseio.com",
    projectId: "unryddle-dev",
    storageBucket: "unryddle-dev.appspot.com",
    messagingSenderId: "488356088844"
  },

  userAuthData: 'USER_AUTH',
  userData: 'USER_DATA',
  adminEmails: ['developer@unryddle.com', 'kweku@unryddle.com', 'kojo@unryddle.com'],
  userRoles: ['admin', 'editor', 'curator', 'counselor', 'expert', 'facilitator', 'finance'],
  personalityTypes: ['realistic', 'investigative', 'artistic', 'social', 'enterprising', 'conventional'],
  educationLevels: ['jhs', 'shs', 'university'],

  modalOptions: {
    backdrop: true,
    keyboard: false,
    focus: true,
    show: true
  },

  quillOptions: {
    modules: {
      toolbar: [
        ['bold', 'italic', 'underline'],
        ['link', 'blockquote'],
        [{ list: 'ordered' }, { list: 'bullet' }]
      ]
    },
    theme: 'snow',
    placeholder: 'Compose relevant info here...'
  },

  storeData: function (key, data) {
    store(key, data);
  },

  loadData: function (key) {
    return store.get(key);
  },

  removeData: function (key) {
    store(key, '');
  },

  clearData: function () {
    store(false);
  },

  // method to remove trailing spaces from slugs and names
  removeSpace: function(str) {
    return str.replace(/\s+$/, '');
  },

  // convert firebase epoch date to "Do MMM, YYYY"
  convertEpoch: function(timestamp){
    return moment(timestamp).format('Do MMM, YYYY');
  },

  showErrorMsg: function(err, handler) {
    console.log(err);
    if(err === undefined){
      var msg = "We have encountered a problem. Please wait a while and try again.";
      handler.find('p').text(msg);
      handler.removeClass('d-none');
      // setTimeout(function() {
      //   handler.addClass('d-none')
      // }, 4000);
    } else {
      var msg = err.msg;
      handler.find('p').text(msg);
      handler.removeClass('d-none');
      // setTimeout(function(){
      //   handler.addClass('d-none');
      // }, 4000);
    }
  },

  spliceArrDataComma: function(obj){
    var arr = obj.split(',');
    return arr;
  },

  spliceArrDataCommaSpace: function(obj){
    var arr = obj.split(', ');
    return arr;
  },

  spliceArrDataNewline: function(obj){
    var arr = obj.split('\n');
    return arr;
  },

  spliceNames: function(obj){
    var arr = [];
    for(var item in obj){
        if(obj.hasOwnProperty(item)){
            arr.push(obj[item].name);
        }
    }
    return arr;
  },

  getCheckVals: function(obj) {
    let personalities = [];
    obj.each(function() {
      let $this = $(this);
      if ($this.prop("checked")) {
        personalities.push($(this).val());
      }
    });

    return personalities;
  },

  displayArrData: function (arr) {
    var results = '';
    for(var i = 0; i < arr.length; i++) {
      results = results.concat(arr[i] + '\n');
    }
    return results;
  },

  // get file from input
  getFileInput: function(event) {
    return event.target.files[0];
  },

  getVideoFiles: function(event) {
    var videoFiles = [];
    for( var i = 0; i < event.target.files.length; i++){
      videoFiles.push(event.target.files[i]);
    }
    return videoFiles;
  },

  // convert date to epoch
  setEpochDate: function(date) {
    return moment(date, 'Do MMM YYYY HH:mm:ss').valueOf();
  },

  // convert epoch to regular date with time
  setRegularDateWithTime: function(date) {
    return moment(date).format('Do MMM YYYY HH:mm:ss');
  },

  // convert epoch to regular date
  setRegularDate: function(date) {
    return moment(date).format('Do MMMM, YYYY');
  },

  // set file metadata
  setFileMeta: function(arr) {
    var metaData = {
      contentType: arr.type
    }
    return metaData;
  },

  getAllUrlParams: function() {

    // get query string from url (optional) or window
    var queryString = window.location.search.slice(1);
      
    // we'll store the parameters here
    var obj = {};
      
    // if query string exists
    if (queryString) {
      
        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];
      
        // split our query string into its component parts
        var arr = queryString.split('&');
      
        for (var i=0; i<arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');
      
            // in case params look like: list[]=thing1&list[]=thing2
            var paramNum = undefined;
            var paramName = a[0].replace(/\[\d*\]/, function(v) {
              paramNum = v.slice(1,-1);
              return '';
            });
      
            // set parameter value (use 'true' if empty)
            var paramValue = typeof(a[1])==='undefined' ? true : a[1];
      
            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            paramValue = paramValue.toLowerCase();
      
            // if parameter name already exists
            if (obj[paramName]) {
              // convert value to array (if still string)
              if (typeof obj[paramName] === 'string') {
                obj[paramName] = [obj[paramName]];
              }
              // if no array index number specified...
              if (typeof paramNum === 'undefined') {
                // put the value on the end of the array
                obj[paramName].push(paramValue);
              }
              // if array index number specified...
              else {
                // put the value at that index number
                obj[paramName][paramNum] = paramValue;
              }
            }
            // if param name doesn't exist yet, set it
            else {
              obj[paramName] = paramValue;
            }
        }
    }
      
    return obj;
  },

}

firebase.initializeApp(unryddleApp.urConfig);

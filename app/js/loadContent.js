function getCareerPathData() {
    service
      .getCareerPaths()
      .then(function(snapshot) {
        var data = snapshot.val();
        unryddleApp.storeData("careerPaths", data);
      })
      .catch(function(error) {
        console.log(error);
      });
}

function getCareerClusterData(){
    service
      .getCareerClusters()
      .then(function(snapshot) {
        var data = snapshot.val();
        unryddleApp.storeData("careerClusters", data);
        
      })
      .catch(function(error) {
        console.log(error);
      });
}

function getCareerPathWaysData(){
    service
      .getCareerPathways()
      .then(function(snapshot) {
        var data = snapshot.val();
        unryddleApp.storeData("careerPathways", data);
      })
      .catch(function(error) {
        console.log(error);
      });
}

function getCareerIndustriesData() {
    service
      .getCareerIndustries()
      .then(function(snapshot) {
        var data = snapshot.val();
        unryddleApp.storeData("careerIndustries", data);
      })
      .catch(function(error) {
        console.log(error);
      });
}

function getCareerWorkContextsData(){
    service
      .getWorkContexts()
      .then(function(snapshot) {
        var data = snapshot.val();
        unryddleApp.storeData("careerWorkContexts", data);
      })
      .catch(function(error) {
        console.log(error);
      });
}

function getCareerWorkStylesData() {
    service
      .getWorkStyles()
      .then(function(snapshot) {
        var data = snapshot.val();
        unryddleApp.storeData("careerWorkStyles", data);
      })
      .catch(function(error) {
        console.log(error);
      });
}

function getCareerWorkValuesData() {
    service
      .getWorkValues()
      .then(function(snapshot) {
        var data = snapshot.val();
        unryddleApp.storeData("careerWorkValues", data);
      })
      .catch(function(error) {
        console.log(error);
      });
}

function getCareerKnowledgeData() {
    service
    .getKnowledge()
    .then(function(snapshot) {
      var data = snapshot.val();
      unryddleApp.storeData("careerKnowledge", data);
    })
    .catch(function(error) {
      console.log(error);
    }); 
}

function getCareerSkillData() {
    service
      .getSkills()
      .then(function(snapshot) {
        var data = snapshot.val();
        unryddleApp.storeData("careerSkills", data);
      })
      .catch(function(error) {
        console.log(error);
      });
}

function getCareerTechnologiesData() {
    service
      .getTechnologies()
      .then(function(snapshot) {
        var data = snapshot.val();
        unryddleApp.storeData("careerTechnologies", data);
      })
      .catch(function(error) {
        console.log(error);
      });
}

function getAllCareers() {
  service.getCareerProfiles().then(function(snapshot) {
    var data = snapshot.val();
    unryddleApp.storeData('allCareerProfiles', data);
  }).catch(function(error){
    console.log(error);
  });
}

getCareerPathData();
getCareerClusterData();
getCareerPathWaysData();
getCareerIndustriesData();
getCareerWorkContextsData();
getCareerWorkStylesData();
getCareerWorkValuesData();
getCareerKnowledgeData();
getCareerSkillData();
getCareerTechnologiesData();
// getAllCareers();
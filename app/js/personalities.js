(function ($) {
  'use strict';

  let personalityPageData = {
    realisticData: null,
    investigativeData: null,
    artisticData: null,
    socialData: null,
    enterprisingData: null,
    conventionalData: null,
    successString: "Your form has been successfully submitted. Thank you!",
  }

  const $loadingbar = $('#loadprogress');
  const $realisticListGroup = $('#realisticGroup');
  const $investigativeListGroup = $('#investigativeGroup');
  const $artisticListGroup = $('#artisticGroup');
  const $socialListGroup = $('#socialGroup');
  const $enterprisingListGroup = $('#enterprisingGroup');
  const $conventionalListGroup = $('#conventionalGroup');
  const $editPersonalityForm = $('#editPersonality');
  const $editPersonalityName = $('#edit_personalityName');
  const $editPersonalityAlias = $('#edit_personalityAlias');
  const $editPersonalityDesc = $('#edit_personalityDesc');
  const $editPersonalityMarker = $('#edit_personalityMarker');
  const $editPersonalityImgURL = $('#edit_personalityImageURL');
  const $editPersonalityVidURL = $('#edit_personalityVideoURL');
  const $editPersonalityBtn = $('#update_personality');
  const $updateError = $('#edit_personality-error');
  const $updateSuccess = $('#edit_personality-success');
  const $editPersonalityModal = $('#editPersonalityModalCenter');



  function renderSuccessMsg(str, handler) {
    handler.text(str);
    handler.removeClass('d-none');
    $('input, textarea').val('');
  }

  function renderErrorMsg(error, handler) {
    let str;
    if (error.code == "auth/user-not-found") {
      str = "User does not exist. Please ensure you have entered a valid email and password."
    } else {
      str = "There was a problem uploading your data. Please try again in a few minutes"
    }
    handler.text(str);
    handler.removeClass('d-none');
    $('input, textarea').val('');
  }

  function emptyProfileData() {
    let html = "<h4 class='text-center mt-5 empty-header'>There is no data for this personality. Add a profile for this personality using the link above.</h4>";

    return html;
  }

  function personalityTemplate(id, item) {
    let imageURL, videoURL;

    if(item.imageURL == undefined) {
      imageURL = "Not available";
    } else {
      imageURL = item.imageURL
    }

    if(item.videoURL == undefined) {
      videoURL = "Not Available";
    } else {
      videoURL = item.videoURL;
    }
    let html = `<div class='list-group-item list-group-item-action flex-column align-items-start'>
    <div class='row w-100 personality-list-margin'>
    <div class='col-3 text-right item-label'>Name:</div>
    <div class='col-7 user_name'>${unryddleApp.removeSpace(item.name)}</div>
    <div class='col-2'>
    <a href='#' id='edit-personality' class='user-action' data-id=${id}>
    <i class='fas fa-edit'></i>
    </a>
    </div>
    </div>
    <div class='row w-100 personality-list-margin'>
    <div class='col-3 text-right item-label'>Slug:</div>
    <div class='col-9 user_slug'>${unryddleApp.removeSpace(item.slug)}</div>
    </div>
    <div class='row w-100 personality-list-margin'>
    <div class='col-3 text-right item-label'>Marker Code:</div>
    <div class='col-9 user_role'>${item.marker}</div>
    </div>
    <div class='row w-100 personality-list-margin'>
    <div class='col-3 text-right item-label'>Alias:</div>
    <div class='col-9 user_role'>${item.alias}</div>
    </div>
    <div class='row w-100 personality-list-margin'>
    <div class='col-3 text-right item-label'>Description:</div>
    <div class='col-9 user_role'>${item.description}</div>
    </div>
    <div class='row w-100 personality-list-margin'>
    <div class='col-3 text-right item-label'>Image Url:</div>
    <div class='col-9 user_role'>${imageURL}</div>
    </div>
    <div class='row w-100 personality-list-margin'>
    <div class='col-3 text-right item-label'>Video Link:</div>
    <div class='col-9'>${videoURL}</div>
    </div>
    </div>`;

    return html;
  }

  // process profile data
  function processProfileData(slug, data) {

    // process if realistic profile
    if (slug == "realistic") {
      if (data == null) {
        $realisticListGroup.append($(emptyProfileData()));
        $loadingbar.addClass('d-none');
      } else {
        for (let profile in data) {
          if (data.hasOwnProperty(profile)) {
            let singleProfile = data[profile];
            let profileID = profile;
            $realisticListGroup.append($(personalityTemplate(profileID, singleProfile)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if investigative profile
    if (slug == "investigative") {
      if (data == null) {
        $investigativeListGroup.append($(emptyProfileData()));
        $loadingbar.addClass('d-none');
      } else {
        for (let profile in data) {
          if (data.hasOwnProperty(profile)) {
            let singleProfile = data[profile];
            let profileID = profile;
            $investigativeListGroup.append($(personalityTemplate(profileID, singleProfile)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if artistic profile
    if (slug == "artistic") {
      if (data == null) {
        $artisticListGroup.append($(emptyProfileData()));
        $loadingbar.addClass('d-none');
      } else {
        for (let profile in data) {
          if (data.hasOwnProperty(profile)) {
            let singleProfile = data[profile];
            let profileID = profile;
            $artisticListGroup.append($(personalityTemplate(profileID, singleProfile)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if social profile
    if (slug == "social") {
      if (data == null) {
        $socialListGroup.append($(emptyProfileData()));
        $loadingbar.addClass('d-none');
      } else {
        for (let profile in data) {
          if (data.hasOwnProperty(profile)) {
            let singleProfile = data[profile];
            let profileID = profile;
            $socialListGroup.append($(personalityTemplate(profileID, singleProfile)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if enterprising profile
    if (slug == "enterprising") {
      if (data == null) {
        $enterprisingListGroup.append($(emptyProfileData()));
        $loadingbar.addClass('d-none');
      } else {
        for (let profile in data) {
          if (data.hasOwnProperty(profile)) {
            let singleProfile = data[profile];
            let profileID = profile;
            $enterprisingListGroup.append($(personalityTemplate(profileID, singleProfile)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }

    // process if conventional profile
    if (slug == "conventional") {
      if (data == null) {
        $conventionalListGroup.append($(emptyProfileData()));
        $loadingbar.addClass('d-none');
      } else {
        for (let profile in data) {
          if (data.hasOwnProperty(profile)) {
            let singleProfile = data[profile];
            let profileID = profile;
            $conventionalListGroup.append($(personalityTemplate(profileID, singleProfile)));
            $loadingbar.addClass('d-none');
          }
        }
      }
    }
  }

  // get individual personality profiles
  // realistic
  function getRealistic() {
    let slug = unryddleApp.personalityTypes[0];
    $loadingbar.removeClass('d-none');
    $realisticListGroup.html('')
    service.getRealisticPersonalityProfile().then(function (snapshot) {
      let data = snapshot.val();
      console.log(data);
      processProfileData(slug, data);
    }).catch(function (error) {
      $loadingbar.removeClass('d-none');
      console.log(error);
    })
  }

  // investigative
  function getInvestigative() {
    let slug = unryddleApp.personalityTypes[1];
    $loadingbar.removeClass('d-none');
    $investigativeListGroup.html('');
    service.getInvestigativePersonalityProfile().then(function (snapshot) {
      let data = snapshot.val();
      processProfileData(slug, data);
    }).catch(function (error) {
      $loadingbar.removeClass('d-none');
      console.log(error);
    })
  }

  // artistic
  function getArtistic() {
    let slug = unryddleApp.personalityTypes[2];
    $loadingbar.removeClass('d-none');
    $artisticListGroup.html('');
    service.getArtisticPersonalityProfile().then(function (snapshot) {
      let data = snapshot.val();
      processProfileData(slug, data);
    }).catch(function (error) {
      $loadingbar.removeClass('d-none');
      console.log(error);
    })
  }

  // social
  function getSocial() {
    let slug = unryddleApp.personalityTypes[3];
    $loadingbar.removeClass('d-none');
    $socialListGroup.html('');
    service.getSocialPersonalityProfile().then(function (snapshot) {
      let data = snapshot.val();
      processProfileData(slug, data);
    }).catch(function (error) {
      $loadingbar.removeClass('d-none');
      console.log(error);
    })
  }

  // enterprising
  function getEnterprising() {
    let slug = unryddleApp.personalityTypes[4];
    $loadingbar.removeClass('d-none');
    $enterprisingListGroup.html('');
    service.getEnterprisingPersonalityProfile().then(function (snapshot) {
      let data = snapshot.val();
      processProfileData(slug, data);
    }).catch(function (error) {
      $loadingbar.removeClass('d-none');
      console.log(error);
    })
  }

  // conventional
  function getConventional() {
    let slug = unryddleApp.personalityTypes[5];
    $loadingbar.removeClass('d-none');
    $conventionalListGroup.html('');
    service.getConventionalPersonalityProfile().then(function (snapshot) {
      let data = snapshot.val();
      processProfileData(slug, data);
    }).catch(function (error) {
      $loadingbar.removeClass('d-none');
      console.log(error);
    })
  }

  // fill edit form for personality
  function populatePersonalityEditForm(id, data) {
    let name, alias, desc, marker, imgURL, vidURL;

    if(data.name == undefined) {
      $editPersonalityName.val('').attr('data-id', id);
    } else {
      $editPersonalityName.val(data.name).attr('data-id', id)
    }

    if(data.alias == undefined) {
      $editPersonalityAlias.val('');
    } else {
      $editPersonalityAlias.val(data.alias);
    }

    if(data.description == undefined) {
      $editPersonalityDesc.val('');
    } else {
      $editPersonalityDesc.val(data.description);
    }

    if(data.marker == undefined) {
      $editPersonalityMarker.val('');
    } else {
      $editPersonalityMarker.val(data.marker);
    }

    if(data.imageURL == undefined) {
      $editPersonalityImgURL.val('');
    } else {
      $editPersonalityImgURL.val(data.imageURL);
    }

    if(data.videoURL == undefined) {
      $editPersonalityVidURL.val('');
    } else {
      $editPersonalityVidURL.val(data.videoURL);
    }

    $editPersonalityBtn.attr('data-id', id);
  }

  // edit personality function
  function processPersonalityAction(event) {
    let target = event.target;
    // console.log(target.tagName);
    if (target.tagName === 'svg' || target.tagName === 'path') {
      let targetID = $(target).closest('a').data('id');
      // console.log(targetID);
      service.personalitiesRef.child(targetID).once('value', function(snapshot) {
        let profile_data = snapshot.val();
        $editPersonalityForm.find('input, select').val('');
        populatePersonalityEditForm(targetID, profile_data);
      });
      $editPersonalityModal.modal(unryddleApp.modalOptions);
    }
  }


  $(document).ready(function () {
    getRealistic();

    // personality tab click event
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      let tabTarget = $(e.target).attr('id');

      // realistic profile
      if (tabTarget == "r-tab") {
        getRealistic();
      }

      // investigative profile
      if (tabTarget == "i-tab") {
        getInvestigative();
      }

      // artistic profile
      if (tabTarget == "a-tab") {
        getArtistic();
      }

      // social profile
      if (tabTarget == "s-tab") {
        getSocial();
      }

      // enterprising profile
      if (tabTarget == "e-tab") {
        getEnterprising();
      }

      // conventional profile
      if (tabTarget == "c-tab") {
        getConventional();
      }
    });

    // edit personality profile action
    $editPersonalityForm.on('submit', function(e) {
      e.preventDefault();

      $editPersonalityBtn.find(' span.fa.fa-spinner').removeClass('d-none');

      let personalityRef = service.personalitiesRef.child($editPersonalityBtn.attr('data-id'));
      let payload = {
        name: unryddleApp.removeSpace($editPersonalityName.val()),
        alias: unryddleApp.removeSpace($editPersonalityAlias.val()),
        description: unryddleApp.removeSpace($editPersonalityDesc.val()),
        slug: unryddleApp.removeSpace($editPersonalityName.val().toLowerCase()),
        marker: unryddleApp.removeSpace($editPersonalityMarker.val()),
        updatedOn: firebase.database.ServerValue.TIMESTAMP
      };

      personalityRef.update(payload, function(error) {
        if (error) {
          console.log(error);
          $updateError.text('There was a problem updating. Please check your internet connection and try again.');
          $updateError.removeClass('d-none');
          $editPersonalityBtn.find('span').addClass('d-none');
        } else {
          $updateSuccess.text('Personality profile has been successfully updated');
          $updateSuccess.removeClass('d-none');
          $editPersonalityBtn.find('span').addClass('d-none');
        }
      })
    })

    // personality action click event(edit/delete)
    $(document).on('click', '.dashWrapper .container-fluid .row .col-12 .card .card-body.height-55 .container-fluid .row .col .tab-content .tab-pane .list-group .list-group-item .row .col-2 a svg', processPersonalityAction);

    // logout function
    $('#sign-out').on('click', function(e) {
      e.preventDefault();
      service.signOut();
    });
  });
}(jQuery))

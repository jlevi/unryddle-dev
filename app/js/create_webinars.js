(function($) {
    'use strict';

    var $insightRow = $('#insightRow');
    var $insightHeading = $('#insight-heading');
    var $interviewRow = $('#interviewRow');
    var $interviewHeading = $('#interview-heading');
    var $webinarCategoryError = $('#webinar-category-error');
    var $webinarForm = $('#newWebinar');
    var $insightProgressModal = $('#webinarInsightProgressModal');
    var $interviewProgressModal = $('#webinarInterviewProgressModal');
    var $submitInsightBtn = $('#submit_insight_data');
    var $submitInterviewBtn = $('#submit_interview_data');
    let SELECTED_CATEGORY = '';
    var insightImageFile = null;
    var insightVideoFiles = null;
    var insightVideoThumbnail = null;
    var insightVideoTranscript = null;
    var insightURLS = {};
    var interviewImageFile = null;
    var interviewVideoFiles = null;
    var interviewVideoThumbnail = null;
    var interviewVideoTranscript = null;
    var interviewHostImageFile = null;
    var interviewURLS = {};
    

    var webinarOverviewEditor = new Quill('#webinar_overview', unryddleApp.quillOptions);
    var webinarObjectivesEditor = new Quill('#webinar_objectives', unryddleApp.quillOptions);
    var insightDescriptionEditor = new Quill('#insight_description', unryddleApp.quillOptions);
    var insightObjectivesEditor = new Quill('#insight_objectives', unryddleApp.quillOptions);
    var insightSpeakerProfile = new Quill('#insight_speaker_profile', unryddleApp.quillOptions);
    var interviewDescriptionEditor = new Quill('#interview_description',unryddleApp.quillOptions);
    var interviewObjectivesEditor = new Quill('#interview_objectives', unryddleApp.quillOptions);
    var interviewSpeakerProfile = new Quill('#interview_speaker_profile',unryddleApp.quillOptions);
    var interviewHostProfile = new Quill('#interview_host_profile', unryddleApp.quillOptions);

    // set datepicker
    var datePicker = new Pikaday({
        field: $('.datepicker')[0],
        incrementMinuteBy: 1,
        showTime: true,
        showMinutes: true,
        use24hour: false,
        format: 'Do MMM YYYY HH:mm:ss', // other formats: 'Do MMMM YYYY', 'D MMM YYYY', 'YYYY-MM-DD HH:mm:ss'
        autoClose: false, // set to false when debuggging or editin pikaday style
        minDate: new Date()
    });

    var datePicker2 = new Pikaday({
        field: $('.datepicker')[1],
        incrementMinuteBy: 1,
        showTime: true,
        showMinutes: true,
        use24hour: true,
        format: 'Do MMM YYYY HH:mm:ss',
        autoClose: false,
        minDate: new Date()
    });

    // reset text editors for insight
    function resetInsightEditors(){
        webinarOverviewEditor.setContents([{ insert: '\n' }]);
        webinarObjectivesEditor.setContents([{ insert: '\n' }]);
        insightDescriptionEditor.setContents([{ insert: '\n' }]);
        insightObjectivesEditor.setContents([{ insert: '\n' }]);
        insightSpeakerProfile.setContents([{ insert: '\n' }]);
    }

    // reset text editors for interview
    function resetInterviewEditors(){
        webinarOverviewEditor.setContents([{ insert: '\n' }]);
        webinarObjectivesEditor.setContents([{ insert: '\n' }]);
        interviewDescriptionEditor.setContents([{ insert: '\n' }]);
        interviewObjectivesEditor.setContents([{ insert: '\n' }]);
        interviewSpeakerProfile.setContents([{ insert: '\n' }]);
        interviewHostProfile.setContents([{ insert: '\n' }]);
    }
    // get all careers
    function getAllCareers(callback) {
        // var careerArr = [];
        service.careerProfilesRef.orderByChild("createdOn").once("value", function(snapshot){
            var careerList = snapshot.val();
            // for (var cp in careerList){
            //     careerArr.push(careerList[cp]);
            // }
            callback(careerList);
        });
    }

    // reset form inputs
    function resetFormInputs(){
        $('.tokenfield-typeahead-industry').tokenfield('destroy');
        $('.tokenfield-typeahead-careers').tokenfield('destroy');
        document.getElementById('newWebinar').reset();
        // define auto suggest for industry field
        var wIndustry = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: unryddleApp.spliceNames(unryddleApp.loadData("careerIndustries"))
        });
        wIndustry.initialize();

        $('.tokenfield-typeahead-industry').tokenfield({
            typeahead:[
                null,
                {
                    source: wIndustry.ttAdapter()
                }
            ],
            showAutocompleteOnFocus: true
        });
        getAllCareers(function(careerList){
            if(careerList){
                var wCareers = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: unryddleApp.spliceNames(careerList)
                });
                wCareers.initialize();

                $('.tokenfield-typeahead-careers').tokenfield({
                    typeahead:[
                        null,
                        {
                            source: wCareers.ttAdapter()
                        }
                    ],
                    showAutocompleteOnFocus: true
                });
            }
        });
    }

    function uploadImageFiles(fileType, uploadMsg, uploadBar, image, callback) {
        fileType.text('');
        uploadBar.css('width', "0%").attr("aria-valuenow", '0').text("");
        var uploadImageTask = service.imagesStorageRef.child(image.name).put(image);
        // register state observer, error observer and
        // completion observer
        uploadImageTask.on('state_changed', function(snapshot){
            fileType.text(uploadMsg);
            var progress = ((snapshot.bytesTransferred / snapshot.totalBytes) * 100).toFixed(0);
            uploadBar.css('width', progress + "%").attr("aria-valuenow", progress).text(progress + "% complete");
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                  console.log('Upload is paused');
                  break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                  console.log('Upload is running');
                  break;
            }
        }, function(error) {
            console.log(error);
        }, function() {
            uploadImageTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                callback(downloadURL);
            });
        });
    }
    function uploadVideoFiles(fileType, uploadMsg, uploadBar, video, callback) {
        fileType.text('');
        uploadBar.css('width', '0%').attr('aria-valuenow', '0').text('');
        var uploadVideoTask = service.videoStorageRef.child(video.name).put(video);
        uploadVideoTask.on('state_changed', function(snapshot) {
            fileType.text(uploadMsg);
            var progress = ((snapshot.bytesTransferred / snapshot.totalBytes) * 100).toFixed(0);
            uploadBar.css('width', progress + "%").attr("aria-valuenow", progress).text(progress + "% complete");
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                  console.log('Upload is paused');
                  break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                  console.log('Upload is running');
                  break;
            }
        }, function(error){
            console.log(error);
        }, function() {
            uploadVideoTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                callback(downloadURL);
            });
        });
    }
    function uploadTranscripts(fileType, uploadMsg, uploadBar, transcript, callback){
        fileType.text('');
        uploadBar.css('width', '0%').attr('aria-valuenow', '0').text('');
        var uploadTranscriptTask = service.filesStorageRef.child(transcript.name).put(transcript);
        uploadTranscriptTask.on('state_changed', function(snapshot) {
            fileType.text(uploadMsg);
            var progress = ((snapshot.bytesTransferred / snapshot.totalBytes) * 100).toFixed(0);
            uploadBar.css('width', progress + "%").attr("aria-valuenow", progress).text(progress + "% complete");
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                  console.log('Upload is paused');
                  break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                  console.log('Upload is running');
                  break;
            }
        }, function(error){
            console.log(error);
        }, function() {
            uploadTranscriptTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                callback(downloadURL);
            });
        });
    }

    // form validate functions
    $webinarForm.validate({
        rules: {
            'webinar_name':{
                required: true
            },
            'webinar_category': {
                required: true
            },
            'insight-title': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-date': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-duration': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-category': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-date': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-duration': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-category': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-industries': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-careers': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-video-file': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight_speaker_name': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight_speaker_image': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight_speaker_position': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight_speaker_company': {
                required: function(element) {
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight_speaker_facebook': {
                url: true
            },
            'insight_speaker_linkedin': {
                url: true
            },
            'insight_speaker_instagram': {
                url: true
            },
            'interview-title': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none')
                }
            },
            'interview-date': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none')
                }
            },
            'interview-duration': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none')
                }
            },
            'interview-category': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none')
                }
            },
            'interview-industries': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none')
                }
            },
            'interview-careers': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none')
                }
            },
            'interview-video-file': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none')
                }
            },
            'interview_speaker_name': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_speaker_image': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_speaker_position': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_speaker_company': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_speaker_position': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_speaker_company': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_speaker_facebook': {
                url: true
            },
            'interview_speaker_linkedin': {
                url: true
            },
            'interview_speaker_instagram': {
                url: true
            },
            'interview_host_name': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_host_image': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_host_position': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_host_company': {
                required: function(element) {
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview_host_facebook': {
                url: true
            },
            'interview_host_linkedin': {
                url: true
            },
            'interview_host_instagram': {
                url: true
            },
            'insight-video-files':{
                required: function(element){
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-video-thumbnail':{
                required: function(element){
                    return !$insightRow.hasClass('d-none');
                }
            },
            'insight-video-transcript':{
                required: function(element){
                    return !$insightRow.hasClass('d-none');
                }
            },
            'interview-video-files':{
                required: function(element){
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview-video-thumbnail':{
                required: function(element){
                    return !$interviewRow.hasClass('d-none');
                }
            },
            'interview-video-transcript':{
                required: function(element){
                    return !$interviewRow.hasClass('d-none');
                }
            },
        }
    });

    $('#webinar_category').on('change', function(e) {
        var cat_value = this.value;
        SELECTED_CATEGORY = cat_value;
        if(cat_value === 'webinar_insight') {
            // check if alternate fields are active
            if(!$interviewHeading.hasClass('d-none') && !$interviewRow.hasClass('d-none')){
                if(!$webinarCategoryError.hasClass('d-none')){
                    $webinarCategoryError.addClass('d-none');
                }
                $interviewHeading.addClass('d-none');
                $interviewRow.addClass('d-none');

                $insightHeading.removeClass('d-none');
                $insightRow.removeClass('d-none');
            } else {
                $insightHeading.removeClass('d-none');
                $insightRow.removeClass('d-none');
                if(!$webinarCategoryError.hasClass('d-none')){
                    $webinarCategoryError.addClass('d-none');
                }
            }
        } else if(cat_value === 'webinar_interview') {
            if(!$insightHeading.hasClass('d-none') && !$insightRow.hasClass('d-none')){
                if(!$webinarCategoryError.hasClass('d-none')){
                    $webinarCategoryError.addClass('d-none');
                }
                $insightHeading.addClass('d-none');
                $insightRow.addClass('d-none');

                $interviewHeading.removeClass('d-none');
                $interviewRow.removeClass('d-none');
            } else {
                $interviewHeading.removeClass('d-none');
                $interviewRow.removeClass('d-none');
                if(!$webinarCategoryError.hasClass('d-none')){
                    $webinarCategoryError.addClass('d-none');
                }
            }
        } else {
            $webinarCategoryError.removeClass('d-none');
            if(!$interviewHeading.hasClass('d-none') && !$interviewRow.hasClass('d-none')){
                $interviewHeading.addClass('d-none');
                $interviewRow.addClass('d-none');
            }
            if(!$insightHeading.hasClass('d-none') && !$insightRow.hasClass('d-none')){
                $insightHeading.addClass('d-none');
                $insightRow.addClass('d-none');
            }
        }
    });


    // define auto suggest for industry field
    var wIndustry = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: unryddleApp.spliceNames(unryddleApp.loadData("careerIndustries"))
    });
    wIndustry.initialize();
    
    $('.tokenfield-typeahead-industry').tokenfield({
        typeahead:[
            null,
            {
                source: wIndustry.ttAdapter()
            }
        ],
        showAutocompleteOnFocus: true
    });
    getAllCareers(function(careerList){
        if(careerList){
            var wCareers = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: unryddleApp.spliceNames(careerList)
            });
            wCareers.initialize();
            
            $('.tokenfield-typeahead-careers').tokenfield({
                typeahead:[
                    null,
                    {
                        source: wCareers.ttAdapter()
                    }
                ],
                showAutocompleteOnFocus: true
            });
        }
    });

    // process form payload
    function processInsightPayload(category) {
        var payload = {};
        payload.createdOn = firebase.database.ServerValue.TIMESTAMP;
        payload.name = $('#webinar_name').val().trim();
        payload.overview = webinarOverviewEditor.root.innerHTML;
        payload.objectives = webinarObjectivesEditor.root.innerHTML;
        payload.category = category;
        payload.insightTitle = $('#insight-title').val().trim();
        payload.insightDuration = $('#insight-duration').val().trim();
        payload.insightCategory = $('#insight-category').val().trim();
        payload.insightIndustries = unryddleApp.spliceArrDataComma($('#insight-industries').val().trim());
        payload.insightCareers =  unryddleApp.spliceArrDataComma($('#insight-careers').val().trim());
        payload.learningObjectives = insightObjectivesEditor.root.innerHTML;
        payload.insightDescription = insightDescriptionEditor.root.innerHTML;
        payload.speaker = {
            name: $('#insight_speaker_name').val().trim(),
            position: $('#insight_speaker_position').val().trim(),
            company: $('#insight_speaker_company').val().trim(),
            profile: insightSpeakerProfile.root.innerHTML,
            facebookURL: $('#insight_speaker_facebook').val().trim(),
            linkedInURL: $('#insight_speaker_linkedin').val().trim(),
            instagramURL: $('#insight_speaker_instagram').val().trim()
        };
        payload.webinarDate = unryddleApp.setEpochDate($('#insight-date').val().trim());

        return payload;
    }

    function processInterviewPayload(category){
        var payload = {};
        payload.createdOn = firebase.database.ServerValue.TIMESTAMP;
        payload.name = $('#webinar_name').val().trim();
        payload.overview = webinarOverviewEditor.root.innerHTML;
        payload.objectives = webinarObjectivesEditor.root.innerHTML;
        payload.category = category;
        payload.interviewTitle = $('#interview-title').val().trim();
        payload.interviewDuration = $('#interview-duration').val().trim();
        payload.interviewCategory = $('#interview-category').val().trim();
        payload.interviewIndustries = unryddleApp.spliceArrDataComma($('#interview-industries').val().trim());
        payload.interviewCareers = unryddleApp.spliceArrDataComma($('#interview-careers').val().trim());
        payload.learningObjectives = interviewObjectivesEditor.root.innerHTML;
        payload.interviewDescription = interviewDescriptionEditor.root.innerHTML;
        payload.speaker = {
            name: $('#interview_speaker_name').val().trim(),
            position: $('#interview_speaker_position').val().trim(),
            profile: interviewSpeakerProfile.root.innerHTML,
            company: $('#interview_speaker_company').val().trim(),
            facebookURL: $('#interview_speaker_facebook').val().trim(),
            linkedInURL: $('#interview_speaker_linkedin').val().trim(),
            instagramURL: $('#interview_speaker_instagram').val().trim()
        };
        payload.host = {
            name: $('#interview_host_name').val().trim(),
            position: $('#interview_host_position').val().trim(),
            company: $('#interview_host_company').val().trim(),
            profile: interviewHostProfile.root.innerHTML,
            facebookURL: $('#interview_host_facebook').val().trim(),
            linkedInURL: $('#interview_host_linkedin').val().trim(),
            instagramURL: $('#interview_host_instagram').val().trim()
        }
        payload.webinarDate = unryddleApp.setEpochDate($('#interview-date').val().trim());
        
        return payload;

    }

    // load insightImageFile
    $('#insight-video-files').on('change', function(event) {
        insightVideoFiles = unryddleApp.getVideoFiles(event);
    });
    $('#insight_speaker_image').on('change', function(event) {
        insightImageFile = unryddleApp.getFileInput(event);
    });
    $('#insight-video-thumbnail').on('change', function(event){
        insightVideoThumbnail = unryddleApp.getFileInput(event);
    });
    $('#insight-video-transcript').on('change', function(event){
        insightVideoTranscript = unryddleApp.getFileInput(event);
    });

    $('#interview-video-files').on('change', function(event) {
        interviewVideoFiles = unryddleApp.getVideoFiles(event);
        // console.log(interviewVideoFiles)
    });
    $('#interview_speaker_image').on('change', function(event) {
        interviewImageFile = unryddleApp.getFileInput(event);
    });
    $('#interview-video-thumbnail').on('change', function(event) {
        interviewVideoThumbnail = unryddleApp.getFileInput(event);
    });
    $('#interview-video-transcript').on('change', function(event) {
        interviewVideoTranscript = unryddleApp.getFileInput(event);
    });
    $('#interview_host_image').on('change', function(event) {
        interviewHostImageFile = unryddleApp.getFileInput(event);
    });

    // submit insight data
    $submitInsightBtn.on('click', function(e) {
        e.preventDefault();
           
        if($webinarForm.valid()){
            $insightProgressModal.modal(unryddleApp.modalOptions);
            uploadImageFiles($('#profileImage'), 'Profile Image', $('#uploadBar1'), insightImageFile, function(url){
                insightURLS.profileURL = url;               
            });
            uploadImageFiles($('#videoThumbnail'), 'Video Thumbnail', $('#uploadBar5'), insightVideoThumbnail, function(url) {
                insightURLS.videoThumbnailURL = url;
            });
            uploadTranscripts($('#videoTranscript'), 'Video Transcript', $('#uploadBar4'), insightVideoTranscript, function(url) {
                insightURLS.videoTranscriptURL = url;
            });
            for(var i = 0; i < insightVideoFiles.length; i++){
                if(insightVideoFiles[i].type === 'video/mp4'){
                    uploadVideoFiles($('#videoMP4'), 'MP4 Video', $('#uploadBar3'), insightVideoFiles[i], function(url){
                        insightURLS.mp4VideoURL = url;
                    });
                }

                if(insightVideoFiles[i].type === 'video/webm'){
                    uploadVideoFiles($('#videoWEBM'), 'WEBM Video', $('#uploadBar2'), insightVideoFiles[i], function(url){
                        insightURLS.webmVideoURL = url;
                    })
                }
            }
        } else {
            $('#webinar-error').removeClass('d-none');
            setTimeout(function(){
                $('#webinar-error').addClass('d-none');
            }, 10000);   
        }
    });

    $submitInterviewBtn.on('click', function(e) {
        e.preventDefault();

        if($webinarForm.valid()){
            $interviewProgressModal.modal(unryddleApp.modalOptions);
            uploadImageFiles($('#profileInterviewImage'), 'Profile Image', $('#uploadIntBar1'), interviewImageFile, function(url){
                interviewURLS.profileImageURL = url;
            });
            uploadImageFiles($('#profileHostImage'), 'Host Profile Image', $('#uploadIntBar6'), interviewHostImageFile, function(url){
                interviewURLS.hostProfileImageURL = url;
            });
            uploadImageFiles($('#videoInterviewThumbnail'), 'Video Thumbnail', $('#uploadIntBar5'), interviewVideoThumbnail, function(url) {
                interviewURLS.videoThumbnailURL = url;
            });
            uploadTranscripts($('#videoInterviewTranscript'), 'Video Transcript', $('#uploadIntBar4'), interviewVideoTranscript, function(url) {
                interviewURLS.videoTranscriptURL = url;
            });
            for(var i = 0; i < interviewVideoFiles.length; i++){
                if(interviewVideoFiles[i].type === 'video/mp4'){
                    uploadVideoFiles($('#videoInterviewMP4'), 'MP4 Video', $('#uploadIntBar3'), interviewVideoFiles[i], function(url){
                        interviewURLS.mp4VideoURL = url;
                    });
                }

                if(interviewVideoFiles[i].type === 'video/webm'){
                    uploadVideoFiles($('#videoInterviewWEBM'), 'WEBM Video', $('#uploadIntBar2'), interviewVideoFiles[i], function(url){
                        interviewURLS.webmVideoURL = url;
                    })
                }
            }
        } else {
            $('#webinar-error').removeClass('d-none');
            setTimeout(function(){
                $('#webinar-error').addClass('d-none');
            }, 10000);
        }
    });

    // submit insight payload data after files upload
    $(document).on('click', '#webinarInsightProgressModal .modal-dialog.modal-dialog-centered .modal-content .modal-footer button#publishInsightWebinar', function(e){
        e.preventDefault();
        var btn = $(this);
        var loader = $(this).find('.spinner');
        var done = $('#insightDone');
        var btnText = $(this).find('.action-text');
        var insightPayload = processInsightPayload('insight');
        insightPayload.speaker.profileImageURL = insightURLS.profileURL;
        insightPayload.videoTranscriptURL = insightURLS.videoTranscriptURL;
        insightPayload.videoThumbnailURL = insightURLS.videoThumbnailURL;
        insightPayload.webmVideoURL = insightURLS.webmVideoURL;
        insightPayload.mp4VideoURL = insightURLS.mp4VideoURL;
        btnText.addClass('d-none');
        loader.removeClass('d-none');
        var webinarKey = service.webinarsRef.push().key;
        var instructorsKey = service.instructorsRef.push().key;
        var webinarData = {};
        var instructorData = {};
        webinarData['app_lessons/' + webinarKey] = insightPayload;
        webinarData['webinars/' + webinarKey] = insightPayload;
        instructorData['instructors/' + instructorsKey] = insightPayload.speaker;
        instructorData['instructors/' + instructorsKey + '/lessons/' + webinarKey] = insightPayload;
        firebase.database().ref().update(webinarData, function(err){
            if(err){
                console.log(err);
            } else {
                firebase.database().ref().update(instructorData, function(err){
                    if(err) {
                        console.log(err);
                    } else {
                        loader.addClass('d-none');
                        btnText.removeClass('d-none');
                        btn.addClass('d-none');
                        done.removeClass('d-none');
                    }
                });
            }
        });
    });

    // submit interview payload data after files upload
    $(document).on('click', '#webinarInterviewProgressModal .modal-dialog.modal-dialog-centered .modal-content .modal-footer button#publishInterviewWebinar', function(e){
        e.preventDefault();
        var btn = $(this);
        var loader = $(this).find('.spinner');
        var done = $('#interviewDone');
        var btnText = $(this).find('.action-text');
        var interviewPayload = processInterviewPayload('interview');
        interviewPayload.speaker.profileImageURL = interviewURLS.profileImageURL;
        interviewPayload.host.hostImageURL = interviewURLS.hostProfileImageURL;
        interviewPayload.videoTranscriptURL = interviewURLS.videoTranscriptURL;
        interviewPayload.videoThumbnailURL = interviewURLS.videoThumbnailURL;
        interviewPayload.webmVideoURL = interviewURLS.webmVideoURL;
        interviewPayload.mp4VideoURL = interviewURLS.mp4VideoURL;
        btnText.addClass('d-none');
        loader.removeClass('d-none');
        var webinarKey = service.webinarsRef.push().key;
        var instructorsKey = service.instructorsRef.push().key;
        var webinarData = {};
        var instructorData = {};
        webinarData['app_lessons/' + webinarKey] = interviewPayload;
        webinarData['webinars/' + webinarKey] = interviewPayload;
        instructorData['instructors/' + instructorsKey] = interviewPayload.speaker;
        instructorData['instructors/' + instructorsKey + '/lessons/' + webinarKey] = interviewPayload;
        firebase.database().ref().update(webinarData, function(err){
            if(err){
                console.log(err);
            } else {
                loader.addClass('d-none');
                btnText.removeClass('d-none');
                btn.addClass('d-none');
                done.removeClass('d-none');
            }
        });
        firebase.database().ref().update(instructorData, function (err) {
            if (err) {
                console.log(err);
            } else {

            }
        })
    });

    // reset form on success
    $insightProgressModal.on('hidden.bs.modal', function (e) {
        // do something...
        resetFormInputs();
        resetInsightEditors();
        location.reload()
    });
    $interviewProgressModal.on('hidden.bs.modal', function(e) {
        resetFormInputs();
        resetInterviewEditors();
        location.reload();
    });
}(jQuery))

(function($) {
  "use strict";

  var careerPageData = {
    successMsg: "Form data was successfully submitted",
    careerPathObj: null,
    careerPathNames: [],
    careerClusterNames: []
  };

  var $logout = $("#logout-action"),
    $loadingbar = $("#loadprogress"),
    $careerPathListGroup = $("#careerPathGroup"),
    $careerClusterListGroup = $("#careerClusterGroup"),
    $careerPathwayListGroup = $("#careerPathwayGroup"),
    $industryListGroup = $("#industryGroup"),
    $workContextListGroup = $("#workContextGroup"),
    $workStylesListGroup = $("#workStylesGroup"),
    $workValuesListGroup = $("#workValuesGroup"),
    $knowledgeListGroup = $("#knowledgeGroup"),
    $skillsListGroup = $("#skillsGroup"),
    $technologyListGroup = $("#technologyGroup"),
    $careerPathForm = $("#createCareerPath"),
    $careerPathBtn = $("#submit_career-path"),
    $careerPathSuccess = $("#career_path-success"),
    $careerPathError = $("#career_path-error"),
    $careerClusterForm = $("#createCareerCluster"),
    $careerClusterName = $("#career_cluster_name"),
    $careerClusterCode = $("#career_cluster_code"),
    $careerClusterCP = $("#career_cluster_careerpath"),
    $careerClusterDesc = $("#career_cluster_description"),
    $careerClusterBtn = $("#submit_career-cluster"),
    $careerClusterSuccess = $("#career_cluster-success"),
    $careerClusterError = $("#career_cluster-error"),
    $careerPathwayForm = $("#createCareerPathway"),
    $careerPathwayName = $("#career_pathway_name"),
    $careerPathwayCode = $("#career_pathway_code"),
    $careerPathwayCluster = $("#career_pathway_cluster"),
    $careerPathwayDesc = $("#career_pathway_description"),
    $careerPathwaySuccess = $("#career_pathway-success"),
    $careerPathwayError = $("#career_pathway-error"),
    $careerPathwayBtn = $("#submit_career-pathway"),
    $createIndustryForm = $("#createIndustry"),
    $industryName = $("#industry_name"),
    $industryCode = $("#industry_code"),
    $industryCluster = $("#industry_careerCluster"),
    $industryDesc = $("#industry_description"),
    $industrySuccess = $("#industry-success"),
    $industryError = $("#industry-error"),
    $industryBtn = $("#submit_industry"),
    $workContextForm = $("#createWorkContext"),
    $workContextName = $("#workContext_name"),
    $workContextDesc = $("#workContext_description"),
    $workContextCategory = $("#workContext_category"),
    $workContextSuccess = $("#workContext-success"),
    $workContextError = $("#workContext-error"),
    $workContextBtn = $("#submit_workContext"),
    $workStyleForm = $("#createWorkStyles"),
    $workStyleName = $("#workStyle_name"),
    $workStyleDesc = $("#workStyle_description"),
    $workStyleSuccess = $("#workStyle-success"),
    $workStyleError = $("#workStyle-error"),
    $workStyleBtn = $("#submit_workStyle"),
    $workValuesForm = $("#createWorkValues"),
    $workValuesName = $("#workValue_name"),
    $workValuesDesc = $("#workValue_description"),
    $workValuesSuccess = $("#workValue-success"),
    $workValuesError = $("#workValue-error"),
    $workValuesBtn = $("#submit_workValue"),
    $careerKnowledgeForm = $("#createKnowledge"),
    $careerKnowledgeName = $("#knowledge_name"),
    $careerKnowledgeDesc = $("#knowledge_description"),
    $careerKnowledgeSuccess = $("#knowledge-success"),
    $careerKnowledgeError = $("#knowledge-error"),
    $careerKnowledgeBtn = $("#submit_knowledge"),
    $careerSkillsForm = $("#createCareerSkills"),
    $careerSkillsName = $("#careerSkills_name"),
    $careerSkillsDesc = $("#careerSkills_description"),
    $careerSkillsCategory = $("#careerSkills_category"),
    $careerSkillsSuccess = $("#careerSkills-success"),
    $careerSkillsError = $("#careerSkills-error"),
    $careerSkillsBtn = $("#submit_careerSkills"),
    $careerTechForm = $("#createCareerTech"),
    $careerTechName = $("#careerTech_name"),
    $careerTechSoftware = $("#careerTech_software"),
    $careerTechSuccess = $("#careerTech-success"),
    $careerTechError = $("#careerTech-error"),
    $careerTechBtn = $("#submit_careerTech"),
    $tabNav = $(".nav-tabs"),
    $editCareerDetailsModal = $("#editCareerDetailModal"),
    $editCareerDetailForm = $("#editCareerDetailForm"),
    $editCareerDetailBtn = $("#update_careerDetail"),
    $editCareerDetailSuccess = $("#edit_careerDetail-success"),
    $editCareerDetailError = $("#edit_careerDetail-error"),
    $editCareerDetailName = $('#careerDetail_name'),
    $editCareerDetailAlias = $('#careerDetail_alias'),
    $editCareerDetailCode = $('#careerDetail_code'),
    $editCareerDetailDesc = $('#careerDetail_desc'),
    $aliasWrapper = $('#aliasWrapper'),
    $codeWrapper = $('#codeWrapper');

  function spliceNames(obj){
    var arr = [];
    for(var item in obj){
        if(obj.hasOwnProperty(item)){
            arr.push(obj[item].name);
        }
    }
    return arr;
  }

  function getCareerPathData() {
    $loadingbar.removeClass("d-none");
    $careerPathListGroup.html("");
    var careerPathData = unryddleApp.loadData("careerPaths");
    var category = "career_path";
    processDataListing(category, careerPathData);
  }

  function getCareerClusterData() {
    $loadingbar.removeClass("d-none");
    $careerClusterListGroup.html("");
    var category = "career_cluster";
    var data = unryddleApp.loadData('careerClusters');
    processDataListing(category, data);
  }

  function getCareerPathWaysData() {
    $loadingbar.removeClass("d-none");
    $careerPathwayListGroup.html("");
    var category = "career_pathway";
    var data = unryddleApp.loadData("careerPathways");
    processDataListing(category, data);
  }

  function getCareerIndustriesData() {
    $loadingbar.removeClass("d-none");
    $industryListGroup.html("");
    var category = "industry";
    var data = unryddleApp.loadData("careerIndustries");
    processDataListing(category, data);
  }

  function getCareerWorkContextsData() {
    $loadingbar.removeClass("d-none");
    $workContextListGroup.html("");
    var category = "work_context";
    var data = unryddleApp.loadData("careerWorkContexts");
    processDataListing(category, data);
  }

  function getCareerWorkStylesData() {
    $loadingbar.removeClass("d-none");
    $workStylesListGroup.html("");
    var category = "work_styles";
    var data = unryddleApp.loadData("careerWorkStyles");
    processDataListing(category, data);
  }

  function getCareerWorkValuesData() {
    $loadingbar.removeClass("d-none");
    $workValuesListGroup.html("");
    var category = "work_values";
    var data = unryddleApp.loadData("careerWorkValues");
    processDataListing(category, data);
  }

  function getCareerKnowledgeData() {
    $loadingbar.removeClass("d-none");
    $knowledgeListGroup.html("");
    var category = "knowledge";
    var data = unryddleApp.loadData("careerKnowledge");
    processDataListing(category, data);
  }

  function getCareerSkillData() {
    $loadingbar.removeClass("d-none");
    $skillsListGroup.html("");
    var category = "skills";
    var data = unryddleApp.loadData("careerSkills");
    processDataListing(category, data);
  }

  function getCareerTechnologiesData() {
    $loadingbar.removeClass("d-none");
    $technologyListGroup.html("");
    var category = "technology";
    var data = unryddleApp.loadData("careerTechnologies");
    processDataListing(category, data);
  }

  // process data listing for career details
  function processDataListing(cat, data) {
    if (cat == "career_path") {
      if (data === null) {
        $careerPathListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $careerPathListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }

    if (cat == "career_cluster") {
      if (data === null) {
        $careerClusterListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $careerClusterListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }

    if (cat == "career_pathway") {
      if (data === null) {
        $careerPathwayListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $careerPathwayListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }

    if (cat == "industry") {
      if (data === null) {
        $industryListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $industryListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }

    if (cat == "work_context") {
      if (data === null) {
        $workContextListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $workContextListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }

    if (cat == "work_styles") {
      if (data === null) {
        $workStylesListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $workStylesListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }

    if (cat == "work_values") {
      if (data === null) {
        $workValuesListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $workValuesListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }

    if (cat == "knowledge") {
      if (data === null) {
        $knowledgeListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $knowledgeListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }

    if (cat == "skills") {
      if (data === null) {
        $skillsListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $skillsListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }

    if (cat == "technology") {
      if (data === null) {
        $technologyListGroup.append($(emptyData()));
        $loadingbar.addClass("d-none");
      } else {
        var count = 0;
        for (var item in data) {
          if (data.hasOwnProperty(item)) {
            count++;
            var dataSet = data[item];
            var dataID = item;
            $technologyListGroup.append(
              $(listTemplate(count, dataID, dataSet, cat))
            );
            $loadingbar.addClass("d-none");
          }
        }
      }
    }
  }

  function getCheckVals(obj) {
    var personalities = [];
    obj.each(function() {
      var $this = $(this);
      if ($this.prop("checked")) {
        personalities.push($(this).val());
      }
    });

    return personalities;
  }

  function renderExistMsg(handler) {
    var exist =
      "An entry with the same name already exists within the database. Please create a new entry with a new name if you wish to proceed.";
    handler.text(exist);
    handler.removeClass("d-none");
    $("input[type='text'], textarea, select").val("");
    $("input[type='checkbox']").removeAttr("checked");
  }

  function renderSuccessMsg(str, handler) {
    handler.text(str);
    handler.removeClass("d-none");
    // setTimeout(function() {
    //   handler.addClass('d-none');
    // }, 6000);
    $("input[type='text'], textarea, select").val("");
  }

  function renderErrorMsg(error, handler) {
    var str;
    if (error.code == "auth/user-not-found") {
      str =
        "User does not exist. Please ensure you have entered a valid email and password.";
    } else if (error.Error == "PERMISSION_DENIED") {
      str = "You do not have permission to upload content here.";
    } else {
      str =
        "There was a problem uploading your data. Please try again in a few minutes";
    }
    handler.text(str);
    handler.removeClass("d-none");
    // setTimeout(function() {
    //   handler.addClass('d-none');
    // }, 6000);
    $("input[type='text'], textarea, select").val("");
  }

  // empty data set template
  function emptyData() {
    var html =
      "<h4 class='text-center mt-5 empty-header'>There are no entries for this section. Please click the link above to add an entry.</h4>";

    return html;
  }

  // listing template for career details
  function listTemplate(count, id, obj, category) {
    var alias, code;
    if (obj.alias === undefined) {
      alias = "N/A";
    } else {
      alias = obj.alias;
    }

    if (obj.code === undefined) {
      code = "N/A";
    } else {
      code = obj.code;
    }
    var html =
      "<div class='list-group-item list-group-item-action flex-column align-items-start'>" +
      "<div class='row w-100 personality-list-margin'>" +
      "<div class='col-2 text-right item-label'>" +
      "<span class='badge badge-unryddle'>" +
      count +
      "</span>" +
      "Name:" +
      "</div>" +
      "<div class='col-8 user_name'>" +
      obj.name +
      "</div>" +
      "<div class='col-2'>" +
      "<a href='#' id='edit-careerdata' class='user-action' data-name='" +
      category +
      "'" +
      "data-id='" +
      id +
      "'" +
      ">" +
      "<i class='fas fa-edit'></i>" +
      "</a>" +
      "</div>" +
      "</div>" +
      "<div class='row w-100 personality-list-margin'>" +
      "<div class='col-2 text-right item-label'>" +
      "Alias:" +
      "</div>" +
      "<div class='col-10 user_slug text-capitalize'>" +
      alias +
      "</div>" +
      "</div>" +
      "<div class='row w-100 personality-list-margin'>" +
      "<div class='col-2 text-right item-label'>" +
      "Code:" +
      "</div>" +
      "<div class='col-10 user_slug text-uppercase'>" +
      code +
      "</div>" +
      "</div>" +
      "<div class='row w-100 personality-list-margin'>" +
      "<div class='col-2 text-right item-label'>" +
      "Description:" +
      "</div>" +
      "<div class='col-10 item_slug'>" +
      obj.description +
      "</div>" +
      "</div>" +
      "</div>";

    return html;
  }

  function createTypeAheadList(obj, arr) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        arr.push(obj[key].name);
      }
    }
    return arr;
  }

  function submitCareerPathData(obj) {
    // console.log(obj.data);
    // console.log(obj.payload);

    if (obj.data) {
      obj.spinner.addClass("d-none");
      renderExistMsg(obj.errorHandler);
    } else {
      var careerPathKey = service.careerPathRef.push().key;
      firebase
        .database()
        .ref("career_path/" + careerPathKey)
        .set(obj.payload, function(error) {
          if (error) {
            console.log(error);
            obj.spinner.addClass("d-none");
            // $('input[name=jhsQuestionOptions]').attr('checked', false);
            renderErrorMsg(error, $careerPathError);
          } else {
            obj.spinner.addClass("d-none");
            // $('input[name=jhsQuestionOptions]').attr('checked', false);
            renderSuccessMsg(careerPageData.successMsg, $careerPathSuccess);
          }
        });
    }
  }

  function submitCareerClusterData(obj) {
    if (obj.data) {
      obj.spinner.addClass("d-none");
      renderExistMsg(obj.errorHandler);
    } else {
      var careerClusterKey = service.careerClusterRef.push().key;
      firebase
        .database()
        .ref("career_clusters/" + careerClusterKey)
        .set(obj.payload, function(error) {
          if (error) {
            console.log(error);
            obj.spinner.addClass("d-none");
            // $('input[name=jhsQuestionOptions]').attr('checked', false);
            renderErrorMsg(error, $careerClusterError);
          } else {
            obj.spinner.addClass("d-none");
            // $('input[name=jhsQuestionOptions]').attr('checked', false);
            renderSuccessMsg(careerPageData.successMsg, $careerClusterSuccess);
          }
        });
    }
  }

  // process career detail for edit when edit button clicked
  function processCareerDetailAction(event) {
    let target = event.target;

    if (target.tagName === "svg" || target.tagName === 'path') {
      let targetID = $(target)
        .closest("a")
        .data("id");
      let category = $(target)
        .closest("a")
        .data("name");

      // process data based on category of selected list item
      if (category == "career_path") {
        service.careerPathRef.child(targetID).once("value", function(snapshot) {
          var data = snapshot.val();
          $editCareerDetailForm.find("input, select").val("");
          renderCareerDetailEditForm(category, targetID, data);
        });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }

      if (category == "career_cluster") {
        service.careerClusterRef
          .child(targetID)
          .once("value", function(snapshot) {
            var data = snapshot.val();
            $editCareerDetailForm.find("input, select").val("");
            renderCareerDetailEditForm(category, targetID, data);
          });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }

      if (category == "career_pathway") {
        service.careerPathwayRef
          .child(targetID)
          .once("value", function(snapshot) {
            var data = snapshot.val();
            $editCareerDetailForm.find("input, select").val("");
            renderCareerDetailEditForm(category, targetID, data);
          });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }

      if (category == "industry") {
        service.careerIndustryRef
          .child(targetID)
          .once("value", function(snapshot) {
            var data = snapshot.val();
            $editCareerDetailForm.find("input, select").val("");
            renderCareerDetailEditForm(category, targetID, data);
          });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }

      if (category == "work_context") {
        service.workContextRef
          .child(targetID)
          .once("value", function(snapshot) {
            var data = snapshot.val();
            $editCareerDetailForm.find("input, select").val("");
            renderCareerDetailEditForm(category, targetID, data);
          });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }

      if (category == "work_styles") {
        service.workStylesRef.child(targetID).once("value", function(snapshot) {
          var data = snapshot.val();
          $editCareerDetailForm.find("input, select").val("");
          renderCareerDetailEditForm(category, targetID, data);
        });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }

      if (category == "work_values") {
        service.workValuesRef.child(targetID).once("value", function(snapshot) {
          var data = snapshot.val();
          $editCareerDetailForm.find("input, select").val("");
          renderCareerDetailEditForm(category, targetID, data);
        });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }

      if (category == "knowledge") {
        service.careerKnowledgeRef
          .child(targetID)
          .once("value", function(snapshot) {
            var data = snapshot.val();
            $editCareerDetailForm.find("input, select").val("");
            renderCareerDetailEditForm(category, targetID, data);
          });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }

      if (category == "skills") {
        service.careerSkillsRef
          .child(targetID)
          .once("value", function(snapshot) {
            var data = snapshot.val();
            $editCareerDetailForm.find("input, select").val("");
            renderCareerDetailEditForm(category, targetID, data);
          });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }

      if (category == "technology") {
        service.careerTechRef.child(targetID).once("value", function(snapshot) {
          var data = snapshot.val();
          $editCareerDetailForm.find("input, select").val("");
          renderCareerDetailEditForm(category, targetID, data);
        });
        $editCareerDetailsModal.modal(unryddleApp.modalOptions);
      }
    }
  }

  function renderCareerDetailEditForm(cat, id, obj) {
    switch (cat) {
      case 'career_path':
        $editCareerDetailName.val(obj.name);
        $editCareerDetailAlias.val(obj.alias);
        $editCareerDetailCode.val(obj.code);
        $editCareerDetailDesc.val(obj.description);
        break;

      case 'industry':
      case 'career_cluster':
      case 'career_pathway':
        $aliasWrapper.addClass('d-none');
        $codeWrapper.removeClass('col-6 col-md-6').addClass('col-12 col-md-12');
        $editCareerDetailName.val(obj.name);
        $editCareerDetailCode.val(obj.code);
        $editCareerDetailDesc.val(obj.description);
        break;

      case 'work_styles':
      case 'work_values':
      case 'work_context':
      case 'knowledge':
      case 'skills':
      case 'technology':
        $aliasWrapper.addClass('d-none');
        $codeWrapper.addClass('d-none');
        $editCareerDetailName.val(obj.name);
        $editCareerDetailDesc.val(obj.description);
        break;
    }
    $editCareerDetailForm.attr({'data-category': cat, 'data-id': id});
    $('.editCareerDetailName').text(obj.name);

  }

  getCareerPathData();
  getCareerClusterData();

  $(document).ready(function() {
    console.log("page ready...");
    // $(".nav-tabs").scrollingTabs({
    //   bootstrapVersion: 4
    // });

    $('#sign-out').on("click", function(e) {
      e.preventDefault();

      service.signOut();
    });

    $("input, select, textarea").on(
      "change keyup keydown paste click",
      function() {
        if (!$(".alert").hasClass("d-none")) {
          $(".alert").addClass("d-none");
        }
      }
    );

    // clear form entries and alert boxes on modal close
    $editCareerDetailsModal.on('hide.bs.modal', function(e) {
      console.log('we are hiding this modal');

      $editCareerDetailForm.find('input, textarea').val('');
      if(!$editCareerDetailSuccess.hasClass('d-none')){
        $editCareerDetailSuccess.addClass('d-none');
      }

      if(!$editCareerDetailError.hasClass('d-none')){
        $editCareerDetailError.addClass('d-none');
      }
    })

    // career details tab event
    $('a[data-toggle="tab"]').on("shown.bs.tab", function(e) {
      var tabTarget = $(e.target).attr("id");

      // console.log(tabTarget);
      if (tabTarget == "careerPath") {
        getCareerPathData();
      }

      if (tabTarget == "careerCluster") {
        getCareerClusterData();
      }

      if (tabTarget == "careerPathway") {
        getCareerPathWaysData();
      }

      if (tabTarget == "careerIndustry") {
        getCareerIndustriesData();
      }

      if (tabTarget == "careerWorkContext") {
        getCareerWorkContextsData();
      }

      if (tabTarget == "careerWorkStyles") {
        getCareerWorkStylesData();
      }

      if (tabTarget == "careerWorkValues") {
        getCareerWorkValuesData();
      }

      if (tabTarget == "careerKnowledge") {
        getCareerKnowledgeData();
      }

      if (tabTarget == "careerSkills") {
        getCareerSkillData();
      }

      if (tabTarget == "careerTechnology") {
        getCareerTechnologiesData();
      }
    });

    // validate path form
    $careerPathForm.validate({
      rules: {
        career_path_name: { required: true },
        career_path_alias: { required: true },
        career_path_description: { required: true },
        career_path_code: { required: true },
        careerPath_personality: { required: true }
      }
    });

    // submit career path form
    $careerPathForm.on("submit", function(e) {
      e.preventDefault();
      var processObj = {};
      processObj.spinner = $careerPathBtn.find("i");
      processObj.spinner.removeClass("d-none");
      if ($(this).valid()) {
        processObj.payload = {
          name: $("#career_path_name").val(),
          alias: $("#career_path_alias").val(),
          code: $("#career_path_code").val(),
          description: $("#career_path_description").val(),
          personality_types: getCheckVals(
            $("input[name='careerPath_personality']")
          ),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };
        service.careerPathRef
          .orderByChild("name")
          .equalTo(processObj.payload.name)
          .once("value", function(snapshot) {
            processObj.data = snapshot.val();
            processObj.errorHandler = $careerPathError;
            submitCareerPathData(processObj);
            // console.log(snapshot.val());
          })
          .catch(function(error) {
            console.log(error);
            spinner.addClass("d-none");
          });
      } else {
        console.log("please fill all fields");
        spinner.addClass("d-none");
      }
    });

    // validate career cluster form
    $careerClusterForm.validate({
      rules: {
        career_cluster_name: { required: true },
        career_cluster_code: { required: true },
        career_cluster_careerpath: { required: true },
        career_cluster_description: { required: true },
        careerCluster_personality: { required: true }
      }
    });

    // submit cluster form
    $careerClusterForm.on("submit", function(e) {
      e.preventDefault();
      var processObj = {};
      processObj.spinner = $careerClusterBtn.find("i");
      processObj.spinner.removeClass("d-none");
      if ($(this).valid()) {
        processObj.payload = {
          name: $careerClusterName.val(),
          code: $careerClusterCode.val(),
          career_path: $careerClusterCP.val(),
          description: $careerClusterDesc.val(),
          personality_types: getCheckVals(
            $("input[name='careerCluster_personality']")
          ),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };

        service.careerClusterRef
          .orderByChild("name")
          .equalTo(processObj.payload.name)
          .once("value", function(snapshot) {
            processObj.data = snapshot.val();
            processObj.errorHandler = $careerClusterError;
            submitCareerClusterData(processObj);
          })
          .catch(function(error) {
            console.log(error);
          });
      } else {
        console.log("form invalid...");
      }
    });

    // validate pathway form
    $careerPathwayForm.validate({
      rules: {
        career_pathway_name: { required: true },
        career_pathway_code: { required: true },
        career_pathway_cluster: { required: true },
        career_pathway_description: { required: true }
      }
    });

    // submit pathway data
    $careerPathwayForm.on("submit", function(e) {
      e.preventDefault();

      var spinner = $careerPathwayBtn.find("i");
      if ($(this).valid()) {
        var payload = {
          name: $careerPathwayName.val(),
          code: $careerPathwayCode.val(),
          career_cluster: $careerPathwayCluster.val(),
          description: $careerPathwayDesc.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };

        var careerPathwayKey = service.careerPathwayRef.push().key;
        firebase
          .database()
          .ref("career_pathways/" + careerPathwayKey)
          .set(payload, function(error) {
            if (error) {
              console.log(error);
              spinner.addClass("d-none");
              // $('input[name=jhsQuestionOptions]').attr('checked', false);
              renderErrorMsg(error, $careerPathwayError);
            } else {
              spinner.addClass("d-none");
              // $('input[name=jhsQuestionOptions]').attr('checked', false);
              renderSuccessMsg(
                careerPageData.successMsg,
                $careerPathwaySuccess
              );
            }
          });
      } else {
        console.log("form is invalid");
      }
    });

    // validate industry form
    $createIndustryForm.validate({
      rules: {
        industry_name: { required: true },
        industry_code: { required: true },
        industry_careerCluster: { required: true },
        industry_description: { required: true }
      }
    });

    // submit industrry form data
    $createIndustryForm.on("submit", function(e) {
      e.preventDefault();

      var spinner = $industryBtn.find("i");
      if ($(this).valid()) {
        spinner.removeClass("d-none");
        var payload = {
          name: $industryName.val(),
          code: $industryCode.val(),
          career_cluster: $industryCluster.val(),
          description: $industryDesc.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };
        // console.log(payload);
        var industryKey = service.careerIndustryRef.push().key;
        firebase
          .database()
          .ref("career_industries/" + industryKey)
          .set(payload, function(error) {
            if (error) {
              console.log(error);
              spinner.addClass("d-none");
              // $('input[name=jhsQuestionOptions]').attr('checked', false);
              renderErrorMsg(error, $industryError);
            } else {
              spinner.addClass("d-none");
              // $('input[name=jhsQuestionOptions]').attr('checked', false);
              renderSuccessMsg(careerPageData.successMsg, $industrySuccess);
            }
          });
      } else {
        console.log("form invalid");
      }
    });

    // validate work context form
    $workContextForm.validate({
      rules: {
        workContext_name: { required: true },
        workContext_description: { required: true },
        workContext_category: { required: true }
      }
    });

    // submit work context form
    $workContextForm.on("submit", function(e) {
      e.preventDefault();

      var spinner = $workContextBtn.find("i");

      if ($(this).valid()) {
        spinner.removeClass("d-none");
        var payload = {
          name: $workContextName.val(),
          category: $workContextCategory.val(),
          description: $workContextDesc.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };

        var workContextKey = service.workContextRef.push().key;
        firebase
          .database()
          .ref("work_contexts/" + workContextKey)
          .set(payload, function(error) {
            if (error) {
              spinner.addClass("d-none");
              renderErrorMsg(error, $workContextError);
            } else {
              spinner.addClass("d-none");
              renderSuccessMsg(careerPageData.successMsg, $workContextSuccess);
            }
          });
      }
    });

    // validate work styles form
    $workStyleForm.validate({
      rules: {
        workStyle_name: { required: true },
        workStyle_description: { required: true }
      }
    });

    // submit work styles form
    $workStyleForm.on("submit", function(e) {
      e.preventDefault();

      var spinner = $workStyleBtn.find("i");
      if ($(this).valid()) {
        spinner.removeClass("d-none");
        var payload = {
          name: $workStyleName.val(),
          description: $workStyleDesc.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };

        var workStyleKey = service.workStylesRef.push().key;
        firebase
          .database()
          .ref("work_styles/" + workStyleKey)
          .set(payload, function(error) {
            if (error) {
              spinner.addClass("d-none");
              renderErrorMsg(error, $workStyleError);
            } else {
              spinner.addClass("d-none");
              renderSuccessMsg(careerPageData.successMsg, $workStyleSuccess);
            }
          });
      }
    });

    // validate work values form
    $workValuesForm.validate({
      rules: {
        workValue_name: { required: true },
        workValue_description: { required: true }
      }
    });

    // submit work values form data
    $workValuesForm.on("submit", function(e) {
      e.preventDefault();

      var spinner = $workValuesBtn.find("i");
      if ($(this).valid()) {
        spinner.removeClass("d-none");
        var payload = {
          name: $workValuesName.val(),
          description: $workValuesDesc.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };

        var workValueKey = service.workValuesRef.push().key;
        firebase
          .database()
          .ref("work_values/" + workValueKey)
          .set(payload, function(error) {
            if (error) {
              spinner.addClass("d-none");
              renderErrorMsg(error, $workValuesError);
            } else {
              spinner.addClass("d-none");
              renderSuccessMsg(careerPageData.successMsg, $workValuesSuccess);
            }
          });
      }
    });

    // validate career knowledge form
    $careerKnowledgeForm.validate({
      rules: {
        knowledge_name: { required: true },
        knowledge_description: { required: true }
      }
    });

    // submit career knowledge form data
    $careerKnowledgeForm.on("submit", function(e) {
      e.preventDefault();

      var spinner = $careerKnowledgeBtn.find("i");
      if ($(this).valid()) {
        spinner.removeClass("d-none");
        var payload = {
          name: $careerKnowledgeName.val(),
          description: $careerKnowledgeDesc.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };

        var knowledgeKey = service.careerKnowledgeRef.push().key;
        firebase
          .database()
          .ref("career_knowledge/" + knowledgeKey)
          .set(payload, function(error) {
            if (error) {
              spinner.addClass("d-none");
              renderErrorMsg(error, $careerKnowledgeError);
            } else {
              spinner.addClass("d-none");
              renderSuccessMsg(
                careerPageData.successMsg,
                $careerKnowledgeSuccess
              );
            }
          });
      }
    });

    // validate career skills form
    $careerSkillsForm.validate({
      rules: {
        careerSkills_name: { required: true },
        careerSkills_description: { required: true },
        careerSkills_category: { required: true }
      }
    });

    // submit career skills form data
    $careerSkillsForm.on("submit", function(e) {
      e.preventDefault();

      var spinner = $careerSkillsBtn.find("i");
      if ($(this).valid()) {
        spinner.removeClass("d-none");
        var payload = {
          name: $careerSkillsName.val(),
          category: $careerSkillsCategory.val(),
          description: $careerSkillsDesc.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };

        var careerSkillsKey = service.careerSkillsRef.push().key;
        firebase
          .database()
          .ref("career_skills/" + careerSkillsKey)
          .set(payload, function(error) {
            if (error) {
              spinner.addClass("d-none");
              renderErrorMsg(error, $careerSkillsError);
            } else {
              spinner.addClass("d-none");
              renderSuccessMsg(careerPageData.successMsg, $careerSkillsSuccess);
            }
          });
      }
    });

    // validate career tech form
    $careerTechForm.validate({
      rules: {
        careerTech_name: { required: true },
        careerTech_software: { required: true }
      }
    });

    // submit career tech form data
    $careerTechForm.on("submit", function(e) {
      e.preventDefault();

      var spinner = $careerTechBtn.find("i");
      if ($(this).valid()) {
        spinner.removeClass("d-none");
        var payload = {
          name: $careerTechName.val(),
          softwares: $careerTechSoftware.val(),
          createdOn: firebase.database.ServerValue.TIMESTAMP
        };

        var technologyKey = service.careerTechRef.push().key;
        firebase
          .database()
          .ref("career_technologies/" + technologyKey)
          .set(payload, function(error) {
            if (error) {
              spinner.addClass("d-none");
              renderErrorMsg(error, $careerTechError);
            } else {
              spinner.addClass("d-none");
              renderSuccessMsg(careerPageData.successMsg, $careerTechSuccess);
            }
          });
      }
    });

    // edit button click event function
    $(document).on("click", ".urWrapper #urSiteContent .dashWrapper .container-fluid .row .col-12 .shadow-sm.card .card-body .container-fluid .row .col.markersTab .tab-content .list-group .list-group-item .row .col-2 a", processCareerDetailAction);

    // submit edit form data
    $editCareerDetailForm.on('submit', function(e){
      e.preventDefault();

      let category = $(this).attr('data-category');
      let id = $(this).attr('data-id');
      let spinner = $editCareerDetailBtn.find('i.fa.fa-spinner');
      spinner.removeClass('d-none');
      if (category == 'career_path') {
        let careerPath_payload = {
          name: $editCareerDetailName.val(),
          alias: $editCareerDetailAlias.val(),
          code: $editCareerDetailCode.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let careerPathRef = service.careerPathRef.child(id);
        careerPathRef.update(careerPath_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Career Path data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }

      if (category == 'career_cluster') {
        let careerCluster_payload = {
          name: $editCareerDetailName.val(),
          code: $editCareerDetailCode.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let careerClusterRef = service.careerClusterRef.child(id);
        careerClusterRef.update(careerCluster_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Career Cluster data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }

      if (category == 'career_pathway') {
        let careerPathway_payload = {
          name: $editCareerDetailName.val(),
          code: $editCareerDetailCode.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let careerPathwayRef = service.careerPathwayRef.child(id);
        careerPathwayRef.update(careerPathway_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Career Pathway data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }

      if (category == 'industry') {
        let industry_payload = {
          name: $editCareerDetailName.val(),
          code: $editCareerDetailCode.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let industryRef = service.careerIndustryRef.child(id);
        industryRef.update(industry_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Industry data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }

      if (category == 'work_context') {
        let workContext_payload = {
          name: $editCareerDetailName.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let workContextRef = service.workContextRef.child(id);
        workContextRef.update(workContext_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Work Context data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }

      if (category == 'work_styles') {
        let workStyles_payload = {
          name: $editCareerDetailName.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let workStylesRef = service.workStylesRef.child(id);
        workStylesRef.update(workStyles_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Work Styles data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }

      if (category == 'work_values') {
        let workValues_payload = {
          name: $editCareerDetailName.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let workValuesRef = service.workValuesRef.child(id);
        workValuesRef.update(workValues_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Work Values data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }

      if (category == 'knowledge') {
        let knowledge_payload = {
          name: $editCareerDetailName.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let knowledgeRef = service.careerKnowledgeRef.child(id);
        knowledgeRef.update(knowledge_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Knowledge data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }

      if (category == 'skills') {
        let skills_payload = {
          name: $editCareerDetailName.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let skillsRef = service.careerSkillsRef.child(id);
        skillsRef.update(skills_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Skills data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }

      if (category == 'technology') {
        let tech_payload = {
          name: $editCareerDetailName.val(),
          description: $editCareerDetailDesc.val(),
          updatedOn: firebase.database.ServerValue.TIMESTAMP
        }
        let technologyRef = service.careerTechRef.child(id);
        technologyRef.update(tech_payload, function(error){
          if (error) {
            console.log(error);
            $editCareerDetailError.text('There was a problem updating your content. Please try again shortly');
            $editCareerDetailError.removeClass('d-none');
            spinner.addClass('d-none');
          } else {
            $editCareerDetailSuccess.text('Technology data was successfully updated. Please continue');
            $editCareerDetailSuccess.removeClass('d-none');
            spinner.addClass('d-none');
          }
        })
      }
    })

    // setup data for bloodhound typeahead for careerPaths
    var careerPathNames = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: spliceNames(unryddleApp.loadData("careerPaths"))
    });
    $("#path_bloodhound .typeahead").typeahead(
      {
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        name: "careerPathNames",
        source: careerPathNames
      }
    );

    // bloodhound typeahead for career clusters
    var careerClusterNames = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: spliceNames(unryddleApp.loadData("careerClusters"))
    });
    $("#cluster_bloodhound .typeahead").typeahead(
      {
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        name: "careerClusterNames",
        source: careerClusterNames
      }
    );

    // token generator for industry form
    var clusterEngine = new Bloodhound({
      local: spliceNames(unryddleApp.loadData("careerClusters")),
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace
    });
    clusterEngine.initialize();
    $(".tokenfield-typeahead").tokenfield({
      typeahead: [
        null,
        {
          source: clusterEngine.ttAdapter()
        }
      ],
      showAutocompleteOnFocus: true
    });
  });
})(jQuery);
